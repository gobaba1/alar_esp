require("./bootstrap");
window.Vue = require("vue");
import VueSweetalert2 from "vue-sweetalert2";
import BootstrapVue from 'bootstrap-vue';

Vue.use(BootstrapVue)

Vue.component("home", require("./components/HomeComponent.vue").default);
Vue.component("perfil", require("./components/PerfilComponent.vue").default);
Vue.component(
    "notificaciones",
    require("./components/Notificaciones.vue").default
);
//COMPOONENTE BUSCADOR
Vue.component("buscador", require("./components/Buscador.vue").default);
//COMPOONENTE ESTADISTICAS
Vue.component("estadisticas", require("./components/Estadisticas.vue").default);
// COMPONENTES ADMINISTRADOR PROGRAMAS
Vue.component("programas-admin", require("./components/Programas_admin.vue").default);
Vue.component("universidades-admin", require("./components/Universidades_admin.vue").default);
Vue.component("subidamasiva-admin", require("./components/Subidamasiva_admin.vue").default);
Vue.component("ciudades-admin", require("./components/Ciudades_admin.vue").default);
Vue.component("tipo-estudios-admin", require("./components/TipoEstudios_admin.vue").default);
Vue.component("rama-admin", require("./components/Rama_admin.vue").default);
Vue.component("perfil-admin", require("./components/Perfiles_admin.vue").default);
Vue.component("costo_vida", require("./components/Administrador/costo_vida.vue").default);

const app = new Vue({
    el: "#app",
    data: {
        menu: 0
    }
});
