<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> -->

    <style>
        body{
            font-family: 'Roboto', sans-serif;
        }
        .texto-center{
            text-align: center;
        }
        .float-right-80{
            margin-left: 80%;
        }
        hr{
            height: 12px;
            border: 0;
            box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.5);
        }
        .mt-1{
            padding-top: 10px;
        }
    </style>
</head>
<body>

    <!-- {{-- <h1 class="texto-center">Correo enviado <img src="https://somadevoos.com/public_image/checked.png" class="mt-1" width="30" height="30"> </h1> --}} -->

    <div class="texto-center">
        <p style="font-size:2rem; font-weight:bold">
            Hola
        </p>

    </div>
    <div class="texto-center">
        <p>
            Gracias por responder al formulario. Uno de nuestros asesores se estará comunicando con usted lo más pronto posible.
        </p>
        <p>
            Le adjuntamos una cartilla informativa de regalo para que pueda inquirir en cómo emprender en el mundo del internet.
        </p>
    </div>
    <hr>
    <!-- <div class="float-right-80 texto-center">
        {{-- <img src="public_image/logo_soma.jpeg" class="mt-1" width="80" height="80" style="border-radius: 5px"> --}}
        <p class="texto-center">
            Aldo Gregori <br>
            Director General
        </p> -->
    </div>
</body>
</html>
