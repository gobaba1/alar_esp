<table>
    <tr>
        <td style="width:58;text-align:center;vertical-align:middle"><b>Universidad</b></td>
        <td style="width:28;text-align:center;vertical-align:middle"><b>Vivienda universitaria / anual</b></td>
        <td style="width:19;text-align:center;vertical-align:middle"><b>Alimentación, gastos personales,<br>Telefonía celular <br> e Internet / anual</b></td>
        <td style="width:23;text-align:center;vertical-align:middle"><b>Seguro médico / por año</b></td>
        <td style="width:23;text-align:center;vertical-align:middle"><b>Divisa ( USD/RUB)</b></td>
    </tr>
    @for ($i = 0; $i < count($universidad); $i++)
        @if($i%2==0) 
        <tr>
            <td style="background-color:#bfbfbf">{{$universidad[$i]->nombre. " (".$universidad[$i]->abrev.")"}}</td>
            @if(isset($universidad[$i]->costo_vivienda))
                <td style="background-color:#bfbfbf;text-align:center;vertical-align:middle">{{$universidad[$i]->costo_vivienda}}</td>
            @else
                <td style="background-color:#bfbfbf"></td>
            @endif
            @if(isset($universidad[$i]->costo_alimentacion))
                <td style="background-color:#bfbfbf;text-align:center;vertical-align:middle">{{$universidad[$i]->costo_alimentacion}}</td>
            @else
                <td style="background-color:#bfbfbf"></td>
            @endif
            @if(isset($universidad[$i]->costo_seguro))
                <td style="background-color:#bfbfbf;text-align:center;vertical-align:middle">{{$universidad[$i]->costo_seguro}}</td>
            @else
                <td style="background-color:#bfbfbf"></td>
            @endif
            @if(isset($universidad[$i]->costo_divisa))
                <td style="background-color:#bfbfbf;text-align:center;vertical-align:middle">{{$universidad[$i]->costo_divisa}}</td>
            @else
                <td style="background-color:#bfbfbf"></td>
            @endif
        </tr>
        @else
        <tr>
            <td>{{$universidad[$i]->nombre. " (".$universidad[$i]->abrev.")"}}</td>
            @if(isset($universidad[$i]->costo_vivienda))
                <td style="text-align:center;vertical-align:middle">{{$universidad[$i]->costo_vivienda}}</td>
            @else
                <td></td>
            @endif
            @if(isset($universidad[$i]->costo_alimentacion))
                <td style="text-align:center;vertical-align:middle">{{$universidad[$i]->costo_alimentacion}}</td>
            @else
                <td ></td>
            @endif
            @if(isset($universidad[$i]->costo_seguro))
                <td style="text-align:center;vertical-align:middle">{{$universidad[$i]->costo_seguro}}</td>
            @else
                <td ></td>
            @endif
            @if(isset($universidad[$i]->costo_divisa))
                <td style="text-align:center;vertical-align:middle">{{$universidad[$i]->costo_divisa}}</td>
            @else
                <td ></td>
            @endif
        </tr>
        @endif
    @endfor
</table>