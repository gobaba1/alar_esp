<table>
    <tr>
        <td width="24">Tipo de estudio</td>
        <td width="24">Rama</td>
        <td width="24">Perfil</td>
        <td width="24">Código</td>
        <td width="24">Programa</td>
        <td width="24">Duración</td>
        <td width="24">Costo</td>
        <td width="24">Idioma</td>
        <td width="24">Beca</td>
        <td width="24">Verificación</td>
        <td width="10"></td>
        <td width="33">Lista de tipo de estudio</td>
        <td width="10"></td>
        <td width="24">Lista de ramas</td>
        <td width="10"></td>
        <td width="33">Lista de perfiles</td>
    </tr>
    @for ($i = 0; $i < $mayor_array; $i++)
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            @if($i==0)
                <td>{{ $formula }}</td>
            @else
                <td></td>
            @endif
            <td></td>
            <td>{{ isset($tipo_estudios[$i]->descripcion) ? $tipo_estudios[$i]->descripcion : '' }}</td>
            <td></td>
            <td>{{ isset($ramas[$i]->descripcion) ? $ramas[$i]->descripcion : '' }}</td>
            <td></td>
            <td>{{ isset($perfiles[$i]->descripcion) ? $perfiles[$i]->descripcion : '' }}</td>
        </tr>
    @endfor
</table>