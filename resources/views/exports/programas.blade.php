<table>
    <tr>
        <td colspan="4" ></td>
        <td >
            <img src="assets/common_img/alar_logo.png" width="145px" valign="middle">
        </td>
        <td colspan="2"></td>
    </tr>
</table>
<table>
    <td colspan="3"   style="background-color: #fab616 ;text-align:center"><b>Presupuesto Anual</b></td>
    <td></td>
    <td colspan="13"   style="background-color: #fab616 ;text-align:center"><b>Cronograma Anual</b></td>
</table>
<table>
    <thead>
        <tr >
            <th width="62"></th>
        </tr>
        <tr>
            <th colspan="2" width="28" style="background-color: #fab616 ;text-align:center"><b>Informacion del programa</b></th>
            <td></td>
            <td></td>
            <!-- Leyenda -->
            <td width="31"></td>
            <!-- <td  style="text-align:center"> <b>LEYENDA</b></td> -->
            <!-- CRONOGRAGAMA -->
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td colspan="5" style="background:#ffe28a;text-align:center">{{$costos_calendario["año"]}}</td>
                    <td colspan="7" style="background:#ddebf7;text-align:center">{{$costos_calendario["año"]+1}}</td>
                @break
                @case("facultad1")
                    <td colspan="12" style="background:#ffe28a;text-align:center">{{$costos_calendario["año"]+1}}</td>
                @break
                @case("facultad2")
                    <td colspan="11" style="background:#ffe28a;text-align:center">{{$costos_calendario["año"]}}</td>
                    <td style="background:#ddebf7;text-align:center">{{$costos_calendario["año"]+1}}</td>
                @break
                @case("facultad3")
                    <td colspan="5" style="background:#ffe28a;text-align:center">{{$costos_calendario["año"]}}</td>
                    <td colspan="7" style="background:#ddebf7;text-align:center">{{$costos_calendario["año"]+1}}</td>
                @break
                @case("carrera_ingles")
                    <td colspan="5" style="background:#ffe28a;text-align:center">{{$costos_calendario["año"]}}</td>
                    <td colspan="7" style="background:#ddebf7;text-align:center">{{$costos_calendario["año"]+1}}</td>
                @break
            @endswitch
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="2" >Tipo de Estudio:{{" ".$info_general->tipo_descripcion}}</td>
            <td></td>
            <td></td>
            <!-- CRONOGRAGAMA -->
            <td></td>
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td></td>
                    <td colspan="10" style="background:#e2efda;text-align:center">Facultad Preparatoria</td>
                    <td colspan="" style="background:#fcffb9;text-align:center" width="12">Vacaciones</td>
                @break
                @case("facultad1")
                    <td colspan="7" style="background:#e2efda;text-align:center">FACULTAD PREPARATORIA</td>
                    <td colspan="" style="background:#fcffb9;text-align:center" width="12">VACACIONES</td>
                    <td colspan="4" style="background:#ffb8be;text-align:center">CARRERA/POSTGRADO</td>
                @break
                @case("facultad2")
                    <td></td>
                    <td colspan="4" style="background:#e2efda;text-align:center">Facultad Preparatoria</td>
                    <td colspan="2" style="background:#fcffb9;text-align:center" width="12">Vacaciones</td>
                    <td colspan="5" style="background:#e2efda;text-align:center">Facultad Preparatoria</td>
                @break
                @case("facultad3")
                    <td></td>
                    <td colspan="10" style="background:#e2efda;text-align:center">Facultad Preparatoria</td>
                    <td style="background:#fcffb9;text-align:center" width="12">Vacaciones</td>
                @break
                @case("carrera_ingles")
                    <td></td>
                    <td colspan="10" style="background:#ffb8be;text-align:center">CARRERA/POSTGRADO</td>
                    <td colspan="1" style="background:#fcffb9;text-align:center" width="12">Vacaciones</td>
                @break
            @endswitch
        </tr>
        <tr>
            @if ($mostrar["columna"]==true)
                <td colspan="2">Rama :{{" ".$info_general->rama}}</td>
            @else
            <td colspan="2">Rama :</td>
            @endif
            <td></td>
            <td></td>
            <!-- CRONOGRAGAMA -->
            <td></td>
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td ></td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Sept.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Oct.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Nov.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Dic.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Ene.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Feb.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Mar.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Abr.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">May.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Jun.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Jul.</td>
                @break
                @case("facultad1")
                    <td width="13" style="background:#cbf6ff;text-align:center">Ene.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Feb.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Mar.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Abr.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">May.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Jun.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Jul.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Ago.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Sept.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Oct.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Nov.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Dic.</td>
                @break
                @case("facultad2")
                    <td width="13" ></td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Mar.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Abr.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">May.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Jun.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Jul.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Agos.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Sept.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Oct.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Nov.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Dic.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Ene.</td>
                @break
                @case("facultad3")
                    <td width="13" ></td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Sept.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Oct.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Nov.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Dic.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Ene.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Feb.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Mar.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Abr.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">May.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Jun.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Jul.</td>
                @break
                @case("carrera_ingles")
                    <td width="13" ></td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Sept.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Oct.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Nov.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Dic.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Ene.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Feb.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Mar.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Abr.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">May.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Jun.</td>
                    <td width="13" style="background:#cbf6ff;text-align:center">Jul.</td>
                @break
            @endswitch
        </tr>
        <tr>
            <td colspan="2">Perfil :{{" ".$info_general->perfil}}</td>  
            <td></td>
            <td></td>
            <!-- leyenda -->
            @if($mostrar["caso_tabla"]!=='carrera_ingles')
                <td  width="25" style="background:#9fffb2;text-align:center" >Costo facultad Preparatoria</td>
            @else
                <td  style="background:#ff0000;text-align:center" >Costo Carrera</td>
            @endif
            <!-- CRONOGRAGAMA -->
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td></td>
                    <td style="background:#9fffb2;text-align:center">{{number_format($costos_calendario["cal_costo_facultad"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad1")
                    <td style="background:#9fffb2;text-align:center">{{$presupuesto_local["costo_programa"]}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad2")
                    <td></td>
                    <td style="background:#9fffb2;text-align:center">{{number_format($costos_calendario["cal_costo_programa"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td  style="background:#9fffb2;text-align:center">{{number_format($presupuesto_local["costo_programa"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad3")
                    <td></td>
                    <td style="background:#9fffb2;text-align:center" >{{number_format($presupuesto_local["costo_programa"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td ></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("carrera_ingles")
                    <td></td>
                    <td style="background:#ff0000;text-align:center">{{number_format($presupuesto_local["costo_programa"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
            @endswitch
        </tr>
        <tr>
            <td colspan="2">Idioma :{{" ".$info_general->idioma}}</td>
            <td></td>
            <td></td>
            <!-- leyenda -->
            <td  style="background:#41c3ff;text-align:center" width="55">Vivienda universitaria</td>
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td></td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                @break
                @case("facultad1")
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad2")
                    <td></td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                @break
                @case("facultad3")
                    <td></td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                @break
                @case("carrera_ingles")
                    <td></td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                @break
            @endswitch
        </tr>
        <tr>
            <td colspan="2">Código :{{" ".$info_general->codigo_programa}}</td>
            <td></td>
            <td></td>
            <!-- leyenda -->
            <td style="background:#da96ff;text-align:center">Seguro médico</td>
            <!-- CRONOGRAGAMA -->
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td></td>
                    <td style="background:#da96ff;text-align:center">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad1")
                    <td style="background:#da96ff;text-align:center">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad2")
                    <td></td>
                    <td style="background:#da96ff;text-align:center">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad3")
                    <td></td>
                    <td style="background:#da96ff;text-align:center">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("carrera_ingles")
                    <td></td>
                    <td style="background:#da96ff;text-align:center">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
            @endswitch
        </tr>
        <tr>
            <td colspan="2">Nombre del programa :{{" ".$info_general->descripcion_programa}}</td>
            <td></td>
            <td></td>
            <!-- leyenda -->
            <td width="52"  style="background:#d9d9d9;text-align:center">Alimentación, gastos personales,Telefonía celular e Internet</td>
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td></td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                @break
                @case("facultad1")
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                @break
                @case("facultad2")
                    <td></td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                @break
                @case("facultad3")
                    <td></td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                @break
                @case("carrera_ingles")
                    <td></td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center;">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                @break
            @endswitch
        </tr>
        <tr>
            <td colspan="2" ><a href="{{$info_general->url}}">Universidad  :{{" ".$info_general->universidad}}</a></td>
            <td></td>
            <td></td>
            <!-- leyenda -->
            <td style="background:#fbff00;text-align:center">Total mensual</td>
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td></td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma1"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                @break
                @case("facultad1")
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($presupuesto_local["costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad2")
                    <td></td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["cal_costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($presupuesto_local["costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                @break
                @case("facultad3")
                    <td></td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($presupuesto_local["costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                @break
                @case("carrera_ingles")
                    <td></td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($presupuesto_local["costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                @break
            @endswitch
        </tr>
        <tr>    
            <td colspan="2">Ciudad   :{{" ".$info_general->ciudad}}</td>
        </tr>
    </tbody>
</table>
<table>
    <thead>
        <tr>
            <td colspan="3" style="background-color: #fab616 ;text-align:center"><b>Costo en: {{$divisa["nombre"].' ('.$divisa["simbolo"].')'}}</b></td>
            <td></td>
            <td></td>
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td colspan="5" style="background:#ffe28a;text-align:center">{{$costos_calendario["año"]+1}}</td>
                    <td colspan="7" style="background:#ddebf7;text-align:center">{{$costos_calendario["año"]+2}}</td>
                @break
                @case("facultad2")
                    <td colspan="11" style="background:#ffe28a;text-align:center">{{$costos_calendario["año"]+1}}</td>
                    <td style="background:#ddebf7;text-align:center">{{$costos_calendario["año"]+2}}</td>
                @break
                @case("facultad3")
                    <td colspan="6" style="background:#ffe28a;text-align:center">{{$costos_calendario["año"]+1}}</td>
                    <td colspan="7" style="background:#ddebf7;text-align:center">{{$costos_calendario["año"]+2}}</td>
                @break
                @case("carrera_ingles")
                    <td colspan="5" style="background:#ffe28a;text-align:center">{{$costos_calendario["año"]+1}}</td>
                    <td colspan="7" style="background:#ddebf7;text-align:center">{{$costos_calendario["año"]+2}}</td>
                @break
            @endswitch
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="col" style="text-align:center">Descripción</th>
            @if($mostrar["caso_tabla"]!=='carrera_ingles')
                <th scope="col" width="24" style="text-align:center">Facultad Preparatoria</th>
            @endif
            @if ($mostrar["columna"]==true)
                <th scope="col" width="16" style="text-align:center">{{$info_general->tipo_descripcion}}</th>
            @endif
            <td></td>
            <td></td>
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td style="background:#fcffb9;text-align:center" width="12">Vacaciones</td>
                    <td colspan="10" style="background:#ffb8be;text-align:center">Carrera/Postgrado</td>
                    <td colspan="" style="background:#fcffb9;text-align:center" width="12">Vacaciones</td>
                @break
                @case("facultad2")
                    <td></td>
                    <td colspan="5" style="background:#e2efda;text-align:center">FACULTAD PREPARATORIA</td>
                    <td colspan="2" style="background:#fcffb9;text-align:center" width="12">VACACIONES</td>
                    <td colspan="5" style="background:#ffb8be;text-align:center">CARRERA/POSTGRADO</td> 
                @break
                @case("facultad3")
                    <td></td>
                    <td style="background:#fcffb9;text-align:center" width="12">VACACIONES</td>
                    <td colspan="10" style="background:#ffb8be;text-align:center">CARRERA/POSTGRADO</td>
                    <td style="background:#fcffb9;text-align:center" width="12">VACACIONES</td>
                @break
                @case("carrera_ingles")
                    <td></td>
                    <td style="background:#fcffb9;text-align:center" width="12">VACACIONES</td>
                    <td colspan="10" style="background:#ffb8be;text-align:center">CARRERA/POSTGRADO</td>
                    <td style="background:#fcffb9;text-align:center" width="12">VACACIONES</td>
                @break
            @endswitch
        </tr>
        <tr>
            <th scope="row">Estudios (Colegiatura) anual</th>
            @if($mostrar["caso_tabla"]!=='carrera_ingles')
                <td style="text-align:center">{{number_format($presupuesto_local["costo_facultad"],0,'',',')}}</td>
            @endif
            @if ($mostrar["columna"]==true)
                <td style="text-align:center">{{number_format($presupuesto_local["costo_programa"],0,'',',')}}</td>
            @endif
            <td></td>
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td></td>
                    <td style="background:#cbf6ff;text-align:center">Ago.</td>
                    <td style="background:#cbf6ff;text-align:center">Sept.</td>
                    <td style="background:#cbf6ff;text-align:center">Oct.</td>
                    <td style="background:#cbf6ff;text-align:center">Nov.</td>
                    <td style="background:#cbf6ff;text-align:center">Dic.</td>
                    <td style="background:#cbf6ff;text-align:center">Ene.</td>
                    <td style="background:#cbf6ff;text-align:center">Feb.</td>
                    <td style="background:#cbf6ff;text-align:center">Mar.</td>
                    <td style="background:#cbf6ff;text-align:center">Abr.</td>
                    <td style="background:#cbf6ff;text-align:center">May.</td>
                    <td style="background:#cbf6ff;text-align:center">Jun.</td>
                    <td style="background:#cbf6ff;text-align:center">Jul.</td>
                @break
                @case("facultad2")
                    <td></td>
                    <td></td>
                    <td style="background:#cbf6ff;text-align:center">Feb.</td>
                    <td style="background:#cbf6ff;text-align:center">Mar.</td>
                    <td style="background:#cbf6ff;text-align:center">Abr.</td>
                    <td style="background:#cbf6ff;text-align:center">May.</td>
                    <td style="background:#cbf6ff;text-align:center">Jun.</td>
                    <td style="background:#cbf6ff;text-align:center">Jul.</td>
                    <td style="background:#cbf6ff;text-align:center">Ago.</td>
                    <td style="background:#cbf6ff;text-align:center">Sept.</td>
                    <td style="background:#cbf6ff;text-align:center">Oct.</td>
                    <td style="background:#cbf6ff;text-align:center">Nov.</td>
                    <td style="background:#cbf6ff;text-align:center">Dic.</td>
                    <td style="background:#cbf6ff;text-align:center">Ene.</td>
                @break
                @case("facultad3")
                    <td></td>
                    <td></td>
                    <td style="background:#cbf6ff;text-align:center">Ago.</td>
                    <td style="background:#cbf6ff;text-align:center">Sept.</td>
                    <td style="background:#cbf6ff;text-align:center">Oct.</td>
                    <td style="background:#cbf6ff;text-align:center">Nov.</td>
                    <td style="background:#cbf6ff;text-align:center">Dic.</td>
                    <td style="background:#cbf6ff;text-align:center">Ene.</td>
                    <td style="background:#cbf6ff;text-align:center">Feb.</td>
                    <td style="background:#cbf6ff;text-align:center">Mar.</td>
                    <td style="background:#cbf6ff;text-align:center">Abr.</td>
                    <td style="background:#cbf6ff;text-align:center">May.</td>
                    <td style="background:#cbf6ff;text-align:center">Jun.</td>
                    <td style="background:#cbf6ff;text-align:center">Jul.</td>
                @break
                @case("carrera_ingles")
                    <td></td>
                    <td></td>
                    <td style="background:#cbf6ff;text-align:center">Ago.</td>
                    <td style="background:#cbf6ff;text-align:center">Sept.</td>
                    <td style="background:#cbf6ff;text-align:center">Oct.</td>
                    <td style="background:#cbf6ff;text-align:center">Nov.</td>
                    <td style="background:#cbf6ff;text-align:center">Dic.</td>
                    <td style="background:#cbf6ff;text-align:center">Ene.</td>
                    <td style="background:#cbf6ff;text-align:center">Feb.</td>
                    <td style="background:#cbf6ff;text-align:center">Mar.</td>
                    <td style="background:#cbf6ff;text-align:center">Abr.</td>
                    <td style="background:#cbf6ff;text-align:center">May.</td>
                    <td style="background:#cbf6ff;text-align:center">Jun.</td>
                    <td style="background:#cbf6ff;text-align:center">Jul.</td>
                @break
            @endswitch
        </tr>
        <tr>
            <th scope="row">Vivienda universitaria anual</th>
            @if($mostrar["caso_tabla"]!=='carrera_ingles')
                <td style="text-align:center">{{number_format($presupuesto_local["costo_vivienda"],0,'',',')}}</td>
            @endif
            @if ($mostrar["columna"]==true)
                <td style="text-align:center">{{number_format($presupuesto_local["costo_vivienda"],0,'',',')}}</td>
            @endif
            <!-- leyenda -->
            <td></td>
            @if($mostrar["caso_tabla"]!=='carrera_ruso/español' && $mostrar["caso_tabla"]!=='carrera_ingles')
                <td></td>
                <td></td>
            @elseif($mostrar["caso_tabla"]=='carrera_ingles')
                <td></td>
                <td  style="background:#ff0000;text-align:center" >Costo Carrera</td>
            @else
                <td  style="background:#ff0000;text-align:center" >Costo Carrera</td>
            @endif
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td style="background:#ff0000;text-align:center">{{number_format($costos_calendario["cal_costo_programa"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="background:#ff0000;text-align:center">{{number_format($costos_calendario["cal_costo_programa"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad2")
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad3")
                    <td></td>
                    <td ></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("carrera_ingles")
                    <td style="background:#ff0000;text-align:center">{{number_format($costos_calendario["cal_costo_programa"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="background:#ff0000;text-align:center">{{number_format($costos_calendario["cal_costo_programa"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
            @endswitch
        </tr>
        <tr>
            <th scope="row">Seguro médico anual</th>
            @if($mostrar["caso_tabla"]!=='carrera_ingles')
                <td style="text-align:center" >{{number_format($presupuesto_local["costo_seguro"],0,'',',')}}</td>
            @endif
            @if ($mostrar["columna"]==true)
                <td style="text-align:center">{{number_format($presupuesto_local["costo_seguro"],0,'',',')}}</td>
            @endif
            <!-- leyenda -->
            <td></td>
            @if($mostrar["caso_tabla"]!=='carrera_ruso/español')
                <td></td>
            @endif
            @if($mostrar["caso_tabla"]!=='facultad1')
                <td  style="background:#41c3ff;text-align:center" width="55">Vivienda universitaria</td>
            @endif
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                @break
                @case("facultad2")
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad3")
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("carrera_ingles")
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                    <td style="background:#41c3ff;text-align:center">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                @break
            @endswitch
        </tr>
        <tr>
            <th scope="row">Alimentación, gastos personales,Telefonía celular e Internet anual</th>
            @if($mostrar["caso_tabla"]!=='carrera_ingles')
                <td style="text-align:center">{{number_format($presupuesto_local["costo_alimentacion"],0,'',',')}}</td>
            @endif
            @if ($mostrar["columna"]==true)
                <td style="text-align:center">{{number_format($presupuesto_local["costo_alimentacion"],0,'',',')}}</td>
            @endif
            <!-- leyenda -->
            <td></td>
            @if($mostrar["caso_tabla"]!=='carrera_ruso/español')
                <td></td>
            @endif
            @if($mostrar["caso_tabla"]!=='facultad1')
                <td style="background:#da96ff;text-align:center">Seguro médico</td>
            @endif
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td></td>
                    <td style="background:#da96ff;text-align:center">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad2")
                    <td></td>
                    <td style="background:#da96ff;text-align:center">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad3")
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td ></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("carrera_ingles")
                    <td></td>
                    <td style="background:#da96ff;text-align:center">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
            @endswitch
        </tr>
        <tr>
            <th scope="row">Total</th>
            @if($mostrar["caso_tabla"]!=='carrera_ingles')
                <td style="text-align:center" >{{$divisa["simbolo"].number_format($presupuesto_local["total_facultad"],0,'',',')}}</td>
            @endif
            @if ($mostrar["columna"]==true)
                <td style="text-align:center">{{$divisa["simbolo"].number_format($presupuesto_local["total_programas"],0,'',',')}}</td>
            @endif
            <!-- leyenda -->
            <td></td>
            @if($mostrar["caso_tabla"]!=='carrera_ruso/español')
                <td></td>
            @endif
            @if($mostrar["caso_tabla"]!=='facultad1')
                <td width="52"  style="background:#d9d9d9;text-align:center">Alimentación, gastos personales,Telefonía celular e Internet</td>
            @endif
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                @break
                @case("facultad2")
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad3")
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("carrera_ingles")
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                    <td style="background:#d9d9d9;text-align:center">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                @break
            @endswitch
        </tr>
        <tr>
            @if($mostrar["caso_tabla"]!=='facultad1')
                <!-- leyenda -->
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="background:#fbff00;text-align:center">Total mensual</td>
            @endif
            @switch($mostrar["caso_tabla"])
                @case("carrera_ruso/español")
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma3"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma4"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma3"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                @break
                @case("facultad2")
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma4"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("facultad3")
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td></td>
                    <td></td>
                    <td ></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @break
                @case("carrera_ingles")
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma3"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma4"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma3"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                    <td style="background:#fbff00;text-align:center">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                @break  
            @endswitch
        </tr>
    </tbody>
</table>
<!-- INFO ALAR -->
<table> 
    <tr >
        <td colspan="7" height="60" style="background-color:#0d225d;text-align:center;color:#ffffff">
            <b>ALAR - Asociación Latinoamericano Rusa <br> LIMA - QUITO - LA PAZ - MEXICO - BOGOTA - SAO PAULO
            Telf +51(1) 222-4612; cel. +51 936-017-225; e-mail: info@universidades-rusia.com</b>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="background-color:#0d225d;vertical-align:top;text-align:right;color:#ffffff"><a href="https://www.universidades-rusia.com"><b>www.universidades-rusia.com</b></a></td>
        <td width="1" style="background-color:#0d225d;color:#ffffff;text-align:center"><b>|</b></td>
        <td colspan="3" style="background-color:#0d225d;vertical-align:top;color:#ffffff"><a href="https://www.russia.net.br"><b>www.rusia.net.br</b></a></td>
    </tr>
</table>
<!-- <table>
    <tr>
        <td colspan="17" style="background-color:#0d225d;text-align:center;color:#ffffff">
            <b>ALAR - Asociación Latinoamericano Rusa </b>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="background-color:#0d225d;text-align:center;color:#ffffff"></td>
        <td width="0.75"></td>
        <td>
            <a href="https://www.universidades-rusia.com/inscripciones/" style=" margin-left:300;margin-top:-220px"><img src="assets/common_img/suscribir.png" width="250px"  style=" margin-left:300;margin-top:8px"></a>
        </td>
    </tr>
</table> -->