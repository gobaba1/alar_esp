<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ALAR</title>

    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('fontawesome/css/all.css') }}">
    <!-- {{-- <link rel="stylesheet" href="/css/home.blade.css"> --}} -->
</head>

<body class="hold-transition sidebar-mini sidebar-collapse">
    <div class="wrapper" id="app">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>

            </ul>

            <!-- SEARCH FORM -->
            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search"
                        aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>


            <!-- Right navbar links -->
            <notificaciones :id_user="{{auth()->id()}}"></notificaciones>

        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <main>
                <!-- Brand Logo -->
                <a href="{{url('inicio')}}" class="brand-link">
                    <img src="./assets/common_img/alar_sidebar.png" class="brand-image img-circle elevation-3"
                        style="opacity: .8">
                    <span class="brand-text font-weight-light">ALAR</span>
                </a>
                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img id="img_profile" @if ($foto_perfil) src="{{url('/miniatura/'.auth()->id().'.jpg')}}"
                                @else src="assets/common_img/defaultUser.png" @endif class="img-circle elevation-2">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block">{{ Auth::user()->name }} </a>
                        </div>
                    </div>
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                            data-accordion="false">
                            @if(@Auth::user()->hasRole('USER_ROLE'))
                            <li class="nav-item">
                                <a href="{{url('inicio')}}" class="nav-link">
                                    <i class="nav-icon fa fa-home"></i>
                                    <p>
                                        Inicio
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('perfil')}}" class="nav-link">
                                    <i class="nav-icon fa fa-user"></i>
                                    <p>
                                        Mi Perfil
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('mi_inscripcion')}}" class="nav-link">
                                    <i class="nav-icon fa fa-edit"></i>
                                    <p>
                                        Mi inscripción
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{url('mi_preparacion')}}" class="nav-link">
                                    <i class="nav-icon fa fa-book"></i>
                                    <p>
                                        Mi Preparación para<br> el viaje
                                    </p>
                                </a>
                            </li>
                            @elseif(@Auth::user()->hasRole('ADMIN_ROLE'))
                            <li class="nav-item">
                                <a href="{{ url('inicio') }}" class="nav-link">
                                    <i class="nav-icon fa fa-book"></i>
                                    <p>
                                        Usuarios
                                        <span class="right badge badge-danger">{{count($users)}}</span>
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('estadisticas') }}" class="nav-link">
                                    <i class="fas fa-chart-bar nav-icon"></i>
                                    <p>
                                        Estadisticas
                                    </p>
                                </a>}
                            </li>
                            @elseif(@Auth::user()->hasRole('CURSO_RUSO'))
                            <li class="nav-item">
                                <a href="{{url('mi_preparacion')}}" class="nav-link">
                                    <i class="nav-icon fa fa-book"></i>
                                    <p>
                                        Mi Preparación para<br> el viaje
                                    </p>
                                </a>
                            </li>
                            @elseif(@Auth::user()->hasRole('ADMINISTRADOR_PROGRAMAS'))
                            <li class="nav-item" @click="menu=0">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-link"></i>
                                    <p>
                                        Programas
                                        <span class="right badge badge-danger">{{$count_programas}}</span>
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item" @click="menu=1">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-link"></i>
                                    <p>
                                        Universidades
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item" @click="menu=2">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-link"></i>
                                    <p>
                                        Ciudades
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item" @click="menu=3">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-link"></i>
                                    <p>
                                        Tipo de Estudios
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item" @click="menu=4">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-link"></i>
                                    <p>
                                        Ramas
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item" @click="menu=5">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-link"></i>
                                    <p>
                                        Perfiles
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item" @click="menu=9">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-link"></i>
                                    <p>
                                        Costos de vida
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-chart-pie"></i>
                                    <p>
                                        Subida Masica
                                        <i class="nav-icon right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview" style="display: none;" @click="menu=7">
                                    <li class="nav-item" >
                                        <a href="#" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>Programas</p>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="nav nav-treeview" style="display: none;" @click="menu=8">
                                    <li class="nav-item" >
                                        <a href="#" class="nav-link">
                                            <i class="nav-icon far fa-circle nav-icon"></i>
                                            <p>Costos de vida</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                            <!-- Add icons to the links using the .nav-icon class
                        with font-awesome or any other icon font library -->
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt nav-icon"></i>
                                    <p>
                                        Cerrar Sesión
                                    </p>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </main>
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <main class="pl-2 pr-2 py-3 pb-3">
                @yield('content')
            </main>
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            {{-- <div class="float-right d-none d-sm-inline">
                Anything you want
            </div> --}}
            <!-- Default to the left -->
            <strong>2014-2020 <a href="https://www.universidades-rusia.com/">ALAR</a>.</strong> Todos los derechos
            reservados.
        </footer>
    </div>
    <!-- ./wrapper -->

    <script src="{{ URL::asset('js/app.js') }}"></script>
    <script src="{{ URL::asset('js/admin_alar.js') }}"></script>

    <script src="{{ URL::asset('js/preparacion.js') }}"></script>
    <script src="{{ URL::asset('js/zonaUsuario/inicio.js') }}"></script>
    @stack('perfil')
    @stack('mi_inscripcion')
    <!-- <script src="https://use.fontawesome.com/08d4e36afc.js"></script> -->

    @if (session('email_recuperar_password'))
        <script type="text/javascript">
            $(document).ready(function() {
                setTimeout(() => {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top',
                        showConfirmButton: false,
                        timer: 10000,
                        timerProgressBar: false,
                        customClass: {
                            container: 'adelante'
                        },
                        showClass: {
                            popup: "animate__animated animate__fadeInRight"
                        },
                        hideClass: {
                            popup: "animate__animated animate__fadeOutRight"
                        },
                        onOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: '{{session('icon')}}',
                        title: '{{ session('email_recuperar_password') }}'
                    })

                }, 1000);
            });
        </script>
    @endif
</body>

</html>
