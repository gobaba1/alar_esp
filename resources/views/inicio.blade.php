@extends('layouts.master')
@section('content')
@if(@Auth::user()->hasRole('USER_ROLE'))
<section class="content" style="background-color:white;border-radius:10px">
        @if(null !== session('status'))
        <div class="alert alert-warning animated slideInDown">
            {{ session('status') }}
        </div>
        @endif
    <div class="row">
        <div class="container pt-4 pb-4">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 ">
                    <a class="card" href="{{url('perfil')}}">
                        <div class="icon d-flex justify-content-center pt-3"><i class="text-center fa fa-user "></i></div>
                        <h3 class="text-center" style="font-size: 1.5rem;margin: 4px;">Mi perfil</h3>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 " id="inscripcion" @if ($states[0]->stateProfile !== '1') onclick="noclick(event,1)" @endif>
                    <a class="card" @if ($states[0]->stateProfile !== '1') href="#" style="cursor: no-drop" @else href="{{url('mi_inscripcion')}}" @endif>
                        <div class="icon d-flex justify-content-center pt-3">
                            <i class="text-center fa fa-edit"  @if ($states[0]->stateProfile !== '1') style="background: gray" @endif></i>
                        </div>
                        <h3 class="text-center" style="font-size: 1.5rem;margin: 4px;">Mi inscripción</h3>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 " id="preparacion" >
                    <a class="card" href="{{url('mi_preparacion')}}" >
                        <div class="icon d-flex justify-content-center pt-3">
                            <i class="text-center fa fa-book"></i>
                        </div>
                        <h3 class="text-center" style="font-size: 1.5rem;margin: 4px;">Mi preparación para el viaje</h3>
                    </a>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="container">
                <div class="timeline-item">
                    <h3 class="timeline-header text-center pb-2">Conoce en 1 minuto cómo funciona tu carpeta personal</h3>
                    <div class="timeline-body">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/DD6l9-w-C5U" frameborder="0" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@elseif(@Auth::user()->hasRole('ADMIN_ROLE'))
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Listado de Usuarios</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="table_users" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 20%">Nombres</th>
                                <th style="width: 20%">Email</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}} {{$user->lastname}}</td>
                                <td>{{$user->email}}</td>
                                <td class="d-flex justify-content-center" >
                                    <button type="button" class="btn btn-sm btn-primary mr-1" data-toggle="modal" data-target="#modalVerDoc" onclick="verDoc({{$user->id}})">
                                        <i class="fas fa-file-alt"></i> Ver Documentos
                                    </button>
                                    <button type="button" class="btn btn-sm btn-default enviar_doc_to_user mr-1" data-toggle="modal" data-target="#modalEnviarDoc" data-id="{{$user->id}}">
                                        <i class="fas fa-file-upload"></i> Enviar Documentos
                                    </button>
                                    <button type="button" class="btn btn-sm btn-info see_courses" data-toggle="modal" data-target="#modalActivarCursos" data-id="{{$user->id}}">
                                        <i class="fas fa-chalkboard-teacher"></i> Activar Cursos
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombres</th>
                                <th>Email</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    {{-- MODAL VER DOCUMENTOS --}}
    <div class="modal fade" id="modalVerDoc" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h1><i class="fas fa-file-alt"></i> Ver Documentos</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <table id="table_docs" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Documento</th>
                            <th>Estado</th>
                            <th>Ver</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Documento</th>
                            <th>Estado</th>
                            <th>Ver</th>
                            <th>Acción</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary">Guardar cambios</button>
            </div>
          </div>
        </div>
    </div>
    {{-- MODAL VER DOCUMENTOS --}}
    {{-- MODAL ENVIAR DOCUMENTOS --}}
    <div class="modal fade" id="modalEnviarDoc" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
                <h1><i class="fas fa-file-upload"></i> Enviar Documentos</h1>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="/enviar_doc_to_user" method="POST">
                <div class="modal-body doc_to_user">
                    @csrf
                    <div class="form-group">
                        <label for="tipo_documento">Tipo de documento</label>
                        <input type="text" class="form-control" name="tipo_documento" required>
                        <input type="hidden" class="u538" name="u53810">
                    </div>
                    <div class="form-group">
                        <label for="url_documento">URL del documento</label>
                        <input type="text" class="form-control" name="url_documento" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Enviar cambios</button>
                </div>
            </form>
          </div>
        </div>
    </div>
    {{-- MODAL ENVIAR DOCUMENTOS --}}
    <!-- {{-- MODAL ACTIVAR CURSOS --}} -->
    <div class="modal fade" id="modalActivarCursos" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h1>
                        <i class="fas fa-chalkboard-teacher"></i>
                        Activar Cursos
                    </h1>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="row">
                            <div class="col-12">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item active">
                                        <a
                                            class="nav-link"
                                            id="ver_clases-tab"
                                            data-toggle="tab"
                                            href="#ver_clases"
                                            role="tab"
                                        >
                                            <h5>
                                                <i class="fas fa-chalkboard"></i>
                                                Clases activas
                                            </h5>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a
                                            class="nav-link"
                                            id="ADD_clases-tab"
                                            data-toggle="tab"
                                            href="#ADD_clases"
                                            role="tab"
                                            aria-controls="ADD_clases"
                                            aria-selected="false"
                                        >
                                            <h5>
                                                <i class="fas fa-plus-square"></i>
                                                Activar Clases
                                            </h5>
                                        </a>
                                    </li>
                                    <li class="nav-item see_modules">
                                        <a
                                            class="nav-link"
                                            id="ADD_modules-tab"
                                            data-toggle="tab"
                                            href="#ADD_modules"
                                            role="tab"
                                            aria-controls="ADD_modules"
                                            aria-selected="false"
                                        >
                                            <h5>
                                                <i class="fas fa-folder-plus"></i>
                                                Activar Módulos
                                            </h5>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="ver_clases" role="tabpanel" aria-labelledby="ver_clases-tab" >
                                        <div class="clases-activas container mt-2" ></div>
                                        <div class="container">
                                            <button type="button" class="btn btn-sm btn-danger float-right mt-2 mb-2 save_clases" >
                                                <i class="fas fa-trash"></i> Remover clases
                                            </button>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade container" id="ADD_clases" role="tabpanel" aria-labelledby="ADD_clases-tab" >
                                        <div class="container mt-3 mb-3">
                                            <label>Seleccione la(s) clase(s) a activar:</label >
                                            <select class="select2 " multiple="multiple" data-placeholder="Seleccione la(s) clase(s)"style="width: 100%;"data-id="">
                                                @foreach ($videos as $video)
                                                <option value="{{$video->id_video}}" >{{$video->abrModulo}} - {{$video->Title}}</option>
                                                @endforeach
                                            </select>
                                            <input type="hidden" id="id_alumno" />
                                            <br />
                                            <hr />
                                            <button type="button" class="btn btn-sm btn-primary float-right mb-2 add_clases">
                                                <i class="fas fa-plus-square"></i> Agregar Clases
                                            </button>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade mt-2 container" id="ADD_modules" role="tabpanel" aria-labelledby="ADD_modules-tab">
                                        <div class="s_mod">

                                        </div>
                                        <br />
                                        <hr />
                                        <button type="button" class="btn btn-sm btn-primary float-right mb-2 add_modulos">
                                            <i class="fas fa-folder-plus"></i> Agregar Módulo
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- {{-- MODAL ACTIVAR CURSOS --}} -->

</section>
<!-- /.content -->
@elseif(@Auth::user()->hasRole('ADMINISTRADOR_PROGRAMAS'))
<section class="content" style="background-color:white;border-radius:10px">
    <template v-if="menu==0">
        <programas-admin></programas-admin>
    </template>
    <template v-if="menu==1">
        <universidades-admin></universidades-admin>
    </template>
    <template v-if="menu==2">
        <ciudades-admin></ciudades-admin>
    </template>
    <template v-if="menu==3">
        <tipo-estudios-admin></tipo-estudios-admin>
    </template>
    <template v-if="menu==4">
        <rama-admin></rama-admin>
    </template>
    <template v-if="menu==5">
        <perfil-admin></perfil-admin>
    </template>
    <template v-if="menu==7">
        <subidamasiva-admin></subidamasiva-admin>
    </template>
    <template v-if="menu==9">
        <costo_vida_panel></costo_vida_panel>
    </template>
    <template v-if="menu==8">
        <costo_vida></costo_vida>
    </template>
</section>
<!-- @elseif(@Auth::user()->hasRole('CURSO_RUSO'))
<section class="content" style="background-color:white;border-radius:10px">
    @extends('mi_preparacion')
</section>
@endif -->
@endsection
