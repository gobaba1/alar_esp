<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title>Presupuesto anual</title>
        <style>
            .page-break {
                page-break-after: always;
            }
        </style>
        <style>
            .meses{
                background:rgb(203,246,255);
                text-align:center
            }
            /* test */
            .corner-widget-header-text {
                color: red;
            }
            footer {
                position: fixed; 
                margin:0cm;
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 1cm;
            }
            header {
                position: fixed;
                top: 0cm;
                left: 0cm;
                right: 0cm;
                height: 2cm;
            }
            .leyenda{
                display:inline-block;
                margin-left:6px;
                width:50px;
                height:10px;
            }
            .costo_carrera{
                background:red;
                font-weight:bold;
                font-size:13px;
            }
            .costo_preparatoria{
                background:#9fffb2;
                font-weight:bold;
                font-size:13px;
            }
            .costo_vivienda{
                background:#41c3ff;
                font-weight:bold;
                font-size:13px;
            }
            .costo_seguro{
                background:#da96ff;
                font-weight:bold;
                font-size:13px;
            }
            .costo_alimentacion{
                background:#d9d9d9;
                font-weight:bold;
                font-size:13px;
            }
            .total_mensual{
                font-weight:bold;
                background:#fbff00;
                font-size:14px;
            }
            /* CLASES BOOTSTRAP */
            .row_boots{
                margin-right: -15px;
                margin-left: -15px;
            }
            /*!
            */:root{--blue:#007bff;--indigo:#6610f2;--purple:#6f42c1;--pink:#e83e8c;--red:#dc3545;--orange:#fd7e14;--yellow:#ffc107;--green:#28a745;--teal:#20c997;--cyan:#17a2b8;--white:#fff;--gray:#6c757d;--gray-dark:#343a40;--primary:#007bff;--secondary:#6c757d;--success:#28a745;--info:#17a2b8;--warning:#ffc107;--danger:#dc3545;--light:#f8f9fa;--dark:#343a40;--breakpoint-xs:0;--breakpoint-sm:576px;--breakpoint-md:768px;--breakpoint-lg:992px;--breakpoint-xl:1200px;--font-family-sans-serif:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";--font-family-monospace:SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace}*,::after,::before{box-sizing:border-box}html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:transparent}header{display:block}body{margin:0;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}[tabindex="-1"]:focus:not(:focus-visible){outline:0!important}h1{margin-top:0;margin-bottom:.5rem}p{margin-top:0;margin-bottom:1rem}strong{font-weight:bolder}img{vertical-align:middle;border-style:none}table{border-collapse:collapse}th{text-align:inherit}[type=button]:not(:disabled),[type=reset]:not(:disabled),[type=submit]:not(:disabled),button:not(:disabled){cursor:pointer}::-webkit-file-upload-button{font:inherit;-webkit-appearance:button}h1{margin-bottom:.5rem;font-weight:500;line-height:1.2}h1{font-size:2.5rem}.row{display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.col-sm-12{position:relative;width:100%;padding-right:15px;padding-left:15px}@media (min-width:576px){.col-sm-12{-ms-flex:0 0 100%;flex:0 0 100%;max-width:100%}}.table{width:100%;margin-bottom:1rem;color:#212529}.table td,.table th{padding:.75rem;vertical-align:top;border-top:1px solid #dee2e6}.table thead th{vertical-align:bottom;border-bottom:2px solid #dee2e6}.table-sm td,.table-sm th{padding:.3rem}.custom-control-input.is-valid:focus:not(:checked)~.custom-control-label::before,.was-validated .custom-control-input:valid:focus:not(:checked)~.custom-control-label::before{border-color:#28a745}.custom-control-input.is-invalid:focus:not(:checked)~.custom-control-label::before,.was-validated .custom-control-input:invalid:focus:not(:checked)~.custom-control-label::before{border-color:#dc3545}.btn-primary:not(:disabled):not(.disabled).active,.btn-primary:not(:disabled):not(.disabled):active{color:#fff;background-color:#0062cc;border-color:#005cbf}.btn-primary:not(:disabled):not(.disabled).active:focus,.btn-primary:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(38,143,255,.5)}.btn-secondary:not(:disabled):not(.disabled).active,.btn-secondary:not(:disabled):not(.disabled):active{color:#fff;background-color:#545b62;border-color:#4e555b}.btn-secondary:not(:disabled):not(.disabled).active:focus,.btn-secondary:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(130,138,145,.5)}.btn-success:not(:disabled):not(.disabled).active,.btn-success:not(:disabled):not(.disabled):active{color:#fff;background-color:#1e7e34;border-color:#1c7430}.btn-success:not(:disabled):not(.disabled).active:focus,.btn-success:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(72,180,97,.5)}.btn-info:not(:disabled):not(.disabled).active,.btn-info:not(:disabled):not(.disabled):active{color:#fff;background-color:#117a8b;border-color:#10707f}.btn-info:not(:disabled):not(.disabled).active:focus,.btn-info:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(58,176,195,.5)}.btn-warning:not(:disabled):not(.disabled).active,.btn-warning:not(:disabled):not(.disabled):active{color:#212529;background-color:#d39e00;border-color:#c69500}.btn-warning:not(:disabled):not(.disabled).active:focus,.btn-warning:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(222,170,12,.5)}.btn-danger:not(:disabled):not(.disabled).active,.btn-danger:not(:disabled):not(.disabled):active{color:#fff;background-color:#bd2130;border-color:#b21f2d}.btn-danger:not(:disabled):not(.disabled).active:focus,.btn-danger:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(225,83,97,.5)}.btn-light:not(:disabled):not(.disabled).active,.btn-light:not(:disabled):not(.disabled):active{color:#212529;background-color:#dae0e5;border-color:#d3d9df}.btn-light:not(:disabled):not(.disabled).active:focus,.btn-light:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(216,217,219,.5)}.btn-dark:not(:disabled):not(.disabled).active,.btn-dark:not(:disabled):not(.disabled):active{color:#fff;background-color:#1d2124;border-color:#171a1d}.btn-dark:not(:disabled):not(.disabled).active:focus,.btn-dark:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(82,88,93,.5)}.btn-outline-primary:not(:disabled):not(.disabled).active,.btn-outline-primary:not(:disabled):not(.disabled):active{color:#fff;background-color:#007bff;border-color:#007bff}.btn-outline-primary:not(:disabled):not(.disabled).active:focus,.btn-outline-primary:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(0,123,255,.5)}.btn-outline-secondary:not(:disabled):not(.disabled).active,.btn-outline-secondary:not(:disabled):not(.disabled):active{color:#fff;background-color:#6c757d;border-color:#6c757d}.btn-outline-secondary:not(:disabled):not(.disabled).active:focus,.btn-outline-secondary:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(108,117,125,.5)}.btn-outline-success:not(:disabled):not(.disabled).active,.btn-outline-success:not(:disabled):not(.disabled):active{color:#fff;background-color:#28a745;border-color:#28a745}.btn-outline-success:not(:disabled):not(.disabled).active:focus,.btn-outline-success:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(40,167,69,.5)}.btn-outline-info:not(:disabled):not(.disabled).active,.btn-outline-info:not(:disabled):not(.disabled):active{color:#fff;background-color:#17a2b8;border-color:#17a2b8}.btn-outline-info:not(:disabled):not(.disabled).active:focus,.btn-outline-info:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(23,162,184,.5)}.btn-outline-warning:not(:disabled):not(.disabled).active,.btn-outline-warning:not(:disabled):not(.disabled):active{color:#212529;background-color:#ffc107;border-color:#ffc107}.btn-outline-warning:not(:disabled):not(.disabled).active:focus,.btn-outline-warning:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(255,193,7,.5)}.btn-outline-danger:not(:disabled):not(.disabled).active,.btn-outline-danger:not(:disabled):not(.disabled):active{color:#fff;background-color:#dc3545;border-color:#dc3545}.btn-outline-danger:not(:disabled):not(.disabled).active:focus,.btn-outline-danger:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(220,53,69,.5)}.btn-outline-light:not(:disabled):not(.disabled).active,.btn-outline-light:not(:disabled):not(.disabled):active{color:#212529;background-color:#f8f9fa;border-color:#f8f9fa}.btn-outline-light:not(:disabled):not(.disabled).active:focus,.btn-outline-light:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(248,249,250,.5)}.btn-outline-dark:not(:disabled):not(.disabled).active,.btn-outline-dark:not(:disabled):not(.disabled):active{color:#fff;background-color:#343a40;border-color:#343a40}.btn-outline-dark:not(:disabled):not(.disabled).active:focus,.btn-outline-dark:not(:disabled):not(.disabled):active:focus{box-shadow:0 0 0 .2rem rgba(52,58,64,.5)}.custom-control-input:focus:not(:checked)~.custom-control-label::before{border-color:#80bdff}.custom-control-input:not(:disabled):active~.custom-control-label::before{color:#fff;background-color:#b3d7ff;border-color:#b3d7ff}.close:not(:disabled):not(.disabled):focus,.close:not(:disabled):not(.disabled):hover{opacity:.75}@supports ((position:-webkit-sticky) or (position:sticky)){}.text-center{text-align:center!important}@media print{*,::after,::before{text-shadow:none!important;box-shadow:none!important}thead{display:table-header-group}img,tr{page-break-inside:avoid}p{orphans:3;widows:3}@page{size:a3}body{min-width:992px!important}.table{border-collapse:collapse!important}.table td,.table th{background-color:#fff!important}}

            .pen-box {
                background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NjU3QTRBQUI2OTA1MTFFNDkwOTFENUQyMzA3MTQzODMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NjU3QTRBQUM2OTA1MTFFNDkwOTFENUQyMzA3MTQzODMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo2NTdBNEFBOTY5MDUxMUU0OTA5MUQ1RDIzMDcxNDM4MyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo2NTdBNEFBQTY5MDUxMUU0OTA5MUQ1RDIzMDcxNDM4MyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PvhzG8UAAAAySURBVHjaYjz86FMaAwLMspHlBTOYGHAAFnSBI48/wyVmIYnDjWUCmQkzFxngtAMgwACAtAhzrNNqnQAAAABJRU5ErkJggg==);
                border: 1px solid #c9dbe6;
                margin: 0 0 16px;
                padding: 0;
            }
            .pen-box p{
                margin: 6px;
            }
            .pen-box p:last-child {
                margin-bottom: 0;
            }
            </style>
    </head>
    <body face="Arial" style="margin-left: 3%; margin-right: 3%;rotate(-90deg)">
        <header>
                <h1 style="color: rgb(13, 34, 93) !important;font-family:Arial,sans-serif !important">Presupuesto</h1>
                <img src="https://universidades-rusia.com/programas/assets/common_img/alar_logo.png" width="250px"  style=" margin-left:76%; margin-top: -75px">
        </header>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-sm-12">
                <div class="pen-box">
                    <p  style="font-style: Arial; font-size: 1.5rem">
                        <u><strong>Información del programa</strong></u>
                    </p>
                    <p>
                        Fecha y Hora del presupuesto :{{" ".$fecha_proforma}}<br>
                        Tipo de estudio      :{{ " ".$info_general->tipo_descripcion}}<br>
                        @if ($mostrar["columna"]==true)
                            Rama                 :{{ " ".$info_general->rama}}<br>
                        @endif
                        Perfil               :{{ " ".$info_general->perfil}}<br>
                        Idioma               :{{ " ".$info_general->idioma}}<br>
                        Código               :{{ " ".$info_general->codigo_programa}}<br>
                        Nombre del programa  :{{ " ".$info_general->descripcion_programa}}<br>
                        Universidad          :{{ " ".$info_general->universidad}}<br>
                        Ciudad               :{{ " ".$info_general->ciudad}}
                    </p>
                </div>
            </div>
        </div>
        <!-- COSTO EN MONEDA LOCAL -->
        <div class="">
            <p style="font-size : 1.5rem"><u><strong>Costo en: {{$divisa["nombre"].' ('.$divisa["simbolo"].')'}}</strong></u></p>
            <div class="row">
                <br>
                <table class="table table-sm">
                    <thead>
                        <tr>
                        <th scope="col">Descripción</th>
                        <th scope="col" class="text-center" <?php echo ($mostrar["caso_tabla"]=='carrera_ingles') ? 'style="display:none";': ''; ?>>Facultad Preparatoria</th>
                        <th scope="col" class="text-center" <?php echo $mostrar["columna"]==false ? 'style="display:none";': ''; ?> >{{$info_general->tipo_descripcion}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td scope="row">Estudios (Colegiatura) anual</td>
                            @if ($mostrar["columna"]==true)
                                <td class="text-center" <?php echo ($mostrar["caso_tabla"]=='carrera_ingles') ? 'style="display:none";': ''; ?>>{{number_format($presupuesto_local["costo_facultad"],0,'',',')}}</td>
                            @endif
                                <td class="text-center">{{number_format($presupuesto_local["costo_programa"],0,'',',')}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Vivienda universitaria anual</td>
                            <td class="text-center" <?php echo ($mostrar["caso_tabla"]=='carrera_ingles') ? 'style="display:none";': ''; ?>>{{number_format($presupuesto_local["costo_vivienda"],0,'',',')}}</td>
                            <td class="text-center" <?php echo $mostrar["columna"]==false ? 'style="display:none";': ''; ?>>{{number_format($presupuesto_local["costo_vivienda"],0,'',',')}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Seguro médico anual</td>
                            <td class="text-center" <?php echo ($mostrar["caso_tabla"]=='carrera_ingles') ? 'style="display:none";': ''; ?>>{{number_format($presupuesto_local["costo_seguro"],0,'',',')}}</td>
                            <td class="text-center" <?php echo $mostrar["columna"]==false ? 'style="display:none";': ''; ?>>{{number_format($presupuesto_local["costo_seguro"],0,'',',')}}</td>
                        </tr>
                        <tr>
                            <td scope="row">Alimentación, gastos personales,Telefonía celular e Internet anual</td>
                            <td class="text-center"<?php echo ($mostrar["caso_tabla"]=='carrera_ingles') ? 'style="display:none";': ''; ?>>{{number_format($presupuesto_local["costo_alimentacion"],0,'',',')}}</td>
                            <td class="text-center" <?php echo $mostrar["columna"]==false ? 'style="display:none";': ''; ?>>{{number_format($presupuesto_local["costo_alimentacion"],0,'',',')}}</td>
                        </tr>
                        <tr>
                            <th scope="row">Total anual</th>
                            @if ($mostrar["columna"]==true)
                                <td class="text-center" <?php echo ($mostrar["caso_tabla"]=='carrera_ingles') ? 'style="display:none";': ''; ?>>{{$divisa["simbolo"].number_format($presupuesto_local["total_facultad"],0,'',',')}}</td>
                            @endif
                                <td class="text-center" >{{$divisa["simbolo"].number_format($presupuesto_local["total_programas"],0,'',',')}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <footer class="page-footer" >
            <div style="margin-left:150px">
                <b>ALAR - Asociación Latinoamericano Rusa</b> | <a href="https://www.alar.us">www.alar.us</a> 
                <a href="https://www.universidades-rusia.com/inscripciones/"><img src="https://universidades-rusia.com/programas/assets/common_img/suscribir.png" width="250px"  style=" margin-left:335;margin-top:-18px"></a>
            </div>
        </footer>        
        @switch($mostrar["caso_tabla"])
            @case("carrera_ruso/español")
                <div class="page-break"></div>
                <div>
                    <table style="margin-top:80px">
                        <tbody>
                            <tr>
                                <td colspan="4" style="text-align:center"> <b>LEYENDA</b></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td>Costo facultad Preparatoria<div class="leyenda costo_preparatoria"><div></td>
                                <td style="padding-left:95px">Costo carrera<div class="leyenda costo_carrera"><div></td>
                                <td style="padding-left:50px">Vivienda universitaria<div class="leyenda costo_vivienda"><div></td>
                                <td style="padding-left:50px">Seguro médico<div class="leyenda costo_seguro"><div></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td colspan="2" >Alimentación, gastos personales,Telefonía celular e Internet<div class="leyenda costo_alimentacion"><div></td>
                                <td style="padding-left:50px">Total mensual<div class="leyenda total_mensual"><div></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding-top:12px">
                                    <i>En las siguientes tablas, Usted encontrará una proyección de pagos mensuales que deberá asumir. Cada pago tiene un color según la leyenda que se adjunta</i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table class="table" style="margin-top:35px;margin-left:-20;">
                        <tbody>
                            <tr style="text-align:center;">
                                <td colspan="5" style="background:rgb(255,226,138)">{{$costos_calendario["año"]}}</td>
                                <td colspan="7" style="background:rgb(221,235,247)">{{$costos_calendario["año"]+1}}</td>
                            </tr>
                            <tr style="text-align:center;">
                                <td></td>
                                <td colspan="10" style="background:rgb(226,239,218)">Facultad Preparatoria</td>
                                <td colspan="" style="background:rgb(252,255,185)">Vacaciones</td>
                            </tr>
                            <tr>
                                <td ></td>
                                <td class="meses">Sept.</td>
                                <td class="meses">Oct.</td>
                                <td class="meses">Nov.</td>
                                <td class="meses">Dic.</td>
                                <td class="meses">Ene.</td>
                                <td class="meses">Feb.</td>
                                <td class="meses">Mar.</td>
                                <td class="meses">Abr.</td>
                                <td class="meses">May.</td>
                                <td class="meses">Jun.</td>
                                <td class="meses">Jul.</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_preparatoria number">{{number_format($costos_calendario["cal_costo_facultad"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_seguro">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma1"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page-break"></div>
                <div>
                    <table style="margin-top:80px">
                        <tbody>
                            <tr>
                                <td colspan="4" style="text-align:center"> <b>LEYENDA</b></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td>Costo facultad Preparatoria<div class="leyenda costo_preparatoria"><div></td>
                                <td style="padding-left:95px">Costo carrera<div class="leyenda costo_carrera"><div></td>
                                <td style="padding-left:50px">Vivienda universitaria<div class="leyenda costo_vivienda"><div></td>
                                <td style="padding-left:50px">Seguro médico<div class="leyenda costo_seguro"><div></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td colspan="2" >Alimentación, gastos personales,Telefonía celular e Internet<div class="leyenda costo_alimentacion"><div></td>
                                <td style="padding-left:50px">Total mensual<div class="leyenda total_mensual"><div></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding-top:12px">
                                    <i>En las siguientes tablas, Usted encontrará una proyección de pagos mensuales que deberá asumir. Cada pago tiene un color según la leyenda que se adjunta</i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table class="table" style="margin-top:35px;margin-left:-20">
                        <tbody>
                            <tr style="text-align:center;">
                                <td colspan="5" style="background:rgb(255,226,138)">{{$costos_calendario["año"]+1}}</td>
                                <td colspan="7" style="background:rgb(221,235,247)">{{$costos_calendario["año"]+2}}</td>
                            </tr>
                            <tr style="text-align:center;">
                                <td style="background:rgb(252,255,185)">Vacaciones</td>
                                <td colspan="10" style="background:rgb(255,184,190)">Carrera/Postgrado</td>
                                <td colspan="" style="background:rgb(252,255,185)">Vacaciones</td>
                            </tr>
                            <tr>
                                <td class="meses">Ago.</td>
                                <td class="meses">Sept.</td>
                                <td class="meses">Oct.</td>
                                <td class="meses">Nov.</td>
                                <td class="meses">Dic.</td>
                                <td class="meses">Ene.</td>
                                <td class="meses">Feb.</td>
                                <td class="meses">Mar.</td>
                                <td class="meses">Abr.</td>
                                <td class="meses">May.</td>
                                <td class="meses">Jun.</td>
                                <td class="meses">Jul.</td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_carrera">{{number_format($costos_calendario["cal_costo_programa"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="costo_carrera">{{number_format($costos_calendario["cal_costo_programa"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_seguro">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma3"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma4"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma3"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @break
            @case("facultad1")
                <div class="page-break"></div>
                <div>
                    <table style="margin-top:80px">
                        <tbody>
                            <tr>
                                <td colspan="4" style="text-align:center"> <b>LEYENDA</b></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td>Costo facultad Preparatoria<div class="leyenda costo_preparatoria"><div></td>
                                <td style="padding-left:95px">Costo carrera<div class="leyenda costo_carrera"><div></td>
                                <td style="padding-left:50px">Vivienda universitaria<div class="leyenda costo_vivienda"><div></td>
                                <td style="padding-left:50px">Seguro médico<div class="leyenda costo_seguro"><div></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td colspan="2" >Alimentación, gastos personales,Telefonía celular e Internet<div class="leyenda costo_alimentacion"><div></td>
                                <td style="padding-left:50px">Total mensual<div class="leyenda total_mensual"><div></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding-top:12px">
                                    <i>En las siguientes tablas, Usted encontrará una proyección de pagos mensuales que deberá asumir. Cada pago tiene un color según la leyenda que se adjunta</i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table class="table" style="margin-top:35px;margin-left:-20;">
                        <tbody>
                            <tr style="text-align:center;">
                                <td colspan="12" style="background:rgb(255,226,138)">{{$costos_calendario["año"]+1}}</td>
                            </tr>
                            <tr style="text-align:center;">
                                <td colspan="7" style="background:rgb(226,239,218)">FACULTAD PREPARATORIA</td>
                                <td colspan="" style="background:rgb(252,255,185)">VACACIONES</td>
                                <td colspan="4" style="background:rgb(255,184,190)">CARRERA/POSTGRADO</td>
                            </tr>
                            <tr>
                                <td class="meses">Ene.</td>
                                <td class="meses">Feb.</td>
                                <td class="meses">Mar.</td>
                                <td class="meses">Abr.</td>
                                <td class="meses">May.</td>
                                <td class="meses">Jun.</td>
                                <td class="meses">Jul.</td>
                                <td class="meses">Ago.</td>
                                <td class="meses">Sept.</td>
                                <td class="meses">Oct.</td>
                                <td class="meses">Nov.</td>
                                <td class="meses">Dic.</td>
                                
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_preparatoria"><b>{{$presupuesto_local["costo_programa"]}}</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_vivienda"><b>{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</b></td>
                                <td class="costo_vivienda"><b>{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</b></td>
                                <td class="costo_vivienda"><b>{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</b></td>
                                <td class="costo_vivienda"><b>{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</b></td>
                                <td class="costo_vivienda"><b>{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</b></td>
                                <td class="costo_vivienda"><b>{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</b></td>
                                <td class="costo_vivienda"><b>{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</b></td>
                                <td class="costo_vivienda"><b>{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_seguro"><b>{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_alimentacion"><b>{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</b></td>
                                <td class="costo_alimentacion"><b>{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</b></td>
                                <td class="costo_alimentacion"><b>{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</b></td>
                                <td class="costo_alimentacion"><b>{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</b></td>
                                <td class="costo_alimentacion"><b>{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</b></td>
                                <td class="costo_alimentacion"><b>{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</b></td>
                                <td class="costo_alimentacion"><b>{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</b></td>
                                <td class="costo_alimentacion"><b>{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="total_mensual"><b>{{$divisa["simbolo"].number_format($presupuesto_local["costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_alimentacion"],0,'',',')}}</b></td>
                                <td class="total_mensual"><b>{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</b></td>
                                <td class="total_mensual"><b>{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</b></td>
                                <td class="total_mensual"><b>{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</b></td>
                                <td class="total_mensual"><b>{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</b></td>
                                <td class="total_mensual"><b>{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</b></td>
                                <td class="total_mensual"><b>{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</b></td>
                                <td class="total_mensual"><b>{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div> 
            @break
            @case("facultad2")
                <div class="page-break"></div>
                <div>
                    <table style="margin-top:80px">
                        <tbody>
                            <tr>
                                <td colspan="4" style="text-align:center"> <b>LEYENDA</b></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td>Costo facultad Preparatoria<div class="leyenda costo_preparatoria"><div></td>
                                <td style="padding-left:95px">Costo carrera<div class="leyenda costo_carrera"><div></td>
                                <td style="padding-left:50px">Vivienda universitaria<div class="leyenda costo_vivienda"><div></td>
                                <td style="padding-left:50px">Seguro médico<div class="leyenda costo_seguro"><div></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td colspan="2" >Alimentación, gastos personales,Telefonía celular e Internet<div class="leyenda costo_alimentacion"><div></td>
                                <td style="padding-left:50px">Total mensual<div class="leyenda total_mensual"><div></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding-top:12px">
                                    <i>En las siguientes tablas, Usted encontrará una proyección de pagos mensuales que deberá asumir. Cada pago tiene un color según la leyenda que se adjunta</i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table class="table" style="margin-top:35px;margin-left:-20;">
                        <tbody>
                            <tr style="text-align:center;">
                                <td colspan="11" style="background:rgb(255,226,138)">{{$costos_calendario["año"]}}</td>
                                <td style="background:rgb(221,235,247)">{{$costos_calendario["año"]+1}}</td>
                            </tr>
                            <tr style="text-align:center;">
                                <td></td>
                                <td colspan="4" style="background:rgb(226,239,218)">Facultad Preparatoria</td>
                                <td colspan="2" style="background:rgb(252,255,185)">Vacaciones</td>
                                <td colspan="5" style="background:rgb(226,239,218)">Facultad Preparatoria</td>
                            </tr>
                            <tr>
                                <td ></td>
                                <td class="meses">Mar.</td>
                                <td class="meses">Abr.</td>
                                <td class="meses">May.</td>
                                <td class="meses">Jun.</td>
                                <td class="meses">Jul.</td>
                                <td class="meses">Agos.</td>
                                <td class="meses">Sept.</td>
                                <td class="meses">Oct.</td>
                                <td class="meses">Nov.</td>
                                <td class="meses">Dic.</td>
                                <td class="meses">Ene.</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_preparatoria">{{number_format($costos_calendario["cal_costo_programa"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td  class="costo_preparatoria">{{number_format($presupuesto_local["costo_programa"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_seguro">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["cal_costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($presupuesto_local["costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page-break"></div>
                <div>
                    <table style="margin-top:80px">
                        <tbody>
                            <tr>
                                <td colspan="4" style="text-align:center"> <b>LEYENDA</b></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td>Costo facultad Preparatoria<div class="leyenda costo_preparatoria"><div></td>
                                <td style="padding-left:95px">Costo carrera<div class="leyenda costo_carrera"><div></td>
                                <td style="padding-left:50px">Vivienda universitaria<div class="leyenda costo_vivienda"><div></td>
                                <td style="padding-left:50px">Seguro médico<div class="leyenda costo_seguro"><div></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td colspan="2" >Alimentación, gastos personales,Telefonía celular e Internet<div class="leyenda costo_alimentacion"><div></td>
                                <td style="padding-left:50px">Total mensual<div class="leyenda total_mensual"><div></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding-top:12px">
                                    <i>En las siguientes tablas, Usted encontrará una proyección de pagos mensuales que deberá asumir. Cada pago tiene un color según la leyenda que se adjunta</i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table class="table" style="margin-top:35px;margin-left:-20">
                        <tbody>
                            <tr style="text-align:center;">
                                <td colspan="11" style="background:rgb(255,226,138)">{{$costos_calendario["año"]+1}}</td>
                                <td style="background:rgb(221,235,247)">{{$costos_calendario["año"]+2}}</td>
                            </tr>
                            <tr style="text-align:center;">
                                <td colspan="5" style="background:rgb(226,239,218)">FACULTAD PREPARATORIA</td>
                                <td colspan="2" style="background:rgb(252,255,185)">VACACIONES</td>
                                <td colspan="5" style="background:rgb(255,184,190)">CARRERA/POSTGRADO</td>
                            </tr>
                            <tr>
                                <td class="meses">Feb.</td>
                                <td class="meses">Mar.</td>
                                <td class="meses">Abr.</td>
                                <td class="meses">May.</td>
                                <td class="meses">Jun.</td>
                                <td class="meses">Jul.</td>
                                <td class="meses">Ago.</td>
                                <td class="meses">Sept.</td>
                                <td class="meses">Oct.</td>
                                <td class="meses">Nov.</td>
                                <td class="meses">Dic.</td>
                                <td class="meses">Ene.</td>
                                
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_seguro">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma4"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @break
            @case("facultad3")
                <div class="page-break"></div>
                <div>
                    <table style="margin-top:80px">
                        <tbody>
                            <tr>
                                <td colspan="4" style="text-align:center"> <b>LEYENDA</b></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td>Costo facultad Preparatoria<div class="leyenda costo_preparatoria"><div></td>
                                <td style="padding-left:95px">Costo carrera<div class="leyenda costo_carrera"><div></td>
                                <td style="padding-left:50px">Vivienda universitaria<div class="leyenda costo_vivienda"><div></td>
                                <td style="padding-left:50px">Seguro médico<div class="leyenda costo_seguro"><div></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td colspan="2" >Alimentación, gastos personales,Telefonía celular e Internet<div class="leyenda costo_alimentacion"><div></td>
                                <td style="padding-left:50px">Total mensual<div class="leyenda total_mensual"><div></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding-top:12px">
                                    <i>En las siguientes tablas, Usted encontrará una proyección de pagos mensuales que deberá asumir. Cada pago tiene un color según la leyenda que se adjunta</i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table class="table" style="margin-top:35px;margin-left:-20;">
                        <tbody>
                            <tr style="text-align:center;">
                                <td colspan="5" style="background:rgb(255,226,138)">{{$costos_calendario["año"]}}</td>
                                <td colspan="7" style="background:rgb(221,235,247)">{{$costos_calendario["año"]+1}}</td>
                            </tr>
                            <tr style="text-align:center;">
                                <td></td>
                                <td colspan="10" style="background:rgb(226,239,218)">Facultad Preparatoria</td>
                                <td style="background:rgb(252,255,185)">Vacaciones</td>
                            </tr>
                            <tr>
                                <td ></td>
                                <td class="meses">Sept.</td>
                                <td class="meses">Oct.</td>
                                <td class="meses">Nov.</td>
                                <td class="meses">Dic.</td>
                                <td class="meses">Ene.</td>
                                <td class="meses">Feb.</td>
                                <td class="meses">Mar.</td>
                                <td class="meses">Abr.</td>
                                <td class="meses">May.</td>
                                <td class="meses">Jun.</td>
                                <td class="meses">Jul.</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_preparatoria">{{number_format($presupuesto_local["costo_programa"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_seguro">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($presupuesto_local["costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page-break"></div>
                <div>
                    <table style="margin-top:80px">
                        <tbody>
                            <tr>
                                <td colspan="4" style="text-align:center"> <b>LEYENDA</b></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td>Costo facultad Preparatoria<div class="leyenda costo_preparatoria"><div></td>
                                <td style="padding-left:95px">Costo carrera<div class="leyenda costo_carrera"><div></td>
                                <td style="padding-left:50px">Vivienda universitaria<div class="leyenda costo_vivienda"><div></td>
                                <td style="padding-left:50px">Seguro médico<div class="leyenda costo_seguro"><div></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td colspan="2" >Alimentación, gastos personales,Telefonía celular e Internet<div class="leyenda costo_alimentacion"><div></td>
                                <td style="padding-left:50px">Total mensual<div class="leyenda total_mensual"><div></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding-top:12px">
                                    <i>En las siguientes tablas, Usted encontrará una proyección de pagos mensuales que deberá asumir. Cada pago tiene un color según la leyenda que se adjunta</i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table class="table" style="margin-top:35px;margin-left:-20">
                        <tbody>
                            <tr style="text-align:center;">
                                <td colspan="5" style="background:rgb(255,226,138)">{{$costos_calendario["año"]+1}}</td>
                                <td colspan="7" style="background:rgb(221,235,247)">{{$costos_calendario["año"]+2}}</td>
                            </tr>
                            <tr style="text-align:center;">
                                <td style="background:rgb(252,255,185)">VACACIONES</td>
                                <td colspan="10" style="background:rgb(255,184,190)">CARRERA/POSTGRADO</td>
                                <td style="background:rgb(252,255,185)">VACACIONES</td>
                            </tr>
                            <tr>
                                <td class="meses">Ago.</td>
                                <td class="meses">Sept.</td>
                                <td class="meses">Oct.</td>
                                <td class="meses">Nov.</td>
                                <td class="meses">Dic.</td>
                                <td class="meses">Ene.</td>
                                <td class="meses">Feb.</td>
                                <td class="meses">Mar.</td>
                                <td class="meses">Abr.</td>
                                <td class="meses">May.</td>
                                <td class="meses">Jun.</td>
                                <td class="meses">Jul.</td>
                                
                                
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @break
            @case("carrera_ingles")
                <div class="page-break"></div>
                <div>
                    <table style="margin-top:80px">
                        <tbody>
                            <tr>
                                <td colspan="4" style="text-align:center"> <b>LEYENDA</b></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td>Costo facultad Preparatoria<div class="leyenda costo_preparatoria"><div></td>
                                <td style="padding-left:95px">Costo carrera<div class="leyenda costo_carrera"><div></td>
                                <td style="padding-left:50px">Vivienda universitaria<div class="leyenda costo_vivienda"><div></td>
                                <td style="padding-left:50px">Seguro médico<div class="leyenda costo_seguro"><div></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td colspan="2" >Alimentación, gastos personales,Telefonía celular e Internet<div class="leyenda costo_alimentacion"><div></td>
                                <td style="padding-left:50px">Total mensual<div class="leyenda total_mensual"><div></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding-top:12px">
                                    <i>En las siguientes tablas, Usted encontrará una proyección de pagos mensuales que deberá asumir. Cada pago tiene un color según la leyenda que se adjunta</i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table class="table" style="margin-top:35px;margin-left:-20;">
                        <tbody>
                            <tr style="text-align:center;">
                                <td colspan="5" style="background:rgb(255,226,138)">{{$costos_calendario["año"]}}</td>
                                <td colspan="7" style="background:rgb(221,235,247)">{{$costos_calendario["año"]+1}}</td>
                            </tr>
                            <tr style="text-align:center;">
                                <td></td>
                                <td colspan="10" style="background:rgb(255,184,190)">CARRERA/POSTGRADO</td>
                                <td colspan="1" style="background:rgb(252,255,185)">Vacaciones</td>
                            </tr>
                            <tr>
                                <td ></td>
                                <td class="meses">Sept.</td>
                                <td class="meses">Oct.</td>
                                <td class="meses">Nov.</td>
                                <td class="meses">Dic.</td>
                                <td class="meses">Ene.</td>
                                <td class="meses">Feb.</td>
                                <td class="meses">Mar.</td>
                                <td class="meses">Abr.</td>
                                <td class="meses">May.</td>
                                <td class="meses">Jun.</td>
                                <td class="meses">Jul.</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_preparatoria">{{number_format($presupuesto_local["costo_programa"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_seguro">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($presupuesto_local["costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="page-break"></div>
                <div>
                    <table style="margin-top:80px">
                        <tbody>
                            <tr>
                                <td colspan="4" style="text-align:center"> <b>LEYENDA</b></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td>Costo facultad Preparatoria<div class="leyenda costo_preparatoria"><div></td>
                                <td style="padding-left:95px">Costo carrera<div class="leyenda costo_carrera"><div></td>
                                <td style="padding-left:50px">Vivienda universitaria<div class="leyenda costo_vivienda"><div></td>
                                <td style="padding-left:50px">Seguro médico<div class="leyenda costo_seguro"><div></td>
                            </tr>
                            <tr style="padding-top:40px">
                                <td colspan="2" >Alimentación, gastos personales,Telefonía celular e Internet<div class="leyenda costo_alimentacion"><div></td>
                                <td style="padding-left:50px">Total mensual<div class="leyenda total_mensual"><div></td>
                            </tr>
                            <tr>
                                <td colspan="7" style="padding-top:12px">
                                    <i>En las siguientes tablas, Usted encontrará una proyección de pagos mensuales que deberá asumir. Cada pago tiene un color según la leyenda que se adjunta</i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table class="table" style="margin-top:35px;margin-left:-20">
                        <tbody>
                            <tr style="text-align:center;">
                                <td colspan="5" style="background:rgb(255,226,138)">{{$costos_calendario["año"]+1}}</td>
                                <td colspan="7" style="background:rgb(221,235,247)">{{$costos_calendario["año"]+2}}</td>
                            </tr>
                            <tr style="text-align:center;">
                                <td style="background:rgb(252,255,185)">VACACIONES</td>
                                <td colspan="10" style="background:rgb(255,184,190)">CARRERA/POSTGRADO</td>
                                <td style="background:rgb(252,255,185)">VACACIONES</td>
                            </tr>
                            <tr>
                                <td class="meses">Ago.</td>
                                <td class="meses">Sept.</td>
                                <td class="meses">Oct.</td>
                                <td class="meses">Nov.</td>
                                <td class="meses">Dic.</td>
                                <td class="meses">Ene.</td>
                                <td class="meses">Feb.</td>
                                <td class="meses">Mar.</td>
                                <td class="meses">Abr.</td>
                                <td class="meses">May.</td>
                                <td class="meses">Jun.</td>
                                <td class="meses">Jul.</td>
                                
                                
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_preparatoria">{{number_format($costos_calendario["cal_costo_programa"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="costo_preparatoria">{{number_format($costos_calendario["cal_costo_programa"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                                <td class="costo_vivienda">{{number_format($costos_calendario["cal_costo_vivienda"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td></td>
                                <td class="costo_seguro">{{number_format($costos_calendario["cal_costo_seguro"],0,'',',')}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                                <td class="costo_alimentacion">{{number_format($costos_calendario["cal_costo_alimentacion"],0,'',',')}}</td>
                            </tr>
                            <tr style="text-align:center">
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma3"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma4"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma3"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                                <td class="total_mensual">{{$divisa["simbolo"].number_format($costos_calendario["suma2"],0,'',',')}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            @break
        @endswitch
    </body>
</html>
