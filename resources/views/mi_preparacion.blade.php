@extends('layouts.master')
@section('content')
<section class="" style="background-color: white; border-radius: 10px;">

    @if($mostrar == 1)
    <div class="container-fluid">
    <div class="bg-dark rounded d-flex justify-content-around" style="height: 3.4rem">
        <h4 class="text-light d-flex align-items-center" id="title_video">{{$data_videos[1][0]->title}}</h4>
        <!-- <div class="text-light d-flex align-items-center"> <i class="fa fa-award fa-2x"></i><p class="">Tu Progreso</p></div> -->
    </div>
    <div class="row">
        <div class="card col-md-8" >
            <div  class="card-body embed-responsive embed-responsive-16by9" style="max-height: 500px;">
                <input type="hidden" value="{{$data_videos[1][1]->id}}" id="video_id">
                <video id="video" controls crossorigin playsinline id="player">
                    <!-- Video files -->
                    <source src="{{ url('/videos/'.$data_videos[1][0]->id)}}" type="video/mp4" size="576" id="video_ruso">
                    <!-- <source src="{{url('/videos/'.$data_videos[1][0]->id)}}" type="video/mp4" size="720">
                    <source src="{{url('/videos/'.$data_videos[1][0]->id)}}" type="video/mp4" size="1080">
                    <source src="{{url('/videos/'.$data_videos[1][0]->id)}}" type="video/mp4" size="1440"> -->
                    <!-- Fallback for browsers that don't support the <video> element -->
                    <a href="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4" download>Download</a>
                </video>
                <input type="hidden" value="{{auth()->id()}}" id="user_id">
            </div>
        </div>
        <!-- <div class="embed-responsive embed-responsive-16by9 col-md-8">
        </div> -->
        <div class="col-md-4">
            <section class="p-3 border bg-white">
                <h3>Contenido del curso</h3>
                <div class="progress">
                    <div id="progress_curso" class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: {{$value_progress}}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">{{$value_progress}}%</div>
                </div>
            </section>
            @foreach($data_videos as $modulos)
                @for ($i = 0; $i < count($modulos); $i++)
                    @if($i==0)
                        <div class="p-3 border bg-light" data-toggle="collapse" href="#collapse{{$modulos[$i]->id_modulo}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                            {{$modulos[$i]->nombre}}
                        </div>
                    @endif
                    <div class="collapse" id="collapse{{$modulos[$i]->id_modulo}}">
                        <div class="btnCurso pl-3 pt-3 pb-3 d-flex align-items-center">
                            <label class="container_check">
                                <input class="ml-3 mt-1" id="checkbox{{$modulos[$i]->video_user_id}}" type="checkbox" onclick="input_checked('{{$modulos[$i]->id}}')" {{$modulos[$i]->progress}}>
                                <span class="checkmark"></span>
                            </label>
                            <div class="pl-2" onclick="change_video('{{$modulos[$i]->id}}','{{$modulos[$i]->title}}')"> {{$modulos[$i]->id}}. {{$modulos[$i]->title}}</div>
                            <button type="button" class="btn btn-outline-primary ml-5"><i class="fa fa-edit"></i> Tareas</button>
                        </div>
                    </div>
                @endfor
            @endforeach
        </div>
    </div>
    </div>
    @else
    <div class="container d-flex justify-content-center flex-column text-center">
        <h2 class="mt30 mb20 text-secondary mt-4">Aún no has adquirido la Capacitación de idioma ruso</h2>
    </div>
    <br>
    <div class="container">
        <div class="timeline-item">
            <h3 class="text-midnight-lighter text-info text-center">Aquí te explicamos porqué deberías hacerlo ⬇️</h3>
            <br>
            <br>
            <div class="timeline-body">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/DD6l9-w-C5U" frameborder="0" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </div>
    @endif
</section>
@endsection
