<!DOCTYPE html>
<html lang="es">
    <head>
        {{-- {!! SEO::generate() !!} --}}
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <title>ALAR - Asociación Latinoamericano Rusa</title> --}}
        <!-- Fonts -->
        <!-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet"> -->
        <!-- <link rel="stylesheet" href="/css/app.css"> -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/slicknav.css')}}">
        <link rel="stylesheet" href="{{URL::asset('css/color-switcher.css')}}">
        <link rel="stylesheet" media="screen" href="{{URL::asset('fonts/font-awesome/all.min.css')}}">
        <!-- <link rel="stylesheet" media="screen" href="/fonts/simple-line-icons.css"> -->
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/owl.theme.css')}}">
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/animate.css')}}">
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/normalize.css')}}">
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/responsive.css')}}">
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/green.css')}}" media="screen" />
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/main.css')}}">
        <link rel="stylesheet" type="text/css" href="{{URL::asset('css/style.css')}}">
        <!-- Styles -->
    <style type="text/css" >
        .flag {
            width: 1.3rem;
            position: relative;top: -3px;
            margin-right: 5px;
        }
        .symbol {
            height: .85rem;
        }
        .page {
        display: none;
        }
        .page-active {
        display: block;
        }
        #div-rama {
            display: none;
        }
        #div-perfil {
            display: none;
        }
        /* #div-idioma {
            display: none;
        } */
        .swal2-container {
            z-index: 19001 !important;
        }
    </style>
    <script src="{{ URL::asset('js/jquery-min.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.twbsPagination.js') }}"></script>

    <script>
    </script>
    </head>
    <body>
        <header id="header-wrap">
            <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar nav-bg" style="z-index:100">
                <div class="container-fluid">
                    <div class="theme-header clearfix">
                        <div class="navbar-header">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                                <span class="fa fa-menu"></span>
                                <span class="fa fa-menu"></span>
                                <span class="fa fa-menu"></span>
                            </button>
                            <a href="index.html" class="navbar-brand"><img src="{{ URL::asset('images/logoalar.png')}}" alt=""></a>
                        </div>
                        <div class="collapse navbar-collapse" id="main-menu">
                            <ul class="navbar-nav mr-auto w-100 justify-content-end">
                                <li class="nav-item dropdown">
                                    <a class="nav-link a-rsociales" href="https://www.facebook.com/ALARrusia/">
                                        <i class="fab fa-facebook-f icon-round"></i>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link a-rsociales" href="https://www.instagram.com/alaroficial/" target="_blank">
                                        <i class="fab fa-instagram icon-round"></i>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link a-rsociales" href="https://www.youtube.com/user/ALAR0001">
                                        <i class="fab fa-youtube icon-round"></i>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link a-rsociales" href="https://www.linkedin.com/company/alar-asociacion-latinoamericano-rusa" target="_blank">
                                        <i class="fab fa-linkedin-in icon-round"></i>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <!-- <a class="nav-link a-rsociales"  target="_blank" href="https://wa.me/51999223551?text=Quiero Informacion"> -->
                                    <a class="nav-link dropdown-toggle a-rsociales"  target="_blank" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fab fa-whatsapp icon-round"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" target="_blank" href="https://wa.me/72517013?text=Hola, quisiera tener mas Información">BOLIVIA </a></li>
                                        <li><a class="dropdown-item" target="_blank" href="https://wa.me/11994395510?text=Hola, quisiera tener mas Información">BRASIL</a></li>
                                        <li><a class="dropdown-item" target="_blank" href="https://wa.me/3158662715?text=Hola, quisiera tener mas Información">COLOMBIA</a></li>
                                        <li><a class="dropdown-item" target="_blank" href="https://wa.me/0991676921?text=Hola, quisiera tener mas Información">ECUADOR</a></li>
                                        <li><a class="dropdown-item" target="_blank" href="https://wa.me/5562333418?text=Hola, quisiera tener mas Información">MEXICO</a></li>
                                        <li><a class="dropdown-item" target="_blank" href="https://wa.me/51936017225?text=Hola, quisiera tener mas Información">PERÚ </a></li>
                                        <li><a class="dropdown-item" target="_blank" href="https://wa.me/51936017225?text=Hola, quisiera tener mas Información">Otros países de Latinoamérica</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ENCUENTRA LO QUE BUSCAS</a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item dropdown" >
                                            <a class="dropdown-toggle dropdown-toggle-split" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">NOSOTROS</a>
                                            <ul class="dropdown-menu" style="margin-left:100%;margin-top:-25%">
                                                <li><a class="dropdown-item" href="https://www.universidades-rusia.com/alar/" >SOBRE ALAR</a></li>
                                                <li><a class="dropdown-item" href="https://www.universidades-rusia.com/convenios-alianzas/" >CONVENIOS & ALIANZAS</a></li>
                                            </ul>
                                        </li>
                                        <li class="nav-item dropdown" >
                                            <a class="dropdown-toggle dropdown-toggle-split" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">¿QUÉ ESTUDIAR EN RUSIA?</a>
                                            <ul class="dropdown-menu" style="margin-left:100%;margin-top:-25%">
                                                <li><a class="dropdown-item" href="https://www.universidades-rusia.com/estudios/carreras/" >CARRERAS</a></li>
                                                <li><a class="dropdown-item" href="https://www.universidades-rusia.com/estudios/doctorados/" >MAESTRÍAS</a></li>
                                                <li><a class="dropdown-item" href="https://www.universidades-rusia.com/estudios/especialidades-medicas/" >ESPECIALIDADES MÉDICAS</a></li>
                                                <li><a class="dropdown-item" href="https://www.universidades-rusia.com/estudios/doctorados/">DOCTORADOS</a></li>
                                                <li><a class="dropdown-item" href="#">DE INTERCAMBIOS EN <br>RUSIA</a></li>
                                                <li><a class="dropdown-item" href="https://www.universidades-rusia.com/temporada/cursos-de-verano-en-rusia/" >ESCUELA DE VERANO</a></li>
                                                <!-- <li><a class="dropdown-item" href="#" >TRASLADOS DENTRO DE<br> RUSIA </a></li> -->
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item" href="https://www.universidades-rusia.com/las-universidades">LAS UNIVERSIDADES</a></li>
                                        <li><a class="dropdown-item" href="https://www.universidades-rusia.com/inscripciones/">¿CÓMO INSCRIBIRME?</a></li>
                                        <li class="nav-item dropdown">
                                            <a class="dropdown-toggle dropdown-toggle-split" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">IDIOMA RUSO</a>
                                            <ul class="dropdown-menu" style="margin-left:100%;margin-top:-25%">
                                                <li><a class="dropdown-item" href="https://www.universidades-rusia.com/certificacion/certificacion-idioma-ruso/" >CERTIFICACIÓN IDIOMA RUSO</a></li>
                                                <li><a class="dropdown-item" href="https://www.blog.universidades-rusia.com/ruso-online/" >CURSOS LIBRES DE RUSO</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item" href="https://www.universidades-rusia.com/contacto/">CONTACTO Y OFICINAS</a></li>
                                        <li><a class="dropdown-item" href="https://www.universidades-rusia.com/preguntas-frecuentes/">PREGUNTAS FRECUENTAS</a></li>
                                        <li><a class="dropdown-item" href="https://www.blog.universidades-rusia.com/">NUESTRO BLOG</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="https://www.universidades-rusia.com/inscripciones/">¡INSCRIBETE AHORA!</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="https://universidades-rusia.com/pt-br/">::PORTUGUÊS::</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link" data-toggle="modal" data-target="#modalLogin">Zona de usuario</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="mobile-menu" data-logo="images/logoalar.png"></div>
            </nav>
            <div id="carousel-area">
                <div id="carousel-slider" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators" style="z-index:4;">
                        <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-slider" data-slide-to="1"></li>
                        <li data-target="#carousel-slider" data-slide-to="2"></li>
                        <li data-target="#carousel-slider" data-slide-to="3"></li>
                        <li data-target="#carousel-slider" data-slide-to="4"></li>
                        <li data-target="#carousel-slider" data-slide-to="5"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="{{ URL::asset('images/slider/slider1.jpg') }}" alt="">
                            <div class="carousel-caption">
                                <h2 class="fadeInUp wow " data-sppb-wow-delay="0.8s">
                                    ¿Porqué viajar a Rusia y no otro país?
                                </h2>
                                <h3 class="fadeInUp wow slide_h3" data-sppb-wow-delay="1.2s">
                                    Alto nivel académico a bajo costo en universidades con reconocimiento mundial
                                </h3><br>
                                <a class="btn btn-lg btn-common fadeInUp wow" data-sppb-wow-delay="1.4s" href="#">
                                    <i class="fa fa-coffee"></i>¡Quiero saber mas!
                                </a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ URL::asset('images/slider/slider2.jpg') }}" alt="">
                            <div class="carousel-caption">
                                <h2 class="fadeInUp wow" data-sppb-wow-delay="0.8s">
                                    ¿Porqué viajar a Rusia y no otro país?
                                </h2>
                                <h3 class="fadeInUp wow slide_h3" data-sppb-wow-delay="1.2s">
                                    Diplomas Dobles, prácticas profesionales y bolsa de trabajo
                                </h3><br>
                                <a class="btn btn-lg btn-common fadeInUp wow" data-sppb-wow-delay="1.4s" href="#">
                                    <i class="fa fa-coffee"></i>¡Quiero saber mas!
                                </a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ URL::asset('images/slider/slider03.jpg') }}" alt="">
                            <div class="carousel-caption">
                                <h2 class="fadeInUp wow" data-sppb-wow-delay="0.8s">
                                    ¿Porqué viajar a Rusia y no otro país?
                                </h2>
                                <h3 class="fadeInUp wow slide_h3" data-sppb-wow-delay="1.2s">
                                    País amigable y de fácil adaptación para los latinoamericanos
                                </h3><br>
                                <a class="btn btn-lg btn-common fadeInUp wow" data-sppb-wow-delay="1.4s" href="#">
                                    <i class="fa fa-download"></i>¡Quiero saber mas!
                                </a>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="{{ URL::asset('images/slider/slider02.jpg') }}" alt="">
                            <div class="carousel-caption">
                                <h2 class="fadeInUp wow" data-sppb-wow-delay="0.8s">
                                    ¿Porqué viajar a Rusia y no otro país?
                                </h2>
                                <h3 class="fadeInUp wow slide_h3" data-sppb-wow-delay="1.2s">
                                    Acceso preferencial para estudiantes de Latinoamérica en las mejores universidades
                                </h3><br>
                                <a class="btn btn-lg btn-common fadeInUp wow" data-sppb-wow-delay="1.4s" href="#">
                                    <i class="fa fa-coffee"></i>¡Quiero saber mas!
                                </a>
                            </div>
                        </div>
                        <div class="carousel-item ">
                            <img class="d-block w-100" src="images/slider/slider01.jpg" alt="">
                            <div class="carousel-caption">
                                <h2 class="fadeInUp wow" data-sppb-wow-delay="0.8s">
                                    ¿Porqué viajar a Rusia y no otro país?
                                </h2>
                                <h3 class="fadeInUp wow slide_h3" data-sppb-wow-delay="1.2s">
                                    Soporte de visado universitario que asegura tu viaje y estadía en Rusia
                                </h3><br>
                                <a class="btn btn-lg btn-common fadeInUp wow" data-sppb-wow-delay="1.4s" href="#">
                                    <i class="fa fa-download"></i>¡Quiero saber mas!
                                </a>
                            </div>
                        </div>
                        <div class="carousel-item ">
                            <img class="d-block w-100" src="images/slider/slider6.jpg" alt="">
                            <div class="carousel-caption">
                                <h2 class="fadeInUp wow" data-sppb-wow-delay="0.8s">
                                    ¿Porqué viajar a Rusia y no otro país?
                                </h2>
                                <h3 class="fadeInUp wow slide_h3" data-sppb-wow-delay="1.2s">
                                    La mejor y más enriquecedora experiencia académica y cultural donde se unen oriente y occidente
                                </h3><br>
                                <a class="btn btn-lg btn-common fadeInUp wow" data-sppb-wow-delay="1.4s" href="#">
                                    <i class="fa fa-download"></i>¡Quiero saber mas!
                                </a>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carousel-slider" role="button" data-slide="prev">
                        <span class="carousel-control carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel-slider" role="button" data-slide="next">
                        <span class="carousel-control carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </header>
        <div id="app">
            <buscador></buscador>
        </div>
        <footer>
            <div id="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <p class="copyright-text">
                            © ALAR Intl LLC - 16192 Coastal Hwy  Lewes , DE 19958. USA | Asociación Latinoamericano Rusa 1999 - {{date("Y")}}. ALAR - Te preparamos para triunfar en Rusia<br>
                            <a href="#" rel="nofollow">Términos de uso sitio web</a> | <a href="#" rel="nofollow">Términos de uso sitio web</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <a href="#" class="back-to-top">
            <i class="fa fa-angle-up">
            </i>
        </a>
        <div id="preloader">
            <div class="loader" id="loader-1"></div>
        </div>
        <div class="modal fade" id="modalLogin" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modalLogin-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">ALAR</h5>
                        <button type="button" class="close closeLogin" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body2">
                        <div class="infoModal" id="infoModal">
                            <div class="containerInfo">
                                <div><p class="titleInfo">Regístrate y genera tu Cuenta&Carpeta personal. Esto te permitirá:</p></div>
                                <div class="listInfo">
                                    <div class="contList">
                                        <img style="width:40px" src="images/modal_inicio/5.svg" alt="" srcset="">
                                        <span class="itemInfo" >Iniciar tu proceso de inscripción para viajar a Rusia</span>
                                    </div>
                                    <div class="contList">
                                        <img style="width:40px" src="images/modal_inicio/2.svg" alt="" srcset="">
                                        <span class="itemInfo" >Iniciar tu Preparación de Idioma Ruso</span>
                                    </div>
                                    <div  class="contList">
                                        <img style="width:40px" src="images/modal_inicio/1.svg" alt="" srcset="">
                                        <span class="itemInfo" >Obtener la ‘Guía del estudiante en Rusia’</span>
                                    </div>
                                    <div  class="contList">
                                        <img style="width:40px" src="images/modal_inicio/3.svg" alt="" srcset="">
                                        <span class="itemInfo" >Organizar toda tu documentación para su gestión en Rusia</span>
                                    </div>
                                    <div  class="contList">
                                        <img style="width:40px" src="images/modal_inicio/4.svg" alt="" srcset="">
                                        <span class="itemInfo" >Recibir notificaciones y documentos para tu visa y viaje</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="register">
                            <div class="bodyForm">
                                <div class="containerSociales">
                                    <div class="backStart" id="backStart">
                                        <svg width="20" height="20" style="display: inline-block; vertical-align: middle;"><path d="M20 8.8H4.7l7-7L10 0 0 10l10 10 1.8-1.8-7-7H20V8.8z" fill-rule="evenodd"></path></svg>
                                        <span>Volver</span>
                                    </div>
                                    <div>
                                        <p class="titleLogin">Ingresa con tus redes sociales</p>
                                    </div>
                                    <div>
                                        <a class="btn btn-redes fb" href="{{url('login/facebook')}}">
                                            <svg width="28" height="28" style="display: inline-block; vertical-align: middle;"><path d="M24.23 0H3.77A3.77 3.77 0 0 0 0 3.77v20.46A3.77 3.77 0 0 0 3.77 28h10.1V18h-2.6a.61.61 0 0 1-.6-.62l-.02-3.22a.61.61 0 0 1 .62-.62h2.6v-3.11c0-3.62 2.2-5.59 5.43-5.59h2.64a.61.61 0 0 1 .62.61v2.72a.61.61 0 0 1-.62.62h-1.62c-1.76 0-2.1.83-2.1 2.05v2.7h3.86a.61.61 0 0 1 .6.69l-.37 3.22a.61.61 0 0 1-.61.55h-3.46l-.02 10h6A3.77 3.77 0 0 0 28 24.23V3.77A3.77 3.77 0 0 0 24.23 0z" fill="#fff"></path></svg>
                                            <span>Facebook</span>
                                        </a>
                                    </div>
                                    <div>
                                        <a class="btn btn-redes goo" href="{{ url('/auth/redirect/google')}}">
                                            <svg width="28" height="28" style="display: inline-block; vertical-align: middle;"><defs>
                                                <path id="a" d="M17.79 7.364H9.21v3.477h4.939c-.46 2.209-2.386 3.477-4.94 3.477-3.014 0-5.442-2.373-5.442-5.318 0-2.945 2.428-5.318 5.442-5.318A5.4 5.4 0 0 1 12.6 4.868l2.68-2.618C13.646.86 11.552 0 9.21 0 4.101 0 0 4.01 0 9s4.102 9 9.21 9C13.813 18 18 14.727 18 9c0-.532-.084-1.105-.21-1.636z"/>
                                                <path id="c" d="M17.79 7.364H9.21v3.477h4.939c-.46 2.209-2.386 3.477-4.94 3.477-3.014 0-5.442-2.373-5.442-5.318 0-2.945 2.428-5.318 5.442-5.318A5.4 5.4 0 0 1 12.6 4.868l2.68-2.618C13.646.86 11.552 0 9.21 0 4.101 0 0 4.01 0 9s4.102 9 9.21 9C13.813 18 18 14.727 18 9c0-.532-.084-1.105-.21-1.636z"/>
                                                <path id="e" d="M17.79 7.364H9.21v3.477h4.939c-.46 2.209-2.386 3.477-4.94 3.477-3.014 0-5.442-2.373-5.442-5.318 0-2.945 2.428-5.318 5.442-5.318A5.4 5.4 0 0 1 12.6 4.868l2.68-2.618C13.646.86 11.552 0 9.21 0 4.101 0 0 4.01 0 9s4.102 9 9.21 9C13.813 18 18 14.727 18 9c0-.532-.084-1.105-.21-1.636z"/>
                                                <path id="g" d="M17.79 7.364H9.21v3.477h4.939c-.46 2.209-2.386 3.477-4.94 3.477-3.014 0-5.442-2.373-5.442-5.318 0-2.945 2.428-5.318 5.442-5.318A5.4 5.4 0 0 1 12.6 4.868l2.68-2.618C13.646.86 11.552 0 9.21 0 4.101 0 0 4.01 0 9s4.102 9 9.21 9C13.813 18 18 14.727 18 9c0-.532-.084-1.105-.21-1.636z"/></defs>
                                                <g fill="none" fill-rule="evenodd">
                                                <path fill="#FFF" stroke="#4285F4" d="M49.5 49.5V.5H5A4.5 4.5 0 0 0 .5 5v40A4.5 4.5 0 0 0 5 49.5h44.5z"/>
                                                <g transform="translate(5 5)">
                                                <mask id="b" fill="#fff">
                                                <use xlink:href="#a"/>
                                                </mask>
                                                <path fill="#FBBC05" fill-rule="nonzero" d="M-.837 14.318V3.682L6.279 9z" mask="url(#b)"/>
                                                </g>
                                                <g transform="translate(5 5)">
                                                <mask id="d" fill="#fff">
                                                    <use xlink:href="#c"/>
                                                </mask>
                                                <path fill="#EA4335" fill-rule="nonzero" d="M-.837 3.682L6.279 9l2.93-2.495 10.047-1.596V-.818H-.837z" mask="url(#d)"/>
                                                </g>
                                                <g transform="translate(5 5)">
                                                <mask id="f" fill="#fff">
                                                    <use xlink:href="#e"/>
                                                </mask>
                                                <path fill="#34A853" fill-rule="nonzero" d="M-.837 14.318L11.72 4.91l3.307.41 4.228-6.137v19.636H-.837z" mask="url(#f)"/>
                                                </g>
                                                <g transform="translate(5 5)">
                                                <mask id="h" fill="#fff">
                                                <use xlink:href="#g"/>
                                                </mask>
                                                <path fill="#4285F4" fill-rule="nonzero" d="M19.256 18.818L6.279 9 4.605 7.773l14.65-4.091z" mask="url(#h)"/>
                                                </g>
                                                </g>
                                            </svg>
                                            <span>Google</span>
                                        </a>
                                    </div>
                                    <div>
                                        <p class="subTitleLogin"> Ó ingresa con tu usuario</p>
                                    </div>
                                    <div class=" btnRegistro">
                                        <button class="btn btn-redes reg" id="showFormRegister">Llenar formulario</button>
                                    </div>
                                </div>
                                <form id="formLogin" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                    <div class="containerLogin">
                                        <input type="text" class="inputLog" placeholder="Introduzca su correo" id="emailLog" name="email">
                                        <input type="password" name="password" class="inputLog" placeholder="Introduzca su contraseña" id="passLog">
                                        <div>
                                            <a href="#" id="forgot_pw" >Olvidé mi contraseña</a>
                                        </div>
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Ingresar') }}
                                        </button>
                                        <!-- <button name="auth" class="btn input-button" id="auth">Iniciar Sesión</button> -->
                                        <div>
                                            <p>¿Aún no tienes cuenta?<a href="#" id="showForm"> Registrate</a></p>
                                        </div>
                                    </div>
                                </form>
                                <form id="register" method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="containerLogin">
                                        <div><h3 class="login">Registro</h3></div>
                                        <input type="text" class="inputLog login" placeholder="Introduzca sus Nombres ejm: Carlos Rogelio" id="name" name="name" value="">
                                        <input type="text" class="inputLog login" placeholder="Introduzca sus Apellidos ejm: Rodriguez Ibañez" id="lastname" name="lastname" value="">
                                        <input type="text" class="inputLog login" placeholder="Introduzca su Correo ejm: carlos@gmail.com" id="mailUser" name="email" value="">
                                        <input type="password" class="inputLog login" placeholder="Introduzca su contraseña" id="passUser" name="password" value="">
                                        <input type="password" class="inputLog login" placeholder="Repita su contraseña" id="repet_passUser" name="password_confirmation" value="">
                                        <input type="submit" name="ingresar" class="btn input-button login" value="Registrarse">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div> -->
                </div>
            </div>
        </div>
        <script src="{{ URL::asset('js/app.js') }}"></script>

        <script src="{{ URL::asset('js/popper.min.js') }}"></script>
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.mixitup.js') }}"></script>
        <script src="{{ URL::asset('js/smoothscroll.js') }}"></script>
        <script src="{{ URL::asset('js/wow.js') }}"></script>
        <script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.slicknav.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.appear.js') }}"></script>
        <script src="{{ URL::asset('js/main.js') }}"></script>


            <script src="{{ URL::asset('js/admin_alar.js') }}"></script>
            <script src="{{ URL::asset('js/sweetalert2.js') }}"></script>
            <script type="text/javascript">
            $(document).ready(function() {
                    $('.js-example-basic-multiple').select2({
                        minimumResultsForSearch :3
                    });
                    var tipo="<?php
                    if(isset($_GET["tipo"])){
                        echo $_GET["tipo"] ;
                    }else{
                        echo "No existe";
                    }
                ?>";
                if(tipo!=="No existe"){
                    if(tipo!==0){
                        $("#selTipoEstudio").val(tipo);
                        if (tipo == 1 || tipo == 2 || tipo == 4){
                            // $("#div-rama,#div-idioma").show();
                            $("#div-rama").show();
                            $("#div-perfil").hide();
                            $("#selRama").val(0);
                            $("#rdIdioma1").prop("checked", true);
                        } else {
                            $("#selPerfil").val(0);
                            $("#selRama").val(0);
                            // $("#div-rama,#div-idioma,#div-perfil").hide();
                            $("#div-rama,#div-perfil").hide();
                        }
                    }
                }
                $("#selTipoEstudio").change(function(){
                    TipEst = $("#selTipoEstudio").val();
                    if (TipEst == 1 || TipEst == 2 || TipEst == 4){
                        // $("#div-rama,#div-idioma").show();
                        $("#div-rama").show();
                        $("#div-perfil").hide();
                        $("#selRama").val(0);
                        $("#rdIdioma1").prop("checked", true);
                    } else {
                        $("#selPerfil").val(0);
                        $("#selRama").val(0);
                        // $("#div-rama,#div-idioma,#div-perfil").hide();
                        $("#div-rama,#div-perfil").hide();
                    }
                });
                $("#selRama").change(function(){
                    TipRam= $("#selRama").val();
                    TipEst = $("#selTipoEstudio").val();
                    if(TipRam !== 0  & TipEst == 1 || TipEst == 2 || TipEst == 4){
                        // var param={
                        // 	"oper":"list",
                        // 	"idRama":TipRam
                        // }
                        $.ajax({
                            url : 'api/programaAll/'+TipRam,
                            // data : param,
                            method : 'get',
                            success : function(response){
                                if(!response.error && response!=='[]') {
                                    // let perfil = JSON.parse(response);// CONVIERTE A JSON UN STRING
                                    let template = '<option value="0" selected>Todos os perfis</option>';
                                    response.forEach(perf => {
                                    template += '<option value="'+perf.id+'">'+perf.descripcion+'</option>';
                                    });
                                    $('#selPerfil').html(template);
                                }
                            }
                        });
                        $("#div-perfil").show();
                        $("#selPerfil").val(0);
                    }else	{
                        $("#div-perfil").hide();
                    }
                });
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent)) {
                $("#carousel-area").css("display","none");
                $("#infoModal").css("display","none");
                }
            });
        </script>

    </script>

@if (session('email_recuperar_password'))
    <script type="text/javascript">
        $(document).ready(function() {
            setTimeout(() => {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top',
                    showConfirmButton: false,
                    timer: 10000,
                    timerProgressBar: false,
                    customClass: {
                        container: 'adelante'
                    },
                    showClass: {
                        popup: "animate__animated animate__fadeInRight"
                    },
                    hideClass: {
                        popup: "animate__animated animate__fadeOutRight"
                    },
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })

                Toast.fire({
                    icon: '{{session('icon')}}',
                    title: '{{ session('email_recuperar_password') }}'
                })

            }, 1000);
        });
    </script>
@endif

        <script src="{{ URL::asset('js/modal/script_zoneUser.js')}}"></script>
        <script src="{{ URL::asset('js/modal/script.js')}}"></script>
    </body>
</html>
