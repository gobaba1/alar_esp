@extends('layouts.master')
@section('content')
<section class="content">
    <!-- LEYENDA -->
    <div class="container-fluid">
        <div class="card">
            <div class="card-header">
                LEYENDA
            </div>
            <div class="card-body">
                <h5><span class="btn btn-sm bg-danger">POR COMPLETAR</span> <span class="btn btn-sm bg-warning">PENDIENTE</span>  <span class="btn btn-sm bg-success">REVISADO</span> <span class="btn btn-sm bg-secondary">RECHAZADO</span></h5>
            </div>
        </div>
    </div>
    <!-- CONTENT ACCESO PREFERENCIAL -->
    <div class="container-fluid">
            <div class="card">
                <div class="card-header">
                    Programa de Acceso Preferencial
                </div>
                <div class="card-body row" style="width:100%">
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="info-box {{$datosFormulario['bgFormulario']}}">
                            <input type="hidden" name="validador" id="validador_formulario" value="{{$validador_formulario}}">
                            <span class="info-box-icon"><i class="fa fa-edit"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Formulario de inscripción </span>
                                <span class="info-box-number">{{$datosFormulario['levelFormulario']}}/2</span>
                                <button type="button" class="btn btn-block btn-default btn-sm"  {{$datosFormulario['propertyFormulario']}}>{{$datosFormulario['textFormulario']}}</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="info-box {{$datosContrato['bgContrato']}}">
                        <span class="info-box-icon"><i class="fa fa-edit"></i></span>
                        <div class="info-box-content ">
                            <span class="info-box-text">Contrato de T&C</span>
                            <span class="info-box-number">{{$datosContrato['levelContrato']}}/2</span>
                            @if($datosFormulario['bgFormulario']!=='bg-danger')
                            <button type="button" class="btn btn-block btn-default btn-sm" {{ $datosContrato['propertyContrato']}} >{{$datosContrato['textContrato']}}</button>
                            @else
                            <button type="button" class="btn btn-block btn-default btn-sm" id="modal_firmar_contrato">{{$datosContrato['textContrato']}}</button>
                            @endif
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-4 col-sm-6 col-12">
                        <div class="info-box {{$datosPasaporte['bgPasaporte']}}">
                        <span class="info-box-icon"><i class="fa fa-upload"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Pasaporte</span>
                            <span class="info-box-number">{{$datosPasaporte['levelPasaporte']}}/2</span>
                            @if($datosFormulario['bgFormulario']!=='bg-danger')
                                <button type="button" class="btn btn-block btn-default btn-sm" {{$datosPasaporte['propertyPasaporte']}} >{{$datosPasaporte['textPasaporte']}}</button>
                            @else
                                <button type="button" class="btn btn-block btn-default btn-sm" id="modal_upload_passport">{{$datosPasaporte['textPasaporte']}}</button>
                            @endif
                        </div>
                        <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
    </div><!-- /.container-fluid -->
    <!-- CONTENT Documentos Academicos y OTROS DOCUMENTOS -->
    <div class="container-fluid">
        <!-- <h3 class="mt-4 mb-2">Documentos Por Subir</h3> -->
        <div class="row">
            <div class="col-md-6 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        Documentos Academicos
                        <br>
                        <p>Nota: No es necesario subir todos los documentos</p>
                    </div>
                    <div class="card-body">
                    @if($datosFormulario['bgFormulario']!=='bg-danger')
                        <button type="button" class="btn btn-block btn-default" data-toggle="modal" data-target=".modal_upload_documents">Subir</button>
                    @else
                        <button type="button" class="btn btn-block btn-default" id="modal_upload_documents">Subir</button>
                    @endif
                    </div>
                    <div class="card-body table-responsive col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tipo</th>
                                <!-- <th scope="col">Fecha</th> -->
                                <th scope="col">Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($data_doc_academicos)>0)
                                    @foreach ($data_doc_academicos as $index=>$academics)
                                        <tr>
                                            <th scope="row">{{$index+1}}</th>
                                            <td>{{$academics['nameArchivo']}}</td>
                                            <td>
                                                    @if($academics['state']=='0')
                                                        <span class="btn btn-sm bg-secondary" style="width:10em">
                                                            Rechazado
                                                        </span>
                                                    @elseif($academics['state']=='1')
                                                        <span class="btn btn-sm bg-warning" style="width:10em">
                                                            Pendiente
                                                        </span>
                                                    @elseif($academics['state']=='2')
                                                        <span class="btn btn-sm bg-success" style="width:10em">
                                                            Revisado
                                                        </span>
                                                    @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-12">
                <div class="card">
                    <div class="card-header">
                        Otros documentos
                        <br>
                        <p>Nota: Subir solo los documentos pedidos por parte de ALAR</p>
                    </div>
                    <div class="card-body">
                        @if($datosFormulario['bgFormulario']!=='bg-danger')
                            <button type="button" class="btn btn-block btn-default"  data-toggle="modal" data-target=".modal_upload_otherDocuments">Subir</button>
                        @else
                            <button type="button" class="btn btn-block btn-default" id="modal_upload_otherDocuments">Subir</button>
                        @endif
                    </div>
                    <div class="card-body table-responsive col-md-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Tipo</th>
                                <!-- <th scope="col">Fecha</th> -->
                                <th scope="col">Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($data_otherDocuments)>0)
                                        @foreach ($data_otherDocuments as $index=>$documents)
                                            <tr>
                                                <th scope="row">{{$index+1}}</th>
                                                <td>{{$documents['nameArchivo']}}</td>
                                                <td>
                                                    @if($documents['state']=='0')
                                                        <span class="btn btn-sm bg-secondary" style="width:10em">
                                                            Rechazado
                                                        </span>
                                                    @elseif($documents['state']=='1')
                                                        <span class="btn btn-sm bg-warning" style="width:10em">
                                                            Pendiente
                                                        </span>
                                                    @elseif($documents['state']=='2')
                                                        <span class="btn btn-sm bg-success" style="width:10em">
                                                            Revisado
                                                        </span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- CONTENT ARCHIVOS ALAR -->
    <div class="container-fluid">
        <!-- <h3 class="mt-4 mb-2">Documentos Por Subir Por Parte De Alar</h3> -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        Documentos Por Subir Por Parte De Alar
                    </div>
                    <div class="card-body table-responsive col-md-12 col-sm-12 col-12">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Descripción</th>
                                <!-- <th scope="col">Estado</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($documents_to_user)>0)
                                        @foreach ($documents_to_user as $index=>$documents)
                                            <tr>
                                                <th scope="row">{{$index+1}}</th>
                                                <td>{{$documents['type']}}</td>
                                                <td><a href="{{$documents['url_drive']}}" target="_blank">{{$documents['url_drive']}}</a></td>
                                                <!-- <td>
                                                        @if($documents['state']=='1')
                                                            <span class="btn btn-sm bg-warning" style="width:10em">
                                                                Pendiente
                                                            </span>
                                                        @else
                                                            <span class="btn btn-sm bg-success" style="width:10em">
                                                                Revisado
                                                            </span>
                                                        @endif
                                                </td> -->
                                            </tr>
                                        @endforeach
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL DE INSCRIPCION -->
    <div class="modal fade bd-example-modal-lg modal_upload_form" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="false" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black !important">Formulario de Inscripción <span id="numero_paso">1/5</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" id="formulario_inscripcion" >
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="idUser" id="id_user" value="">
                    <div class="modal-body">
                        <div id="primeraParte">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Solicito Inscripción y vacante a la Facultad de Idioma: </h4>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="facultad_necesita" id="noFacultad" value="0" @if($data_formulario[0]->facultad_necesita=='0') checked @endif>
                                                <label class="form-check-label" >No necesito Facultad de Idioma, estudiaré en Inglés</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row pt-2 pb-2">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="facultad_necesita" id="exoFacultad" value="1" @if($data_formulario[0]->facultad_necesita=='1') checked @endif>
                                                <label class="form-check-label">Solicito me exoneren de la Facultad de Idioma pues domino el Ruso</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="facultad_necesita" id="siCurso" value="2" @if($data_formulario[0]->facultad_necesita=='2') checked @endif>
                                                <label class="form-check-label">Sí solicite el curso de idioma</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="@if($data_formulario[0]->facultad_necesita==2) display:block @else display:none @endif"  id="datosCurso">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-6">
                                                <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="faculdad_mes">En</label>
                                                </div>
                                                <select class="custom-select" name="facultad_mes" id="facultad_mes" >
                                                    <option value="">Elegir...</option>
                                                    <option value="Enero"  @if($data_formulario[0]->facultad_mes=='Enero') selected @endif>Enero</option>
                                                    <option value="Marzo" @if($data_formulario[0]->facultad_mes=='Marzo') selected @endif>Marzo</option>
                                                    <option value="Septiembre" @if($data_formulario[0]->facultad_mes=='Septiembre') selected @endif>Septiembre</option>
                                                </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <label class="input-group-text" for="facultad_anio">del año</label>
                                                </div>
                                                <select class="custom-select" name="facultad_anio" id="facultad_anio" >
                                                    <option value="">Elegir...</option>
                                                    <option value="2020" @if($data_formulario[0]->facultad_anio=='2020') selected @endif>2020</option>
                                                    <option value="2021" @if($data_formulario[0]->facultad_anio=='2021') selected @endif>2021</option>
                                                    <option value="2022" @if($data_formulario[0]->facultad_anio=='2022') selected @endif>2022</option>
                                                </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">En la Universidad: </span>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="Ej: Belgorod" name="facultad_universidad" id="facultad_universidad" value="@isset($data_formulario[0]->facultad_universidad) {{$data_formulario[0]->facultad_universidad}} @endisset">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                <h4 class="box-title">Solicito Inscripción y vacante de estudios para estudios de: </h4>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <label class="input-group-text" for="vacante_estudio">En</label>
                                            </div>
                                            <select class="custom-select" name="vacante_estudio" id="vacante_estudio" >
                                                <option value="" >Elegir...</option>
                                                <option value="Pregrado" @if($data_formulario[0]->vacante_estudio=='Pregrado') selected @endif>Pregrado (Licenciatura)</option>
                                                <option value="Maestría" @if($data_formulario[0]->vacante_estudio=='Maestría') selected @endif>Maestría</option>
                                                <option value="Doctorado: (Ph.D)" @if($data_formulario[0]->vacante_estudio=='Doctorado: (Ph.D)') selected @endif> Doctorado: (Ph.D)</option>
                                                <option value="(D.Sc)" @if($data_formulario[0]->vacante_estudio=='(D.Sc)') selected @endif>(D.Sc)</option>
                                                <option value="Especialidad Médica" @if($data_formulario[0]->vacante_estudio=='Especialidad Médica') selected @endif>Especialidad Médica</option>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="col-md-9 col-sm-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">En la Universidad: </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: Belgorod"  name="vacante_universidad" id="vacante_universidad" value="@isset($data_formulario[0]->vacante_universidad) {{$data_formulario[0]->vacante_universidad}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Carrera o tema de estudios: </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: Ingeniería Industrial"  name="vacante_rama" id="vacante_rama" value="@isset($data_formulario[0]->vacante_rama) {{$data_formulario[0]->vacante_rama}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Vuestro Grado académico y/o Título actua: </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: Ingeniería Industrial" name="vancante_gradoActual" id="vancante_gradoActual" value="@isset($data_formulario[0]->vancante_gradoActual) {{$data_formulario[0]->vancante_gradoActual}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">¿Quién financia vuestros estudios?: </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: Ingeniería Industrial"  name="vacante_financia" id="vacante_financia" value="@isset($data_formulario[0]->vacante_financia) {{$data_formulario[0]->vacante_financia}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <div class="box box-primary">
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <button type="subtmit" class="btn btn-primary saveInscripcion" id="saveInscripcion">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="segundaParte" style="display:none">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Apellidos y nombres de los padres:  </h4>
                                    <div class="my-2">
                                        <i>
                                            Si no cuenta con la informacion necesaria, por favor completar con la palabra "ausente" en el campo
                                        </i>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Madre: </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: Maria Perez Cordova" name="madre_nombre" id="madre_nombre" value="@isset($data_formulario[0]->madre_nombre) {{$data_formulario[0]->madre_nombre}} @endisset">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Correo: </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: maria@hotmail.com"  name="madre_correo" id="madre_correo" value="@isset($data_formulario[0]->madre_correo) {{$data_formulario[0]->madre_correo}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Padre:</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: Pablo Rodriguez Santillan"  name="padre_nombre" id="padre_nombre" value="@isset($data_formulario[0]->padre_nombre) {{$data_formulario[0]->padre_nombre}} @endisset">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Correo: </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: pablo@hotmail.com" name="padre_correo" id="padre_correo" value="@isset($data_formulario[0]->padre_correo) {{$data_formulario[0]->padre_correo}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Dirección actual, ciudad y código postal:</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: avenida Olivare 165-Lima- cod 763" name="padres_direccion" id="padres_direccion" value="@isset($data_formulario[0]->padres_direccion) {{$data_formulario[0]->padres_direccion}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Prefijo de ciudad, número de casa:</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: +873 7562830" name="padres_telefono" id="padres_telefono" value="@isset($data_formulario[0]->padres_telefono) {{$data_formulario[0]->padres_telefono}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Prefijo de ciudad, celular (móvil):</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: +873 986629070" name="padres_celular" id="padres_celular" value="@isset($data_formulario[0]->padres_celular) {{$data_formulario[0]->padres_celular}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div>
                                    <div class="row">
                                        <div class="col-12 text-center">
                                            <button type="subtmit" class="btn btn-primary saveInscripcion" id="saveInscripcion">Guardar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="terceraParte" style="display:none">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Persona a quien informar sobre Usted (en caso de emergencia, urgencia o necesidad): </h4>
                                    <div class="my-2">
                                        <i>
                                            Si no cuenta con la informacion necesaria, por favor completar con la palabra "ausente" en el campo
                                        </i>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Nombres y Apellidos: </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: Maria Perez Cordova" name="respon_nombre" id="respon_nombre" value="@isset($data_formulario[0]->padres_celular) {{$data_formulario[0]->padres_celular}} @endisset">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Correo: </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: maria@hotmail.com"  name="respon_correo" id="respon_correo" value="@isset($data_formulario[0]->respon_correo) {{$data_formulario[0]->respon_correo}} @endisset" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Prefijo de ciudad, número de casa:</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: +873 7562830" name="respon_telefono" id="respon_telefono" value="@isset($data_formulario[0]->respon_telefono) {{$data_formulario[0]->respon_telefono}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Prefijo de ciudad, celular (móvil):</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: +873 986629070" name="respon_celular"  id="respon_celular" value="@isset($data_formulario[0]->respon_celular) {{$data_formulario[0]->respon_celular}} @endisset" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Nombres y apellidos del apoderado o persona responsable: </h4>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Nombres y Apellidos:</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: +873 7562830" name="apoder_nombre" id="apoder_nombre" value="@isset($data_formulario[0]->apoder_nombre) {{$data_formulario[0]->apoder_nombre}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">N° Documento de identidad:</span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Ej: +873 7562830"  name="apoder_dni" id="apoder_dni" value="@isset($data_formulario[0]->apoder_dni) {{$data_formulario[0]->apoder_dni}} @endisset">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <button type="subtmit" class="btn btn-primary saveInscripcion" id="saveInscripcion">Guardar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div id="cuartaParte" style="display:none">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h4 class="box-title">Firmar y Subir documento </h4>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label for="">¿Tienes tu firma escaneada?</label>
                                        <select class="form-control" name="" id="firma_escaneada">
                                            <option value="">Elegir</option>
                                            <option value="si">Sí</option>
                                            <option value="no">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="si_firma_escaneada" style="display:none">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-check"><p>1. Deale click al cuadrado y suba su firma</p></div>
                                </div>
                                <!-- <div class="col-md-12 col-sm-12" style="align-items: center; justify-content: center; display: flex;">
                                    <div class="form-check">
                                        <input type="file" id="upload_passport" value="Subir Contrato de Términos y Condiciones" name="thing" style="border: 1px solid rgb(204, 204, 204); display: inline-block; padding: 6px 12px; cursor: pointer; width: 100%;">
                                    </div>
                                </div> -->
                                <form action="{{url('/descargar_formulario_i')}}" method="POST" target="_blank" enctype="multipart/form-data">
                                    @csrf
                                    <label class="cabinet center-block">
                                        <figure>
                                            <img class="gambar img-responsive" id="item-img-output" />
                                            <figcaption><i class="fa fa-camera"></i></figcaption>
                                            <input type="file" class="item-img file center-block" name="firma_digital">
                                        </figure>

                                    </label>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-check"><p>Luego de subir su firma deale click en el boton descargar</p></div>
                                    </div>
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-primary" onclick="" style="cursor:no-drop;opacity: 0.65" id="EnviarPDF">descargar</button>
                                    </div>
                                </form>
                            </div>
                            <div class="row" id="no_firma_escaneada" style="display:none">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-check"><p>1.Descargue el formulario</p></div>
                                    <div class="col-md-12 col-sm-12" style="align-items: center; justify-content: center; display: flex;">
                                        <div class="form-check">
                                            <form action="{{url('/descargar_formulario_i')}}" method="POST" target="_blank" enctype="multipart/form-data">
                                                @csrf
                                                <button type="submit" class="btn btn-primary btn-sm">Descargar Formulario</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-check"><p>Firme en los campos correspondientes y escanee del documento</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="quintaParte" style="display:none">
                        <form method="post" action="/upload" id="" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                            <div id="">
                                <input type="hidden" name="folder_id" id="" value="{{$folder_id[0]->folder_id}}">
                                <input type="hidden" name="file_name" id="" value="formulario_pdf">
                                <input type="hidden" name="file_type" id="" value="preferencial">
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h4 class="box-title"><strong>Subir documento</strong></h4>
                                        </div>
                                        <!-- /.box-header -->
                                        <!-- form start -->
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-check">
                                                        <p>Suba su documento firmado</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12" style="align-items: center;justify-content: center;display: flex;">
                                                    <div class="form-check">
                                                        <input type="file" style="border: 1px solid #ccc; display: inline-block;padding: 6px 12px;cursor: pointer; width: 100%" id="upload_passport" value="Subir formulario" name="thing">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn_upload_passport">Subir</button>
                            </div>
                        </form>
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="back1" numberB="0"><<</button>
                        <button type="button" class="btn btn-secondary" id="next1">>></button>
                    </div>
            </div>
        </div>
    </div>
    <!-- MODAL DE INSCRIPCION - FIRMAR CONTRATO-->
    <div class="modal fade bd-example-modal-lg modal_firmar_contrato" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black !important">Formulario de Inscripción</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post" action="/upload" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div id="">
                            <input type="hidden" name="folder_id"  value="{{$folder_id[0]->folder_id}}">
                            <input type="hidden" name="file_name"  value="contrato">
                            <input type="hidden" name="file_type"  value="preferencial">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title"><strong>Primer paso</strong></h4>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-check">
                                                <p>Primero descargue el Contrato de Términos y Condiciones</p>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12" style="align-items: center;justify-content: center;display: flex;">
                                            <div class="form-check">
                                            <a href="ContratoTyC/TerminosCondiciones-CR.pdf" class="btn btn-primary btn-sm" target="_blank">Contrato de Términos y Condiciones</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title"><strong>Segundo paso</strong></h4>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-check">
                                                <p>Firme en los campos correspondientes del documento</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                            </div>

                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title"><strong>Tercer paso</strong></h4>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-check">
                                                <p>Escanee el documento y súbalo</p>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12" style="align-items: center;justify-content: center;display: flex;">
                                            <div class="form-check">
                                                <input type="file" style=" border: 1px solid #ccc; display: inline-block;padding: 6px 12px;cursor: pointer; width: 100%" id="terminos_condiciones" name="thing" value="Subir Contrato de Términos y Condiciones">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.box-body -->
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="save_TyC">Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- MODAL UPLOAD PASSPORT-->
    <div class="modal fade bd-example-modal-lg modal_upload_passport" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black">Subir Pasaporte</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        <form method="post" action="/upload" id="" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
            <div id="">
                <input type="hidden" name="folder_id" id="" value="{{$folder_id[0]->folder_id}}">
                <input type="hidden" name="file_name" id="" value="pasaporte">
                <input type="hidden" name="file_type" id="" value="preferencial">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4 class="box-title"><strong>Scanear y subir pasaporte</strong></h4>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-check">
                                        <p>Escanee el documento y súbalo</p>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12" style="align-items: center;justify-content: center;display: flex;">
                                    <div class="form-check">
                                        <input type="file" style="border: 1px solid #ccc; display: inline-block;padding: 6px 12px;cursor: pointer; width: 100%" id="upload_passport" value="Subir Contrato de Términos y Condiciones" name="thing">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="btn_upload_passport">Subir</button>
            </div>
        </form>
            </div>
        </div>
    </div>
    <!-- MODAL SUBIR DOCUMENTOS -->
    <div class="modal fade bd-example-modal-lg modal_upload_documents" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black">Subir Documentos Acádemicos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        <form method="post" action="/upload" id="formulario_upload_documents" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                    <div class="box box-primary">
                        <div class="card-body">
                            <div class="form-group">
                                <input type="hidden" name="folder_id" id="" value="{{$folder_id[0]->folder_id}}">
                                <input type="hidden" name="file_type" id="" value="academicos">
                                <label for="">ELige el tipo de documento</label>
                                <!-- <input id="nameDocument" type="text" class="form-control" id="" placeholder="Ejemplo: Certificado de Estudios"> -->
                                <select name="file_name" id="nameDocument" class="form-control" required>
                                    <option value="">Elegir</option>
                                    <option value="Certificado de Estudios Secundaria" {{ $data_disables_academicos['state_Secundaria']}}>Certificado de Estudios Secundaria</option>
                                    <option value="Certificado de Estudios Universidad" {{ $data_disables_academicos['state_Universitario']}}>Certificado de Estudios Universidad</option>
                                    <option value="Partida de Nacimiento" {{ $data_disables_academicos['state_Partida']}} >Partida de Nacimiento</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Suba el archivo</label>
                                <input type="file" style="border: 1px solid #ccc; display: inline-block;padding: 6px 12px;cursor: pointer; width: 100%" id="upload_document" value="Subir Contrato de Términos y Condiciones" name="thing" required>
                                <!-- <input type="file" name="thing" class="form-control" id="upload_document" > -->
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            <div class="modal-footer">
                <!-- <button type="submit" class="btn btn-primary" id="btn_upload_document">Subir otro archivo</button> -->
                <button type="submit" class="btn btn-primary" id="btn_upload_other_documents" >Subir</button>
            </div>
        </form>
            </div>
        </div>
    </div>
    <!-- MODAL SUBIR OTROS DOCUMENTOS -->
    <div class="modal fade bd-example-modal-lg modal_upload_otherDocuments" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black">Subir Otros Documentos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
        <form method="post" action="/upload" id="formulario_upload_documents" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                    <div class="box box-primary">
                        <div class="card-body">
                            <div class="form-group">
                                <input type="hidden" name="folder_id" id="" value="{{$folder_id[0]->folder_id}}">
                                <input type="hidden" name="file_type" id="" value="documentos">
                                <label for="">Escriba el nombre del documento</label>
                                <input id="file_name" name="file_name" type="text" class="form-control" id="" placeholder="Ejemplo: Constacia Penal">
                            </div>
                            <div class="form-group">
                                <label for="">Suba el archivo</label>
                                <input type="file" style="border: 1px solid #ccc; display: inline-block;padding: 6px 12px;cursor: pointer; width: 100%" id="upload_document" value="Subir Contrato de Términos y Condiciones" name="thing" required>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            <div class="modal-footer">
                <!-- <button type="submit" class="btn btn-primary" id="btn_upload_document">Subir otro archivo</button> -->
                <button type="submit" class="btn btn-primary" id="btn_upload_other_documents" >Subir</button>
            </div>
        </form>
            </div>
        </div>
    </div>
    <!-- MODAL IMAGEN -->
    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" style="color:black">Editar Foto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body d-flex justify-content-center">
                    <div id="upload-demo" class="center-block" ></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Delimitar</button>
                </div>
            </div>
        </div>
    </div>
</section>
@push('mi_inscripcion')
<script src="{{ URL::asset('js/inscripcion.js') }}"></script>
@endpush
@endsection
