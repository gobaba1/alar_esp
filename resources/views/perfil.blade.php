@extends('layouts.master')
@section('content')
<section class="container-fluid">
    @if(session('success'))
      <div class="alert alert-success" role="alert">
          {{ session('success') }}
      </div>
    @endif
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-2">
          <div class="card">
            <div class="card-header text-center">
              Esta foto se usara para los documentos.
            </div>
          </div>
          <label class="cabinet center-block">
            <figure>
                <img
                @if ($foto_perfil)
                    src="{{url('/miniatura/'.auth()->id().'.jpg')}}"
                @else
                    src="assets/common_img/defaultUser.png"
                @endif
                class="gambar img-responsive img-thumbnail" id="item-img-output" />
              <figcaption><i class="fa fa-camera"></i></figcaption>
              <input type="file" class="item-img file center-block" name="file_photo"/>
              <input type="hidden" id="user_id" value="{{auth()->id()}}">
            </figure>
          </label>
        </div>
        <div class="col-md-12 col-sm-12 col-lg-10">
          <form action="/perfil/{{$perfil[0]->id}}" method="POST">
            @csrf
            @method('put')
            <div class="card card-secondary" >
              <!-- {{-- INFORMACION PERSONAL --}} -->
              <div class="card-header">
                <h3 class="card-title">Información Personal</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" id="id_collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="form-group col-md-6 col-lg-4 col-sm-12">
                    <input style="display:none" value="{{$user[0]->id}}" name="user_id">
                    <label for="nombres">
                      Nombre
                      <span class="asterisk_red">*</span>
                    </label>
                    <input
                      autocomplete="off"
                      type="text"
                      class="form-control"
                      id="nombres"
                      placeholder="Ingrese su nombre"
                      value="{{$user[0]->name}}"
                      name="name"
                    />
                  </div>
                  <div class="form-group col-md-6 col-lg-4 col-sm-12">
                    <label for="apellidos">
                      Apellidos
                      <span class="asterisk_red">*</span>
                    </label>
                    <input
                      autocomplete="off"
                      type="text"
                      class="form-control"
                      id="apellidos"
                      placeholder="Ingrese su apellido"
                      value="{{$user[0]->lastname}}"
                      name="lastname"
                    />
                  </div>
                  <div class="form-group col-md-6 col-lg-4 col-sm-12">
                    <label for="genero">
                      Género
                      <span class="asterisk_red">*</span>
                    </label>
                    <select class="form-control" id="genero" name="genero" >
                      <option value="">Elegir</option>
                      <option value="m" @if ($perfil[0]->sexo == 'm') selected="selected" @endif>Masculino</option>
                      <option value="f" @if ($perfil[0]->sexo == 'f') selected="selected" @endif>Femenino</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-6 col-lg-4 col-sm-12">
                    <label for="birthday">
                      Fecha de Nacimiento
                      <span class="asterisk_red">*</span>
                    </label>
                    <input
                      autocomplete="off"
                      type="date"
                      class="form-control"
                      id="birthday"
                      name="birthday"
                      value="{{$perfil[0]->fecNacimiento}}"
                    />
                  </div>
                  <div class="form-group col-md-6 col-lg-4 col-sm-12">
                    <label for="pasaporte">Pasaporte</label>
                    <input
                      autocomplete="off"
                      type="text"
                      class="form-control"
                      id="pasaporte"
                      name="pasaporte"
                      placeholder="Ingrese su N° Pasaporte"
                      value="{{$perfil[0]->numPasaporte}}"

                    />
                  </div>

                  <div class="form-group col-md-6 col-lg-4 col-sm-12">
                    <label for="estado_civil">Estado Civil</label>
                    <select class="form-control" id="estado_civil" name="estado_civil" >
                      <option>Elegir</option>
                      <option value="s" @if ($perfil[0]->estado_civil == 's') selected="selected" @endif>Soltero(a)</option>
                      <option value="c" @if ($perfil[0]->estado_civil == 'c') selected="selected" @endif>Casado(a)</option>
                      <option value="d" @if ($perfil[0]->estado_civil == 'd') selected="selected" @endif>Divorciado(a)</option>
                      <option value="v" @if ($perfil[0]->estado_civil == 'v') selected="selected" @endif>Viudo(a)</option>
                    </select>
                  </div>
                </div>
              </div>
              <!-- {{-- CONOCIMIENTOS DE IDIOMAS --}} -->
              <div class="card-header">
                <h3 class="card-title">Conocimientos de Idiomas</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="form-group col-4">
                    <label for="idiomas_state">
                      ¿Tiene algún conocimiento de otros idiomas?
                      <span class="asterisk_red">*</span>
                    </label>
                    <select class="form-control" id="idiomas_state" name="idiomas_state">
                      <option value=" " > Elegir</option>
                      <option  @if(count($perfil_idiomas)>0) selected="selected" @endif value="1">Si</option>
                      <option  value="2">No</option>
                    </select>
                  </div>
                  <div class="col-md-12 col-sm-12 box-dato" style="padding-bottom: 10px;">
                    <button class="addLenguage btn btn-primary" id="addLenguage" @if (count($perfil_idiomas)==0) disabled="disabled" @endif value="1" onclick="return agregarIdioma()" >Añadir otro idioma <i class="fa fa-plus"></i></button>
                  </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <table id="datatable-responsive" class="table">
                            <thead>
                            <tr>
                            </tr>
                            </thead>
                            <tbody >
                                @if(count($perfil_idiomas)>0)
                                    @for ($mxx=0; $mxx < count($perfil_idiomas); $mxx++)
                                      <tr id="fila-<?= $mxx;?>">
                                          <td>
                                              <label class="label-input-format">Idiomas</label>
                                              <select class="form-control input-format marginb-24 lenguage" name="idioma[]" >
                                                  <option value="">Elegir</option>
                                                  <option value="1" @if ($perfil_idiomas[$mxx]['idioma_id'] == '1') selected="selected" @endif>Inglés</option>
                                                  <option value="2" @if ($perfil_idiomas[$mxx]['idioma_id'] == '2') selected="selected" @endif>Alemán</option>
                                                  <option value="3" @if ($perfil_idiomas[$mxx]['idioma_id'] == '3') selected="selected" @endif>Chino</option>
                                                  <option value="4" @if ($perfil_idiomas[$mxx]['idioma_id'] == '4') selected="selected" @endif>Francés</option>
                                                  <option value="5" @if ($perfil_idiomas[$mxx]['idioma_id'] == '5') selected="selected" @endif>Italiano</option>
                                                  <option value="6" @if ($perfil_idiomas[$mxx]['idioma_id'] == '6') selected="selected" @endif>Portugués</option>
                                                  <option value="7" @if ($perfil_idiomas[$mxx]['idioma_id'] == '7') selected="selected" @endif>Ruso</option>
                                              </select>
                                          </td>
                                          <td>
                                              <label class="label-input-format">Hablado</label>
                                              <select class="form-control input-format marginb-24" name="escrito[]">
                                                  <option value="1"@if ($perfil_idiomas[$mxx]['escrito'] == '1') selected="selected" @endif>Básico</option>
                                                  <option value="2"@if ($perfil_idiomas[$mxx]['escrito'] == '2') selected="selected" @endif>Intermedio</option>
                                                  <option value="3"@if ($perfil_idiomas[$mxx]['escrito'] == '3') selected="selected" @endif>Avanzado</option>
                                              </select>
                                          </td>
                                          <td>
                                              <label class="label-input-format">Escrito</label>
                                              <select class="form-control input-format marginb-24" name="lectura[]">
                                                  <option value="1"@if ($perfil_idiomas[$mxx]['lectura'] == '1') selected="selected" @endif>Básico</option>
                                                  <option value="2"@if ($perfil_idiomas[$mxx]['lectura'] == '2') selected="selected" @endif>Intermedio</option>
                                                  <option value="3"@if ($perfil_idiomas[$mxx]['lectura'] == '3') selected="selected" @endif>Avanzado</option>
                                              </select>
                                          </td>
                                          <td>
                                              <label class="label-input-format">Lectura</label>
                                              <select class="form-control input-format marginb-24" name="hablado[]">
                                                  <option value="1"@if ($perfil_idiomas[$mxx]['hablado'] == '1') selected="selected" @endif>Básico</option>
                                                  <option value="2"@if ($perfil_idiomas[$mxx]['hablado'] == '2') selected="selected" @endif>Intermedio</option>
                                                  <option value="3"@if ($perfil_idiomas[$mxx]['hablado'] == '3') selected="selected" @endif>Avanzado</option>
                                              </select>
                                          </td>
                                          <td class="text-center">
                                              <a href="#" data-fila="fila-<?= $mxx;?>" autocomplete="off"  style="margin-top: 2rem;" onclick="eliminarFila(<?= $mxx; ?>)" class="btn btn-sm btn-danger">
                                              <i class="fas fa-trash"></i>
                                              </a>
                                          </td>
                                        </tr>

                                    @endfor
                                @else
                                @for ($mx=0; $mx < 1; $mx++)

                                @endfor
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
              <!-- {{-- INFORMACION DE RESIDENCIA --}} -->
              <div class="card-header">
                <h3 class="card-title">Información de Residencia</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="form-group col-md-4 col-sm-6">
                    <label for="pais">
                      País de Residencia
                      <span class="asterisk_red">*</span>
                    </label>
                    <select class="form-control" id="pais" name="pais" >
                      <option value="">Elegir</option>
                      <option value="1"@if ($perfil[0]->idPais == '1') selected="selected" @endif>Bolivia</option>
                      <option value="2"@if ($perfil[0]->idPais == '2') selected="selected" @endif>Colombia</option>
                      <option value="3"@if ($perfil[0]->idPais == '3') selected="selected" @endif>Ecuador</option>
                      <option value="4"@if ($perfil[0]->idPais == '4') selected="selected" @endif>México</option>
                      <option value="5"@if ($perfil[0]->idPais == '5') selected="selected" @endif>Perú</option>
                      <option value="6"@if ($perfil[0]->idPais == '6') selected="selected" @endif>Argentina</option>
                      <option value="7"@if ($perfil[0]->idPais == '7') selected="selected" @endif>Belice</option>
                      <option value="8"@if ($perfil[0]->idPais == '8') selected="selected" @endif>Chile</option>
                      <option value="9"@if ($perfil[0]->idPais == '9') selected="selected" @endif>Costa Rica</option>
                      <option value="10"@if ($perfil[0]->idPais == '10') selected="selected" @endif>Cuba</option>
                      <option value="11"@if ($perfil[0]->idPais == '11') selected="selected" @endif>El Salvador</option>
                      <option value="12"@if ($perfil[0]->idPais == '12') selected="selected" @endif> Estados Unidos</option>
                      <option value="13"@if ($perfil[0]->idPais == '13') selected="selected" @endif>Guatamela</option>
                      <option value="14"@if ($perfil[0]->idPais == '14') selected="selected" @endif>Haití</option>
                      <option value="15"@if ($perfil[0]->idPais == '15') selected="selected" @endif>Honduras</option>
                      <option value="16"@if ($perfil[0]->idPais == '16') selected="selected" @endif>Nicaragua</option>
                      <option value="17"@if ($perfil[0]->idPais == '17') selected="selected" @endif>Panamá</option>
                      <option value="18"@if ($perfil[0]->idPais == '18') selected="selected" @endif>Paraguay</option>
                      <option value="19"@if ($perfil[0]->idPais == '19') selected="selected" @endif>República Dominicana</option>
                      <option value="20"@if ($perfil[0]->idPais == '20') selected="selected" @endif>Uruguay</option>
                      <option value="21"@if ($perfil[0]->idPais == '21') selected="selected" @endif>Venezuela</option>
                    </select>
                  </div>
                  <div class="form-group col-md-4 col-sm-6">
                    <label for="ciudad">
                      Ciudad
                      <span class="asterisk_red">*</span>
                    </label>
                    <input
                      autocomplete="off"
                      type="text"
                      class="form-control"
                      name="ciudad"
                      id="ciudad"
                      placeholder="Ingrese su ciudad"
                      value="{{$perfil[0]->ciudad}}"

                    />
                  </div>
                  <div class="form-group col-md-4 col-sm-6">
                    <label for="nacionalidad">
                      Nacionalidad
                      <span class="asterisk_red">*</span>
                    </label>
                    <input
                      autocomplete="off"
                      type="text"
                      class="form-control"
                      id="nacionalidad"
                      name="nacionalidad"
                      placeholder="Ingrese su nacionalidad"
                      value="{{$perfil[0]->nacionalidad}}"

                    />
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-8 col-sm-6">
                    <label for="dirección">
                      Dirección
                      <span class="asterisk_red">*</span>
                    </label>
                    <input
                      autocomplete="off"
                      type="text"
                      class="form-control"
                      id="dirección"
                      name="direccion"
                      placeholder="Ingrese su dirección"
                      value="{{$perfil[0]->direccion}}"
                    />
                  </div>
                  <div class="form-group col-md-4 col-sm-6">
                    <label for="codPostal">
                      Código Postal
                      <span class="asterisk_red">*</span>
                    </label>
                    <input
                      autocomplete="off"
                      type="text"
                      class="form-control"
                      id="codPostal"
                      name="codPostal"
                      placeholder="Ingrese su dirección"
                      value="{{$perfil[0]->codPostal}}"

                    />
                  </div>
                </div>
              </div>
              <!-- {{-- CONTACTO --}} -->
              <div class="card-header">
                <h3 class="card-title">Contacto</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="form-group col-md-2 col-sm-6">
                    <label for="prefijo">Prefijo - Celular</label>
                      <input type="text" value="{{$perfil[0]->prefijo}}" id="prefijoHide" name="prefijoHide" autocomplete="off" class="form-control" readonly required >
                  </div>
                  <div class="form-group col-md-5 col-sm-6">
                    <label for="celular">
                      Celular
                      <span class="asterisk_red">*</span>
                    </label>
                    <input
                        autocomplete="off"
                        class="form-control"
                        id="celular"
                        name="celular"
                        placeholder="Ingrese su  N° celular"

                        type="text"
                        value="{{$perfil[0]->celular}}"
                        required
                    />
                  </div>
                  <div class="form-group col-md-5 col-sm-6">
                    <label for="telefono">
                      Teléfono
                      <span class="asterisk_red">*</span>
                    </label>
                    <input
                      autocomplete="off"
                      type="text"
                      class="form-control"
                      id="telefono"
                      name="telefono"
                      placeholder="Ingrese su teléfono"
                      value="{{$perfil[0]->telefono}}"

                    />
                  </div>
                </div>
              </div>
              <div class="card-body" align="center">
                    <button class="btn btn-primary" >Actualizar</button>
              </div>
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{$error}}</li>
                  @endforeach
                </ul>
              </div>
              @endif
            </div>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-lg-12">
          <form action="" method="post">
            @csrf
            @method('put')
            <div class="row d-flex align-items-end flex-column">
              <div class="col-md-12 col-sm-12 col-lg-10">
                <div class="card card-secondary">
                  <!-- {{-- INFORMACION PERSONAL --}} -->
                  <div class="card-header">
                    <h3 class="card-title">HERRAMIENTAS PARA UNA COMUNICACIÓN PRODUCTIVA </h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse" id="id_collapse">
                        <i class="fas fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="form-group col-md-12 col-lg-12 col-sm-12">
                        <label for="telegram">Telegram<span class="asterisk_red">*</span></label>
                        <p>Es de suma importancia poder compartir contigo documentos relevantes y hacerte llegar notificaciones oportunas para tu viaje. Para ello empleamos la aplicación gratuita Telegram. Sigue, por favor los siguientes pasos:
                        </p>
                        <ul class="list-group">
                          <li class="list-group-item">1. Instala Telegram gratuitamente desde el PlayStore o AppStore</li>
                          <li class="list-group-item">2. Busca el Bot de ALAR y pega el siguien mensaje. Aquí te indicamos cómo hacerlo:
                            <br>
                            <div class="row">
                              <div class="col-md-4">
                                <div class="input-group mb-3 mt-4">
                                  <input type="text" id="user_email" class="form-control" aria-label="Recipient's username" aria-describedby="basic-addon2" value="mi correo es: {{$user[0]->email}}">
                                  <div class="input-group-append">
                                      <button type="button" class="btn btn-secondary" id="basic-addon2" data-toggle="tooltip" data-placement="top" title="Copiar correo">
                                          <i class="fa fa-copy"></i>
                                      </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </li>
                          <li class="list-group-item">3. Activa el Bot de ALAR en tu celular clickeando <a href="https://web.telegram.org/#/im?p=@alar45_bot" target="_blank">AQUÍ</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
</section>
<!-- MODAL -->
<div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Foto</h4>
      </div> -->
      <div class="modal-header">
        <h5 class="modal-title" style="color:black">Editar Foto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body d-flex justify-content-center">
        <div id="upload-demo" class="center-block" ></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="cropImageBtn" class="btn btn-primary">Delimitar</button>
      </div>
    </div>
  </div>
</div>
@push('perfil')
<script src="{{ URL::asset('js/perfil.js') }}"></script>
@endpush
@endsection
@if (session('mensaje_pefil'))
    <script type="text/javascript">
        setTimeout(() => {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 10000,
                timerProgressBar: false,
                customClass: {
                    container: 'adelante'
                },
                showClass: {
                    popup: "animate__animated animate__fadeInRight"
                },
                hideClass: {
                    popup: "animate__animated animate__fadeOutRight"
                },
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })
            Toast.fire({
                icon: '{{session('icon')}}',
                title: '{{ session('mensaje_pefil') }}'
            })
        }, 1000);
    </script>
@endif  