@extends('layouts.master')
@section('content')
<section class="content">

    <div class="card">
        <div class="card-header">
            <h2 class="m-0"><i class="fas fa-chart-bar"></i> Estadísticas</h2>
        </div>
        <div class="card-body">

          <estadisticas></estadisticas>

        </div>
      </div>

</section>
@endsection
