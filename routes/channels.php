<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

// LOGICA PARA RESTRINGIR EL ACCESO A UN CANAL PRIVADO: SI NO EXISTE USUARIO LOGUEADO RETORNA false
Broadcast::channel('notify', function ($user) {
    return $user != null ;
});


// CANAL PRIVADO, TRANSMITIR EVENTOS NOTIFICACIONES POR SUBIDA DE ARCHIVOS
Broadcast::channel('updateDoc.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
