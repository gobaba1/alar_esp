<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::apiResource('login', 'Api\LoginController');
Route::apiResource('codePhone', 'Api\CodePhoneController');
Route::apiResource('changeImage', 'Api\ChangeImageController');

Route::apiResource('user_doc', 'Api\UserDocsController');
Route::apiResource('update_state_doc', 'Api\UpdateStateDocController');
Route::apiResource('update_progress', 'Api\ProgressController');
// Telegram BOT
Route::apiResource('telegram', 'Api\TelegramController');
// API PROGRAMAS CONTROLLER
Route::apiResource('getprogramas_admin', 'Api\Programas_admin');
Route::apiResource('selectsProgramasAdmin', 'Api\SelectsAdminProgramas');


Route::apiResource('admin_universidades', 'Api\Admin_panel_universidades');
Route::apiResource('admin_ciudades', 'Api\Admin_panel_ciudades');
Route::apiResource('admin_tipoe', 'Api\Admin_panel_tipo_estudios');
Route::apiResource('admin_ramas', 'Api\Admin_panel_ramas');
Route::apiResource('admin_perfiles', 'Api\Admin_panel_perfiles');
Route::apiResource('admin_costos', 'Api\Admin_panel_costos');
// Route::apiResource('carga_programas_masivo', 'Api\Admin_panel_cargamasiva');

