<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',array(
    'view'=>'welcome',
    'uses'=>'ProgramaController@selectAll'
))->name('welcome');

Route::get('/curso_ruso', function () {
    return view('curso_ruso');
});
Route::post('/verificar','VerificarUsuarioController@Verificar');
Auth::routes();

Route::post('/subir','Controller@subirArchivo')->name('subir');


Route::get('/inicio', 'HomeController@index')->name('inicio');
//MI PREPARACION
Route::get('/mi_preparacion','PreparacionController@index')->middleware('auth');
Route::get('/videos/{filename}','PreparacionController@get_video')->middleware('auth');
//MI INSCRIPCION
Route::get('/mi_inscripcion','Validar_archivos@index')->middleware('auth');;
Route::get('/validar_formulario','Form_inscripcionController@validar_formulario')->middleware('auth');;
Route::resource('/perfil', 'PerfilController')->middleware('auth');
Route::post('/update_formulario_inscripcion','Form_inscripcionController@update');
//CONTROLADORES DE TELEGRAM
Route::post('/bot_telegram','botTelegramController@handle');

$router->group(['prefix' => '[833065748:AAH4kCQQQJoC7HIHk2pFxvbfFYexFWrZXfI]'], function() use ($router) {
    $router->get('set', 'botTelegramController@set');
    $router->post('hook', 'botTelegramController@hook');
});

//API PERFILES DE LA PAGINA PROGRAMAS
Route::get('/data_buscador',array(
    'view'=>'welcome',
    'uses'=>'ProgramaController@selectBuscador'
));
Route::get('/api/programaAll/{id_rama}','ProgramaController@selectPerfiles');
Route::get('monedaLocal/{iso4217}','ProgramaController@monedaLocal');
Route::post('/api/programas',array(
    'uses'=>'ProgramaController@filtroPrograma'
));
Route::get('/api/Presupuesto/{iduniversidad}','ProgramaController@Presupuesto');
Route::get('/api/Divisa/{iso4217}','ProgramaController@Divisa');
Route::get('/api/AllProgramas','ProgramaController@AllProgramas');
Route::post('/api/presupuesto','ProgramaController@listarPresupuesto');
Route::post('/api/inscripcionPrograma','ProgramaController@inscripcionPrograma');

//SUBIR ARCHIVOS A DRIVE
Route::post('/upload',function(Request $request){
    $request->file('thing')->storeAs("",$request->file_name,"google");
    return redirect('/mi_inscripcion');
});
//OBTENER LA IMAGEN
Route::get('/miniatura/{filename}',array(
    'as'=>'imageVideo',
    'uses'=>'botTelegramController@getImage'
));

// Route::get('/logo_universidades/{filename}', array(
//     'uses'=>'ControllerImage@getImage'
// ));



#############################################################################################
//RUTAS PARA IMPRIMIR PDF CON DOMPDF
Route::name('imprimir_form_inscripcion_dompdf')->get('/descargar_formulario_inscripcion', 'ImprimirPdfDomPdf@form_inscripcion')->middleware('auth');
//RUTAS PARA IMPRIMIR PDF CON DOMPDF

//RUTAS PARA IMPRIMIR PDF CON FPDF
Route::post('/formulario_inscripcion', 'ImprimirPdfFpdf@descargar_formulario_i')->middleware('auth');
Route::post('/descargar_formulario_i', 'ImprimirPdfFpdf@descargar_formulario_i')->middleware('auth');
//RUTAS PARA IMPRIMIR PDF CON FPDF

#############################################################################################
// RUTAS PARA LAS NOTIFICACIONES
Route::get('/getNotificaciones','NotificacionesController@getNotificacionesxID');
Route::post('/getRazon','NotificacionesController@getRazon');
// RUTAS PARA LAS NOTIFICACIONES
#############################################################################################
// RUTAS PARA ACTUALIZAR ESTADO DE DOCUMENTOS
Route::post('/updated_user_doc','UpdateStateDocController@updateState')->name('user_doc');
// RUTAS PARA ACTUALIZAR ESTADO DE DOCUMENTOS

#############################################################################################

// RUTAS PARA CURSOS
Route::post('/getCursosxID','CursosController@getCursosxID');
Route::post('/updateCursosActivosxID','CursosController@updateCursosActivosxID');
Route::post('/removeCursosxID','CursosController@removeCursosxID');
Route::post('/add_clasesXmodulos','CursosController@add_clasesXmodulos');
Route::get('/getModulos','CursosController@getModulos');
// RUTAS PARA CURSOS

#############################################################################################

// RUTAS PARA LOGIN CON FACEBOOK
Route::get('/login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('/login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
// RUTAS PARA LOGIN CON FACEBOOK

#############################################################################################

// RUTAS PARA LOGIN CON GOOGLE
Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');
// RUTAS PARA LOGIN CON GOOGLE

#############################################################

// RUTAS PARA VER ESTADISTICAS
Route::get('/estadisticas', 'EstadisticasController@index');
Route::get('/estadisticas_tab1', 'EstadisticasController@tab1');
Route::get('/estadisticas_tab2', 'EstadisticasController@tab2');
Route::post('/estadisticas_tab3', 'EstadisticasController@tab3');
Route::post('/estadisticas_tab4', 'EstadisticasController@tab4');
Route::post('/estadisticas_tab5', 'EstadisticasController@tab5');
Route::post('/estadisticas_tab6', 'EstadisticasController@tab6');
// RUTAS PARA VER ESTADISTICAS
#############################################################


// CLEAR CACHE
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear ');
    return "Cache is cleared";
});

#############################################################

//DESCARGAR PDF Y EXCEL
// Route::name('imprimir_form_presupuesto_dompdf')->post('/descargar_formulario_presupuesto', 'ImprimirPdfDomPdf@form_presupuesto');
Route::get('/descargar_formulario_presupuesto/{id_universidad}/{id_programa}/{moneda}/{pais_procedencia}/{region_procedencia}/{prueba?}','ImprimirPdfDomPdf@form_presupuesto');
Route::get('/descargar_excel_presupuesto/{id_universidad}/{id_programa}/{moneda}/{pais_procedencia}/{region_procedencia}','ImprimirPdfDomPdf@imprimir_excel_presupuesto');
Route::get('/descargar_plantilla','ImprimirPlantillaController@imprimir_excel_programas');
Route::get('/descargar_plantilla_costo','ImprimirPlantillaController@imprimir_excel_costos');
#############################################################

// CAMBIAR PASSWORD
// Route::post('/olvido_password', 'EmailController@olvido_password')->name('olvido_password');
// Route::get('/recuperar_contrasena/{id}', 'EmailController@olvido_password')->name('olvido_password');


Route::post('recuperar_password', 'RecuperarPassword@validarRequest')->name('recuperar_password');
Route::post('recuperar_password_token', 'RecuperarPassword@resetearPassword')->name('recuperar_password_token');
// CAMBIAR PASSWORD

#############################################################

// ENVIAR DOCUMENTOS
Route::post('/enviar_doc_to_user', 'DocumentoToUserController@enviar_documento_to_user');

// ENVIAR DOCUMENTOS
Route::post('/carga_masiva', 'Carga_masiva@cargar_data');
Route::post('/carga_masiva_costos', 'Carga_masiva@cargar_data_costos');
Route::post('/delete_masivo', 'Carga_masiva@eliminar_datos')->middleware('auth');
#############################################################

#############################################################

// RUTAS PARA PROGRAMAS ADMINISTRADOR
// Route::post('/getprogramas_admin', 'DocumentoToUserController@enviar_documento_to_user');

// RUTAS PARA PROGRAMAS ADMINISTRADOR

#############################################################

#############################################################
// RUTAS PARA CRONS
Route::get('/update_rates', 'UpdateRates@updateRates');

// RUTAS PARA CRONS
#############################################################

