<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValidacionesArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validaciones_archivos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('tyc_status'); // STATUS DEL ARCHIVO TYC
            $table->tinyInteger('passport_status'); // STATUS DEL ARCHIVO PASAPORTE
            $table->tinyInteger('form_status'); // STATUS DEL ARCHIVO FORMULARIO DE INSCRIPCION
            $table->bigInteger('user_id')->unsigned(); // ID DEL USUARIO DEL PERFIL
            $table->foreign('user_id')->references('id')->on('users'); // FK: user_id
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validaciones_archivos');
    }
}
