<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notificaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user1')->unsigned(); // ID DEL USUARIO QUE ENVIA LA NOTIFICACION
            $table->bigInteger('user2')->unsigned(); // ID DEL USUARIO QUE RECIBE LA NOTIFICACION
            $table->string('razon_constancia')->nullable();;
            $table->string('sustento')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notificaciones');
    }
}
