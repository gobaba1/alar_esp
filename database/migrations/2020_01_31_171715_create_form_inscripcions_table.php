<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormInscripcionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_inscripcions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('facultad_necesita');
            $table->string('facultad_mes');
            $table->string('facultad_anio');
            $table->string('facultad_universidad');
            $table->string('vacante_estudio');
            $table->string('vacante_rama');
            $table->string('vacante_universidad');
            $table->string('vancante_gradoActual');
            $table->string('vacante_financia');
            $table->string('madre_nombre');
            $table->string('madre_correo');
            $table->string('padre_nombre');
            $table->string('padre_correo');
            $table->string('padres_direccion');
            $table->string('padres_telefono');
            $table->string('padres_celular');
            $table->string('respon_nombre');
            $table->string('respon_correo');
            $table->string('respon_telefono');
            $table->string('respon_celular');
            $table->string('apoder_nombre');
            $table->string('apoder_dni');
            $table->bigInteger('user_id')->unsigned(); // ID DEL USUARIO DEL PERFIL
            $table->foreign('user_id')->references('id')->on('users'); // FK: user_id
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_inscripcions');
    }
}
