<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfiles', function (Blueprint $table) {
            $table->bigIncrements('id'); // ID
            $table->string('sexo'); // SEXO
            $table->date('fecNacimiento'); // FECHA DE NACIMIENTO
            $table->bigInteger('idPais'); // ID DEL PAIS DE ORIGEN
            $table->string('ciudad'); // CIUDAD
            $table->string('nacionalidad'); // NACIONALIDAD
            $table->string('celular'); // CELULAR
            $table->string('direccion'); // DIRECCION
            $table->string('codPostal'); // CODIGO POSTAL
            $table->bigInteger('telefono'); // TELEFONO
            $table->string('facebook'); // FACEBOOK
            $table->bigInteger('numPasaporte'); // NUMERO DE PASAPORTE
            $table->string('estado_civil'); // ESTADO CIVIL
            $table->bigInteger('user_id')->unsigned(); // ID DEL USUARIO DEL PERFIL
            $table->foreign('user_id')->references('id')->on('users'); // FK: user_id
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfiles');
    }
}
