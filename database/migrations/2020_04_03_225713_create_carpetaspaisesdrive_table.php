<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarpetaspaisesdriveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carpetaspaisesdrive', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('id_carpeta');
            $table->bigInteger('id_pais')->unsigned();
            $table->bigInteger('id_carpeta_tiempo')->unsigned();
            $table->foreign('id_pais')->references('id')->on('pais'); // FK: id_pais
            $table->foreign('id_carpeta_tiempo')->references('id')->on('carpetastiempodrive'); // FK: id_carpeta_tiempo
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carpetaspaisesdrive');
    }
}
