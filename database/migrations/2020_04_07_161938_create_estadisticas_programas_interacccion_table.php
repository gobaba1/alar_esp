<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstadisticasProgramasInteracccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estadisticas_programas_interacccion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipo')->nullable(); 
            $table->bigInteger('id_programa')->nullable(); 
            $table->string('pais_procedencia')->nullable(); 
            $table->string('region_procedencia')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estadisticas_programas_interacccion');
    }
}
