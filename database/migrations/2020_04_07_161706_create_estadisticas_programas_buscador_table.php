<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstadisticasProgramasBuscadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estadisticas_programas_buscador', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tipo_estudio')->nullable(); 
            $table->bigInteger('rama_estudio')->nullable(); 
            $table->bigInteger('perfil_estudio')->nullable(); 
            $table->bigInteger('idioma')->nullable(); 
            $table->bigInteger('ciudad')->nullable(); 
            $table->bigInteger('moneda')->nullable(); 
            $table->string('pais_procedencia')->nullable(); 
            $table->string('region_procedencia')->nullable(); 
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estadisticas_programas_buscador');
    }
}
