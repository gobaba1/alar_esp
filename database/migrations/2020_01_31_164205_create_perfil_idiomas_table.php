<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerfilIdiomasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfil_idiomas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('hablado', 1); // NIVEL DE HABLA DEL IDIOMA
            $table->char('escrito', 1); // NIVEL DE ESCRITURA DEL IDIOMA
            $table->char('lectura', 1); // NIVEL DE LECTURA DEL IDIOMA

            $table->bigInteger('user_id')->unsigned(); // ID DEL USUARIO DEL PERFIL
            $table->foreign('user_id')->references('id')->on('users'); // FK: user_id

            $table->bigInteger('idioma_id')->unsigned(); // ID DEL USUARIO DEL PERFIL
            $table->foreign('idioma_id')->references('id')->on('idiomas'); // FK: user_id
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfil_idiomas');
    }
}
