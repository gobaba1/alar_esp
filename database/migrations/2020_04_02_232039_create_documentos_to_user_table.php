<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentosToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos_to_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_admin');
            $table->bigInteger('id_alumno');
            $table->string('url_drive');
            $table->string('type');
            $table->string('state');
            $table->string('mensaje')->nullabe();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentos_to_user');
    }
}
