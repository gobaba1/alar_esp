<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoEstudio extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'tipo_estudio';

    protected $fillable = [
        'descripcion'
    ];
}
