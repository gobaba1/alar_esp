<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
class ImportCostos  implements ToCollection
{
    private $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function collection(Collection $rows)
    {
        // $rows [$i][0] ->Universidad
        // $rows [$i][1] ->Vivienda
        // $rows [$i][2] ->alimentacion
        // $rows [$i][3] ->seguro
        // $rows [$i][4] ->divisa
        for ($i=1; $i < count($rows); $i++) { 
            if(!empty($rows [$i][1]) && !empty($rows [$i][2]) && !empty($rows [$i][3]) && !empty($rows [$i][4]) ){
                $universidad = explode("(", $rows [$i][0]);
                $id_univ=DB::connection('mysql2')->table('universidad')->select('universidad.id')
                                        ->where('universidad.nombre',$universidad)->first();
                // SI LA MONEDA ESTA EN RUBLOS SE CONVIERTE A DOLARES;
                $costo_vivienda         = $rows [$i][1];
                $costo_alimentacion     = $rows [$i][2];
                $costo_seguro           = $rows [$i][3];
                if(strtoupper($rows[$i][4])=='RUB'){
                    $conversion=DB::connection('mysql2')->table('divisa')->select('divisa.ratio')
                                        ->where('divisa.iso4217','RUB')->first();
                    $costo_vivienda         =intval (intval($rows [$i][1]) / $conversion->ratio);
                    $costo_alimentacion     =intval (intval($rows [$i][2]) / $conversion->ratio);
                    $costo_seguro           =intval (intval($rows [$i][3]) / $conversion->ratio);
                }
                DB::connection('mysql2')->table('costos_presupuesto')->updateOrInsert(
                    ['id_universidad' => $id_univ->id],
                    ['costo_vivienda' => $costo_vivienda,
                    'costo_alimentacion' => $costo_alimentacion,
                    'costo_seguro' => $costo_seguro,
                    'costo_divisa' =>$rows[$i][4],
                    ],
                );
            }
        }
    }
}
