<?php

namespace App\Exports;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
class Plantilla_descarga_costo implements FromView
{
    public  $universidad;
    public function view(): View
    {
        return view('exports.descargaPlantilla_costo', [
            'universidad'=>$this->universidad,
        ]);
    }
    // /**
    // * @return \Illuminate\Support\Collection
    // */
    // public function collection()
    // {
    // }
}
