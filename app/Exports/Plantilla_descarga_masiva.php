<?php

namespace App\Exports;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;

class Plantilla_descarga_masiva implements FromView
{
    public  $rama,$tipo_estudio,$perfil,$mayor_array;
    public function view(): View
    {
        $formula='SI(SI.ERROR(BUSCARV(A2;L$2:L$9;1;FALSO);"Error")="Error";"Error en Tipo de Estudio/ ";"Bien/ ")&SI(SI.ERROR(BUSCARV(B2;N$2:N$10;1;FALSO);"Error")="Error";" Error en rama/ ";"Bien ")&SI(SI.ERROR(BUSCARV(C2;P$2:P$'.count($this->perfil).';1;FALSO);"Error")="Error";"Error en Perfil ";"Correcto")';
        return view('exports.descargaPlantilla', [
            'tipo_estudios'=>$this->tipo_estudio,
            'ramas' => $this->rama,
            'perfiles'=>$this->perfil,
            'mayor_array'=>$this->mayor_array,
            'formula'       =>$formula,
        ]);
    }
    // /**
    // * @return \Illuminate\Support\Collection
    // */
    // public function collection()
    // {
    // }
}
