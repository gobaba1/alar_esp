<?php

namespace App\Exports;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;

class Programas implements FromView
{
    public  $info_general,$presupuesto_local,$presupuesto_rublos,$presupuesto_dolares;
    public function view(): View
    {
        return view('exports.programas', [
            'fecha_proforma'=>$this->fecha_proforma,
            'costos_calendario'=>$this->costos_calendario,
            'mostrar'=>$this->mostrar,
            'info_general' => $this->info_general,
            'divisa'        =>$this->divisa,
            // 'presupuesto_rublos'=>$this->presupuesto_rublos,
            // 'presupuesto_dolares'=>$this->presupuesto_dolares,
            'presupuesto_local'=>$this->presupuesto_local
        ]);
    }
    // /**
    // * @return \Illuminate\Support\Collection
    // */
    // public function collection()
    // {
    // }
}
