<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UpdateDocNotifications implements  ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $user;
    public $message;
    public $doc;
    public $alumno;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $message, $doc, $alumno)
    {
        $this->user = $user;
        $this->message = $message;
        $this->doc = $doc;
        $this->alumno = $alumno;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
    //    \Log::debug("$this->user : {$this->message} , {$this->doc}");
        return new PrivateChannel("updateDoc.{$this->user}");
    }
}
