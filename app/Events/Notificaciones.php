<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class Notificaciones implements  ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $message;
    public $name;
    public $lastname;
    public $img;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message, $name, $lastname, $img)
    {
        $this->message = $message;
        $this->name = $name;
        $this->lastname = $lastname;
        $this->img = $img;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
//        \Log::debug($this->message);
        return new Channel('notify');
    }
}
