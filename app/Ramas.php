<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ramas extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'rama';

    protected $fillable = [
        'descripcion'
    ];
}
