<?php

namespace App\Http\Controllers;

use App\User;
use App\Perfiles;
use App\Form_inscripcions;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ImprimirPdfFpdf extends Controller
{
    public function descargar_formulario_i(Request $request)
    {
        // $file = $request->firma_digital;
        // dd( $file);
        $data_form = Form_inscripcions::where('user_id', auth()->id())->get();
        $data_perfil = Perfiles::where('user_id', auth()->id())->get();
        $data_user = User::where('id', auth()->id())->get();

        $pdf = new Fpdf();
        $pdf->AddPage();

        /* LOGO ALAR*/
        $pdf->Image("images/logo_alar_naranja.png" , 12 ,6, 50 , 20 , "PNG");

        /* FIRMA DIGITAL*/
        if (isset($request->firma_digital)) {
            $file = $request->file('firma_digital');
            //obtenemos el nombre del archivo
            \Storage::disk('public_raiz')->put(auth()->id().'_firmaDigital.jpg',  \File::get($file));
            $pdf->Image("uploads/".auth()->id()."_firmaDigital.jpg" , 130 ,266, 50 , 20 );
            Storage::disk('public_raiz')->delete(auth()->id()."_firmaDigital.jpg");

        }
        if (isset($request->firma_digital)) {
        }
        $pdf->Ln();

        /* FOTO USUARIO */
        // $pdf->Image("../../../zonaUsuario/".$user[0]['picture'] , 167 ,4, 35 , 38 );

        $pdf->Ln(22);

        /* TITULO FORMULARIO */
        $pdf->SetLineWidth(.2);
        $pdf->Cell(30);
        $pdf->SetFont('Arial','B',22);
        $pdf->Cell(40,7,utf8_decode('Formulario de Inscripción'),0,1,'L');

        $pdf->Ln(5);
    /* --------------------------------------------------- PRIMERA CAJA DE DATOS --------------------------------------------------- */
    /* RECTANGULO DE LA PRIMERA CAJA */
        $pdf->SetFillColor(199);
        $pdf->SetDrawColor(3,3,3);
        $pdf->SetLineWidth(.6);
        $pdf->Rect(14, 44, 188, 46, '.8', '');

        /* GROSOR DE LOS BORDES INTERNO DE LA TABLA*/
        $pdf->SetLineWidth(.2);
        /* NOMBRE Y APELLIDOS */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(40,6,'Apellidos y Nombres',1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(148,6,utf8_decode($data_user[0]->name.' '.$data_user[0]->lastname),1,0,'L');
        // $pdf->Cell(148,6,utf8_decode('JEAN CARLO PALOMINO GONZALES'),1,0,'L');
        $pdf->Ln();
        /*-----------------------------------------------------------------------------*/
        /* SEXO */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(12,6,'Sexo',1,0,'L');

        $pdf->SetFont('Arial','',9);
        $sexo = ($data_perfil[0]->sexo=='m') ? 'MASCULINO' : 'FEMENINO' ;
        $pdf->Cell(28,6,utf8_decode($sexo),1,0,'C');
        /* PAIS DE PROCEDENCIA */
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(34,6,utf8_decode('País de Procedencia'),1,0,'C');

        $pdf->SetFont('Arial','',9);
        // $pdf->Cell(63,6,utf8_decode($user[0]['PAIS']),1,0,'L');
        $pdf->Cell(63,6,utf8_decode('PERÚ'),1,0,'L');
        /* CIUDAD */
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(17,6,'Ciudad',1,0,'L');

        $pdf->SetFont('Arial','',9);
        // $pdf->Cell(34,6,utf8_decode($user[0]['ciudad']),1,1,'L');
        $pdf->Cell(34,6,utf8_decode($data_perfil[0]->ciudad),1,1,'L');

        /*-------------------------------------------------------------------------------*/
        /* FECHA DE NACIMIENTO */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->MultiCell(40,4,utf8_decode('Fecha de nacimiento (Día / Mes / Año)'),1,'L');

        $pdf->setXY(54,56);

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(20,8,utf8_decode($data_perfil[0]->fecNacimiento),1,0,'C');


        /* EMAIL */
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(20,8,'EMAIL',1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(53,8,utf8_decode($data_user[0]->email),1,0,'L');

        /* FACEBOOK */
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(18,8,'facebook',1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(37,8,utf8_decode($data_user[0]->facebook),1,1,'L');

        /*-------------------------------------------------------------------------------*/
        /* NACIONALIDAD */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(40,6,'Nacionalidad',1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(46,6,utf8_decode($data_perfil[0]->nacionalidad),1);

        /* N° DE PASAPORTE (SI YA LO TIENE) */
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(50,6,utf8_decode('N° de pasaporte (si ya lo tiene)'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(52,6,utf8_decode($data_perfil[0]->numPasaporte),1,1,'L');

        /*-------------------------------------------------------------------------------*/
        /* N° DE PASAPORTE (SI YA LO TIENE) */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(40,6,utf8_decode('Estado civil'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        switch ($data_perfil[0]->estado_civil) {
            case 'c': $estadoCivil = 'CASADO'; break;
            case 's': $estadoCivil = 'SOLTERO'; break;
            case 'd': $estadoCivil = 'DIVORCIADO'; break;
            case 'v': $estadoCivil = 'VIUDO'; break;
            default: $estadoCivil = ''; break;
        }
        $pdf->Cell(148,6,utf8_decode($estadoCivil),1,1,'C');
        // $pdf->Cell(148,6,utf8_decode('estadoCivil'),1,1,'C');

        /*-------------------------------------------------------------------------------*/
        /* DIRECCION Y CODIGO POSTAL */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(40,6,utf8_decode('Dirección y CódigoPostal'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(148,6,utf8_decode(utf8_decode($data_perfil[0]->direccion).' - '.utf8_decode($data_perfil[0]->codPostal)),1,1,'C');

        /*-------------------------------------------------------------------------------*/

        $prefijo = explode(' ', $data_perfil[0]->celular);
        /* PREFIJO TELEFONICO DE CIUDAD, N° TEL. DE CASA */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->MultiCell(40,4,utf8_decode('Prefijo telefónico de ciudad, n° tel. de casa'),1,'L');

        $pdf->setXY(54,82);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(90,8,utf8_decode($prefijo[0]).' '.utf8_decode($data_perfil[0]->telefono),1,0,'C');

        /* CELULAR */
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(18,8,utf8_decode('Celular'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(40,8,utf8_decode($data_perfil[0]->celular),1,0,'L');

    /*------------------------------------------------------------------------------------------------------------*/
        $pdf->Ln(12);
    /* --------------------------------------------------- SEGUNDA CAJA DE DATOS --------------------------------------------------- */
    /* RECTANGULO DE LA SEGUNDA CAJA */
        $pdf->SetFillColor(199);
        $pdf->SetDrawColor(3,3,3);
        $pdf->SetLineWidth(.6);
        $pdf->Rect(14, 94, 188, 24, '.6', '');

        /* GROSOR DE LOS BORDES INTERNO DE LA TABLA*/
        $pdf->SetLineWidth(.2);

        /* CONOCIMIENTO DE IDIOMAS */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(188,6,utf8_decode('Conocimiento de idiomas (Niveles: básico, intermedio, avanzado)'),1,1,'L');
        $idiomas = DB::table('perfil_idiomas')
                        ->select('perfil_idiomas.*','idiomas.idioma')
                        ->join('idiomas', 'perfil_idiomas.idioma_id', '=', 'idiomas.id')
                        ->where('user_id', auth()->id())->limit(3)->get();
        // dd($idiomas);
        function nivel_idioma($nivel){
            switch ($nivel) { case '1': return 'BASICO'; break;	case '2': return 'INTERMEDIO'; break;	case '3': return 'AVANZADO'; break;	default: return ''; break;}
        }
        $idiomaA = (isset($idiomas[0]->idioma)) ? $idiomas[0]->idioma : '-';
        $idiomaAH = (isset($idiomas[0]->hablado)) ? nivel_idioma($idiomas[0]->hablado) : '-';
        $idiomaAE = (isset($idiomas[0]->escrito)) ? nivel_idioma($idiomas[0]->escrito) : '-';
        $idiomaAL = (isset($idiomas[0]->lectura)) ? nivel_idioma($idiomas[0]->lectura) : '-';
        $idiomaB = (isset($idiomas[1]->idioma)) ? $idiomas[1]->idioma : '-';
        $idiomaBH = (isset($idiomas[1]->hablado)) ? nivel_idioma($idiomas[1]->hablado) : '-';
        $idiomaBE = (isset($idiomas[1]->escrito)) ? nivel_idioma($idiomas[1]->escrito) : '-';
        $idiomaBL = (isset($idiomas[1]->lectura)) ? nivel_idioma($idiomas[1]->lectura) : '-';
        $idiomaC = (isset($idiomas[2]->idioma)) ? $idiomas[2]->idioma : '-';
        $idiomaCH = (isset($idiomas[2]->hablado)) ? nivel_idioma($idiomas[2]->hablado) : '-';
        $idiomaCE = (isset($idiomas[2]->escrito)) ? nivel_idioma($idiomas[2]->escrito) : '-';
        $idiomaCL = (isset($idiomas[2]->lectura)) ? nivel_idioma($idiomas[2]->lectura) : '-';

        /* IDIOMA 1 */
            /* A)*/
                $pdf->Cell(4);
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(8,6,utf8_decode('a)'),1,0,'C');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(39,6,strtoupper(utf8_decode($idiomaA)),1,0,'L');

                /* HABLADO */
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(15,6,utf8_decode('Hablado'),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(32,6,strtoupper(utf8_decode($idiomaAH)),1,0,'L');

                /* ESCRITO */
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(15,6,utf8_decode('Escrito'),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(32,6,strtoupper(utf8_decode($idiomaAE)),1,0,'L');

                /* LECTURA */
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(15,6,utf8_decode('Lectura'),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(32,6,utf8_decode($idiomaAL),1,1,'L');

            /* B)*/
                $pdf->Cell(4);
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(8,6,utf8_decode('b)'),1,0,'C');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(39,6,strtoupper(utf8_decode($idiomaB)),1,0,'L');

                /* HABLADO */
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(15,6,utf8_decode('Hablado'),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(32,6,strtoupper(utf8_decode($idiomaBH)),1,0,'L');

                /* ESCRITO */
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(15,6,utf8_decode('Escrito'),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(32,6,strtoupper(utf8_decode($idiomaBE)),1,0,'L');

                /* LECTURA */
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(15,6,utf8_decode('Lectura'),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(32,6,strtoupper(utf8_decode($idiomaBL)),1,1,'L');

            /* C)*/
                $pdf->Cell(4);
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(8,6,utf8_decode('c)'),1,0,'C');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(39,6,strtoupper(utf8_decode($idiomaC)),1,0,'L');

                /* HABLADO */
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(15,6,utf8_decode('Hablado'),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(32,6,strtoupper(utf8_decode($idiomaCH)),1,0,'L');

                /* ESCRITO */
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(15,6,utf8_decode('Escrito'),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(32,6,strtoupper(utf8_decode($idiomaCE)),1,0,'L');

                /* LECTURA */
                $pdf->SetFont('Arial','B',9);
                $pdf->Cell(15,6,utf8_decode('Lectura'),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(32,6,strtoupper(utf8_decode($idiomaCL)),1,1,'L');


    // /*------------------------------------------------------------------------------------------------------------*/
        $pdf->Ln(4);
    // /* --------------------------------------------------- TERCERA CAJA DE DATOS --------------------------------------------------- */
    // /* RECTANGULO DE LA TERCERA CAJA */
        $pdf->SetFillColor(199);
        $pdf->SetDrawColor(3,3,3);
        $pdf->SetLineWidth(.6);
        $pdf->Rect(14, 122, 188, 30, '.6', '');

        /* GROSOR DE LOS BORDES INTERNO DE LA TABLA*/
        $pdf->SetLineWidth(.2);

        /* SOLICITO INSCRIPCION Y VACANTE A LA FACULTAD DE IDIOMA */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(188,6,utf8_decode('Solicito Inscripción y vacante a la Facultad de idioma (Escoja FI1, FI2, FI3 u otra opción)'),1,1,'L');
        switch ($data_form[0]->facultad_necesita) {
            case '1':
                /* FI1 */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI1    En Enero del                               20__'),1,0,'L');

                /* RECTANGULO GRIS */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(26,6,'',1,0,'L',true);

                /* FI2 */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI2    En Marzo del                               20__'),1,1,'L');

                /* FI3 */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI3    En Setiembre del                         20__'),1,0,'L');

                /* NO NECESITO FACULTAD DE IDIOMA, ESTUADRE EN INGLES */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(107,6,utf8_decode('        No necesito Facultad de Idioma, estudiaré en Inglés'),1,1,'L');

                /* SOLICITO ME EXONEREN DE LA FACULTAD */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','U',9);
                $pdf->SetTextColor(255,0,0);
                $pdf->Cell(188,6,utf8_decode('Solicito que me exoneren de la Facultad de Idioma pues domino el Ruso'),1,1,'C');

                $pdf->SetTextColor(0,0,0);
                /* EN LA UNIVERSIDAD */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(33,6,utf8_decode('En la universidad: '),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(155,6,utf8_decode(''),1,1,'L');
            break;
            case '0':
                /* FI1 */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI1    En Enero del                               20__'),1,0,'L');

                /* RECTANGULO GRIS */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(26,6,'',1,0,'L',true);

                /* FI2 */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI2    En Marzo del                               20__'),1,1,'L');

                /* FI3 */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI3    En Setiembre del                         20__'),1,0,'L');

                /* NO NECESITO FACULTAD DE IDIOMA, ESTUADRE EN INGLES */
                $pdf->SetFont('Arial','U',9);
                $pdf->SetTextColor(255,0,0);
                $pdf->Cell(107,6,utf8_decode('No necesito Facultad de Idioma, estudiaré en Inglés'),1,1,'C');

                $pdf->SetTextColor(0,0,0);
                /* SOLICITO ME EXONEREN DE LA FACULTAD */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(188,6,utf8_decode('Solicito que me exoneren de la Facultad de Idioma pues domino el Ruso'),1,1,'C');

                /* EN LA UNIVERSIDAD */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(33,6,utf8_decode('En la universidad: '),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(155,6,utf8_decode(''),1,1,'L');
            break;
            case '2':
                switch ($data_form[0]->facultad_mes) {
                    case 'Enero':$anio1 = $data_form[0]->facultad_anio; $anio2 = ''; $anio3 = '';
                /* FI1 */
                $pdf->Cell(4);
                $pdf->SetTextColor(255,0,0);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI1    En Enero del                               '.$anio1),1,0,'L');

                /* RECTANGULO GRIS */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(26,6,'',1,0,'L',true);

                $pdf->SetTextColor(0,0,0);
                /* FI2 */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI2    En Marzo del                               '.$anio2),1,1,'L');

                /* FI3 */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI3    En Setiembre del                         '.$anio3),1,0,'L');
                    break;

                    case 'Marzo':$anio1 = ''; $anio2 = $data_form[0]->facultad_anio; $anio3 = '';
                    /* FI1 */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI1    En Enero del                               '.$anio1),1,0,'L');

                /* RECTANGULO GRIS */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(26,6,'',1,0,'L',true);

                /* FI2 */
                $pdf->SetFont('Arial','',9);
                $pdf->SetTextColor(255,0,0);
                $pdf->Cell(81,6,utf8_decode('      FI2    En Marzo del                               '.$anio2),1,1,'L');

                /* FI3 */
                $pdf->Cell(4);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI3    En Setiembre del                         '.$anio3),1,0,'L');




                    break;
                    case 'Septiembre':$anio1 = ''; $anio2 = ''; $anio3 = $data_form[0]->facultad_anio;
                        /* FI1 */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI1    En Enero del                               '.$anio1),1,0,'L');

                /* RECTANGULO GRIS */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(26,6,'',1,0,'L',true);

                /* FI2 */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI2    En Marzo del                               '.$anio2),1,1,'L');

                /* FI3 */
                $pdf->Cell(4);
                $pdf->SetTextColor(255,0,0);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(81,6,utf8_decode('      FI3    En Septiembre del                         '.$anio3),1,0,'L');
                $pdf->SetTextColor(0,0,0);

                    break;
                    default: $anio1 = ''; $anio2 = ''; $anio3 = ''; break;
                }


                /* NO NECESITO FACULTAD DE IDIOMA, ESTUADRE EN INGLES */
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(107,6,utf8_decode('No necesito Facultad de Idioma, estudiaré en Inglés'),1,1,'C');

                /* SOLICITO ME EXONEREN DE LA FACULTAD */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(188,6,utf8_decode('Solicito que me exoneren de la Facultad de Idioma pues domino el Ruso'),1,1,'C');

                /* EN LA UNIVERSIDAD */
                $pdf->Cell(4);
                $pdf->SetFont('Arial','',9);
                $pdf->Cell(33,6,utf8_decode('En la universidad: '),1,0,'L');

                $pdf->SetFont('Arial','',9);
                $pdf->Cell(155,6,utf8_decode($data_form[0]->facultad_universidad),1,1,'L');
            break;
        }
    //     // /* FI1 */
    //     // $pdf->Cell(4);
    //     // $pdf->SetFont('Arial','',9);
    //     // $pdf->Cell(81,6,utf8_decode('      FI1    En Enero del                               20__'),1,0,'L');

    //     // /* RECTANGULO GRIS */
    //     // $pdf->SetFont('Arial','',9);
    //     // $pdf->Cell(26,6,'',1,0,'L',true);

    //     // /* FI2 */
    //     // $pdf->SetFont('Arial','',9);
    //     // $pdf->Cell(81,6,utf8_decode('      FI2    En Marzo del                               20__'),1,1,'L');

    //     // /* FI3 */
    //     // $pdf->Cell(4);
    //     // $pdf->SetFont('Arial','',9);
    //     // $pdf->Cell(81,6,utf8_decode('      FI3    En Setiembre del                         20__'),1,0,'L');

    //     // /* NO NECESITO FACULTAD DE IDIOMA, ESTUADRE EN INGLES */
    //     // $pdf->SetFont('Arial','',9);
    //     // $pdf->Cell(107,6,utf8_decode('        No necesito Facultad de Idioma, estudiaré en Inglés'),1,1,'L');

    //     // /* SOLICITO ME EXONEREN DE LA FACULTAD */
    //     // $pdf->Cell(4);
    //     // $pdf->SetFont('Arial','',9);
    //     // $pdf->Cell(188,6,utf8_decode('Solicito que me exoneren de la Facultad de Idioma pues domino el Ruso'),1,1,'C');

    //     // /* EN LA UNIVERSIDAD */
    //     // $pdf->Cell(4);
    //     // $pdf->SetFont('Arial','',9);
    //     // $pdf->Cell(33,6,utf8_decode('En la universidad: '),1,0,'L');

    //     // $pdf->SetFont('Arial','',9);
    //     // $pdf->Cell(155,6,utf8_decode('En la universidad: '),1,1,'L');

    // /* -------------------------------------------------------------------------------------------------- */
        $pdf->Ln(4);
    // /* --------------------------------------------------- CUARTA CAJA DE DATOS --------------------------------------------------- */
    // /* RECTANGULO DE LA CUARTA CAJA */
        $pdf->SetFillColor(199);
        $pdf->SetDrawColor(3,3,3);
        $pdf->SetLineWidth(.6);
        $pdf->Rect(14, 156, 188, 24, '.6', '');

        /* GROSOR DE LOS BORDES INTERNO DE LA TABLA*/
        $pdf->SetLineWidth(.2);

        /* SOLICITO INSCRIPCION Y VACANTE DE ESTUDIOS */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(188,6,utf8_decode('Solicito Inscripción y vacante de estudios para estudios de :'),1,1,'L');
        switch ($data_form[0]->vacante_estudio) {
            case 'Pregrado':$grado_estudos = 'PREGRADO(LICENCIATURA)'; break;
            case 'Maestría':$grado_estudos = 'MAESTRÍA'; break;
            case 'Doctorado: (Ph.D)':$grado_estudos = 'DOCTORADO(Ph.D)'; break;
            case '(D.Sc)':$grado_estudos = '(D.Sc)'; break;
            case 'Especialidad Médica':$grado_estudos = 'ESPECIALIDAD MÉDICA'; break;
            default: $grado_estudos = ''  ; break;
        }
        /* OPCIONES */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(188,6,utf8_decode($grado_estudos),1,1,'C');

        /* CARRERA O TEMA DE ESTUDIOS */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(45,6,utf8_decode('Carrera o tema de estudios'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(143,6,utf8_decode($data_form[0]->vacante_rama),1,1,'L');

        /* EN LA UNIVESIDAD */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(45,6,utf8_decode('En la universidad'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(143,6,utf8_decode($data_form[0]->vacante_universidad),1,1,'L');

    // /* -------------------------------------------------------------------------------------------------- */
        $pdf->Ln(4);
    // /* --------------------------------------------------- QUINTA CAJA DE DATOS --------------------------------------------------- */
    // /* RECTANGULO DE LA QUINTA CAJA */
        $pdf->SetFillColor(199);
        $pdf->SetDrawColor(3,3,3);
        $pdf->SetLineWidth(.6);
        $pdf->Rect(14, 184, 188, 12, '.6', '');

        /* GROSOR DE LOS BORDES INTERNO DE LA TABLA*/
        $pdf->SetLineWidth(.2);

        /* VUESTRO GRADO ACADEMICO */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(68,6,utf8_decode('Vuestro Grado académico y/o Título actual'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(120,6,utf8_decode($data_form[0]->vancante_gradoActual),1,1,'L');

        /* QUIEN FINANCIA VUESTROS ESTUDIOS */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(68,6,utf8_decode('¿Quién financia vuestros estudios ?'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(120,6,utf8_decode($data_form[0]->vacante_financia),1,1,'L');

    // /* -------------------------------------------------------------------------------------------------- */
        $pdf->Ln(4);
    // /* --------------------------------------------------- SEXTA CAJA DE DATOS --------------------------------------------------- */
    // /* RECTANGULO DE LA SEXTA CAJA */
        $pdf->SetFillColor(199);
        $pdf->SetDrawColor(3,3,3);
        $pdf->SetLineWidth(.6);
        $pdf->Rect(14,200, 188, 30, '.6', '');

        /* GROSOR DE LOS BORDES INTERNO DE LA TABLA*/
        $pdf->SetLineWidth(.2);

        /* APELLIDOS Y NOMBRES DE LOS PADRES */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(188,5.5,utf8_decode('Apellidos y nombres de los padres'),1,1,'L');

        /* NOMBRE DE LA MADRE */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(18,5.5,utf8_decode('Madre:'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(88,5.5,utf8_decode($data_form[0]->madre_nombre),1,0,'L');

        /* EMAIL DE LA MADRE */
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(18,5.5,utf8_decode('Email'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(64,5.5,utf8_decode($data_form[0]->madre_correo),1,1,'L');

        /* NOMBRE DE LA PADRE */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(18,5.5,utf8_decode('Padre:'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(88,5.5,utf8_decode($data_form[0]->padre_nombre),1,0,'L');

        /* EMAIL DE LA PADRE */
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(18,5.5,utf8_decode('Email'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(64,5.5,utf8_decode($data_form[0]->padre_correo),1,1,'L');

        /* RECTANGULO PARA EL MULTICELL DE LA DIRECCION */
        $pdf->SetFillColor(199);
        $pdf->SetDrawColor(3,3,3);
        $pdf->SetLineWidth(.2);
        $pdf->Rect(74, 216.5, 128, 8, '.6', '');

        $pdf->SetLineWidth(.2);
        /* DIRECCION DOMICILIARIA ACTUAL, CIUDAD Y CODIGO POSTAL */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->MultiCell(60,4,utf8_decode('Dirección domiciliaria actual, ciudad y código postal:'),1,'L');
        $pdf->setXY(74,216.5);
        $pdf->SetFont('Arial','',9);
        if (strlen($data_form[0]->padres_direccion)>=90) {
            $pdf->MultiCell(128,4,utf8_decode($data_form[0]->padres_direccion),0,'L');
        } else {
            $pdf->Cell(128,8,utf8_decode($data_form[0]->padres_direccion),1,1,'L');
        }

    //     /* PREFIJO TELEFONICO DE CIUDAD, NUMERO DE CASA */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(72,5.5,utf8_decode('Prefijo telefónico de ciudad, número de casa:'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(116,5.5,utf8_decode($data_form[0]->padres_telefono.'    -    '.$data_form[0]->padres_celular),1,1,'L');


    // /* -------------------------------------------------------------------------------------------------- */
        $pdf->Ln(4);
    // /* --------------------------------------------------- SETIMA CAJA DE DATOS --------------------------------------------------- */
    /* RECTANGULO DE LA SETIMA CAJA */
        $pdf->SetFillColor(199);
        $pdf->SetDrawColor(3,3,3);
        $pdf->SetLineWidth(.6);
        $pdf->Rect(14,234, 188, 22, '.6', '');

    /* RECTANGULO DE LA SETIMA CAJA */
        $pdf->SetFillColor(199);
        $pdf->SetDrawColor(3,3,3);
        $pdf->SetLineWidth(.6);
        $pdf->Rect(14,256, 188, 30, '.6', '');

        /* GROSOR DE LOS BORDES INTERNO DE LA TABLA*/
        $pdf->SetLineWidth(.2);

        /* PERSONA A QUIEN INFORMAR */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(188,5.5,utf8_decode('Persona a quien informar sobre Usted (en caso de emergencia, urgencia o necesidad)'),1,1,'L');

        /* CAJA EN BLANCO */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(106,5.5,utf8_decode($data_form[0]->respon_nombre),1,0,'C');

        /* EMAIL */
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(18,5.5,utf8_decode('Email'),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(64,5.5,utf8_decode($data_form[0]->respon_correo),1,1,'L');

        /* EMAIL */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(70,5.5,utf8_decode('Prefijo telefónico de ciudad, número de casa: '),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(36,5.5,utf8_decode($data_form[0]->respon_telefono),1,0,'L');

        /* CELULAR MOVIL */
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(25,5.5,utf8_decode('Celular (móvil) '),1,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(57,5.5,utf8_decode($data_form[0]->respon_celular),1,1,'L');

        /* CAJA EN BLANCO */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(188,5.5,utf8_decode(''),1,1,'L');

        /* NOMBRES Y APELLIDOS DEL APODERADO O PERSONA RESPONSABLE */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(188,5.5,utf8_decode('Nombres y apellidos de la apoderado o persona responsable: '.$data_form[0]->apoder_nombre),1,1,'L');

        /* N° DOCUMENTO DE IDENTIDAD */
        $pdf->Cell(4);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(94,5.5,utf8_decode('N° de Documento de Identidad: '.$data_form[0]->apoder_dni),1,0,'L');

    /* RECTANGULO DE LA FIRMA ESTUDIANTE */
        $pdf->SetFillColor(199);
        $pdf->SetDrawColor(3,3,3);
        $pdf->SetLineWidth(.6);
        $pdf->Rect(108,261.5, 94, 24.5, '.6', '');

        /* FIRMA DEL ESTUDIANTE */
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(94,5.5,utf8_decode('Firma del (la) estudiante: '),0,1,'L');
        /* FIRMA DEL APODERADO */
        $pdf->SetLineWidth(.2);
        $pdf->Cell(4);
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(94,5.5,utf8_decode('Firma apoderado o responsable: '),0,1,'L');
        $pdf->Output('form_inscripcion.pdf','D');
        exit();
    }


}
