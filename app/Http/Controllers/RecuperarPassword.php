<?php

namespace App\Http\Controllers;

use App\User;
use DateTime;

use Carbon\Carbon;
use PHPMailer\PHPMailer;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RecuperarPassword extends Controller
{
    public function validarRequest(Request $request)
    {
        //You can add validation login here
        $user = DB::table('users')->where('email', '=', $request->email)->get();
        //Check if the user exists
        if (count($user) < 1) {
            return redirect('/')->with(['email_recuperar_password' => 'Este correo no se encuentra registrado en ALAR. \n Por favor, ingrese un correo válido.', 'icon' => 'error' ]);
        }

        //Create Password Reset Token
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => Str::random(60),
            'created_at' => Carbon::now()
        ]);
        //Get the token just created above
        $tokenData = DB::table('password_resets')
            ->where('email', $request->email)
            ->latest()
            ->first();


        try {
            $subject = "Recuperar contraseña";
            $for = $request->email;

            $data_usuario = User::where('email', $request->email)->first();

            $text             = 'Recuperar Contraseña';
            $mail             = new PHPMailer\PHPMailer(); // create an instace of PHP MAILER
            $mail->isSMTP();
            $mail->SMTPDebug  = 0; // debugging:  0 = off (for production use, No debug messages), 1 = errors and messages, 2 = messages only
            $mail->SMTPAuth   = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host       = "mail.universidades-rusia.com";
            $mail->Port       = 465; // or 587
            $mail->IsHTML(true);
            $mail->Username = "alar_noreply@universidades-rusia.com";
            $mail->Password = "JJqgc5o7V(gk";
            $mail->SetFrom("alar_noreply@universidades-rusia.com", 'ALAR', 0);
            $mail->Subject = utf8_decode($subject);
            $mail->Body    = '<html>

                            <center><p style="font-size: 3rem; font-weight: bold;">Elige una nueva contrase&ntilde;a</p></center>
                            <div style="border:1px; background: #2837A1; margin-right: 4%; margin-left: 4%; padding-right: 20px; padding-left: 20px; padding-top: 0.5px; padding-bottom: 0.5px; border-radius: 15px;" >
                                <p style="color: #fff;font-size: 25px;">
                                    <strong>Estimado '.$data_usuario->name.', hemos recibido una solicitud para restablecer tu contraseña de la Zona de Usuario de ALAR.</strong>
                                </p>
                            </div>

                            <div style="border:1px; background: #fff; margin-top: 10px; margin-right: 4%; margin-left: 4%; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 20px; border-radius: 20px;  border: 4px outset #cccccc; background-color: #fff;" >
                            <br>
                            <p style="color: #000; font-size: 1.2rem; align:center;">Para reestablecer tu contrase&ntilde;a, haz click en el siguiente botón y llena el formulario:</p>
                                <a
                                href="'.config('base_url').''.$tokenData->token.'?email='.urlencode($request->email).'"
                                style="text-decoration: none; padding: 10px;font-weight: 600;font-size: 20px;color: #ffffff;background-color: #1883ba;border-radius: 6px; border: 2px solid #0016b0; margin-right: 4%; margin-top: 1px; align:center; vertical-align:middle; ">
                                    Restablece tu contrase&ntilde;a
                                </a>
                            <br>
                            <br>
                            <hr>
                            <p style="color: #000; font-size: 1.2rem; align:center;">
                                O copia y pega este link en tu navegador.<br>
                                <a href="https://www.universidades-rusia.com/programas/password/reset/'.$tokenData->token.'?email='.urlencode($request->email).'">
                                    https://www.universidades-rusia.com/programas/password/reset/'.$tokenData->token.'?email='.urlencode($request->email).'
                                </a>
                            </p>
                            <hr>
                            <p style="color: #000; font-size: 1.2rem; align:center; font-style: italic;">
                                *El botón y el enlace solo son válidos por una hora (60 minutos).
                            </p>
                        </div>

                        </html>';
        $mail->AddAddress($for);

            if ($mail->Send()) {
                return redirect('/')->with(['email_recuperar_password' => 'Se ha enviado un mensaje al correo: '.$request->email.' \n Puede que se encuentre en su bandeja de entrada, correos no deseados o spam.', 'icon' => 'success' ]);
            } else {
                return redirect('/')->with(['email_recuperar_password' => 'Algo salió mal al intentar enviar el correo electrónico, vuelva a intentarlo en unos minutos.', 'icon' => 'warning' ]);
            }
        } catch (\Throwable $th) {
            throw $th;
        }

    }
    public function resetearPassword(Request $request){
            //validar campos
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|exists:users,email',
                'password' => 'required|confirmed',
                'token' => 'required' ]
            );

            //Redireccionar si no están correctos los datos
            if ($validator->fails()) {
                return redirect()->back()->withErrors(['email' => 'Por favor, rellene este campo.']);
            }

            $password = $request->password;// Validar token

            $tokenData = DB::table('password_resets')
                            ->where('token', $request->token)
                            ->first();
            $horaInicio = new DateTime(date('y-m-d H:i:s'));
            $horaTermino = new DateTime(date('y-m-d H:i:s', strtotime($tokenData->created_at)));
            $intervalo = $horaInicio->diff($horaTermino);
            $minutos = ($intervalo->d * 24 * 60) + ($intervalo->h * 60) + $intervalo->i;

            // Redireccionar si el token no existe
            // Redireccionar si el token ya tiene más de una hora (60 min) de creación
            if (!$tokenData || $minutos>=60)
                return redirect('/')->with([
                                        'email_recuperar_password' => 'El enlace que ha usado para recuperar su contraseña ya expiró.',
                                        'icon' => 'warning'
                                        ]);

            $user = User::where('email', $tokenData->email)->first();
            // Redireccionar si el correo es inválido
            if (!$user) return redirect()->back()->withErrors(['email' => 'Email no válido']);
            //Cifrar (hashear) la nueva contraseña y acutalizarla
            $user->password = \Hash::make($password);
            $user->update(); //or $user->save();

            //Logear al usuario
            Auth::login($user);

            //Eliminar el token
            DB::table('password_resets')
                ->where('email', $user->email)
                ->delete();

            return redirect('/inicio')
                    ->with([
                        'email_recuperar_password' => 'Su contraseña se ha actualizado exitosamente.',
                        'icon' => 'success'
                    ]);

            // POR VER AGREGAR CONFIRMACION DE CAMBIO DE CONTRASEÑA

        }
}
