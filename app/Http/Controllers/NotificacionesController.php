<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Notificaciones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificacionesController extends Controller
{
    public function __construct (){
        Carbon::setLocale('es');
    }
    public function getNotificacionesxID()
    {
        $time_not = array();

        $notificaciones = DB::table('notificaciones')
                                ->select('notificaciones.*','users.name','users.lastname')
                                ->join('users', 'users.id', '=', 'notificaciones.user1')
                                ->where('user2',auth()->id())
                                ->orderBy('notificaciones.id', 'desc')
                                ->get();
        if (count($notificaciones)) {
            foreach ($notificaciones as $not) {
                $time_ago = new Carbon($not->created_at);
                array_push($time_not,$time_ago->diffForHumans());
            }
        }

        return [
            'notificaciones' => $notificaciones,
            'time_ago'       => $time_not,
        ];
    }

    public function getRazon(Request $request)
    {
        // return $request->all();
        $razon = DB::table('notificaciones')
                    ->select('razon2','razon_constancia','tipo')
                    ->where('id',$request->id)
                    ->get();
        return $razon;
    }

}
