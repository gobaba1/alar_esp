<?php

namespace App\Http\Controllers\Api;

use view;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{

    public function store(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = User::where('email',$request->email)->first();
            // dd($user->id);
            Auth::loginUsingId($user->id);
            // dd(Auth::user());
            if(Auth::check()){
                Auth::setUser($user);
                // return view('/inicio');
                return redirect()->route('inicio');
            } else {
                dd('asdas');
            }
        } else {
            return 2;
        }
    }
}