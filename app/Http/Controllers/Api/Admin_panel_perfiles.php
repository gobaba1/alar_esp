<?php

namespace App\Http\Controllers\Api;

use App\Perfil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class Admin_panel_perfiles extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perfiles = DB::connection('mysql2')->table('perfil')
                    ->select('perfil.descripcion', 'perfil.idrama','rama.descripcion as rama', 'perfil.id')
                    ->join('rama', 'rama.id', 'perfil.idrama')
                    ->get();

        $collect_ramas= collect();
        $collect_ramas->push(['value' => '', 'descripcion' => 'Seleccione una Rama', 'id' => null]);
        $ramas = DB::connection('mysql2')->table('rama')->get();

        foreach ($ramas as $key => $value) {
            // dd($value);
            $collect_ramas->push(['value' => $value->descripcion, 'descripcion' => $value->descripcion, 'id' => $value->id]);
        }

        return json_encode([
            'perfiles' => $perfiles,
            'ramas' => $collect_ramas
        ]);

    }

    public function store(Request $request)
    {
        $data = (object) $request->data;
        // dd($data);
        $perfil = new Perfil();
        $perfil->descripcion = $data->descripcion;
        $perfil->idrama = $data->rama;

        $save = $perfil->save();

        if ($save) {
            return json_encode(['mensaje' => 'ok']);
        } else {
            return json_encode(['mensaje' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        $data = (object) $request->data;
        // dd($data);
        $perfil = Perfil::find($data->id);
        $perfil->descripcion = $data->descripcion;
        $perfil->idrama = $data->idrama;
        $save = $perfil->save();

        if ($save) {
            return json_encode(['mensaje' => 'ok']);
        } else {
            return json_encode(['mensaje' => 'error']);
        }
    }

    public function destroy($id)
    {
        $perfil = Perfil::destroy($id);
        if ($perfil) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }


    }
}
