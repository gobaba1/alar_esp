<?php

namespace App\Http\Controllers\Api;

use App\Notificaciones AS ModelNotificaciones;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Events\UpdateDocNotifications;

class UserDocsController extends Controller
{
    public function store(Request $request)
    {
        $id_user = $request->id;
        $docs_user = DB::table('validaciones_archivos')
                        ->where('user_id',$id_user)
                        ->get();
        $folder_id = DB::table('perfiles')
                        ->select('folder_id')
                        ->where('user_id',$id_user)
                        ->get();

        return json_encode([
            'docs_user' => $docs_user,
            'folder_url'  => $folder_id,
        ]);
    }
}
