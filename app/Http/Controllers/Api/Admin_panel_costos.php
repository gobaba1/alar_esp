<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Costos_presupuesto;
class Admin_panel_costos extends Controller
{
    public function index()
    {
        $costos = DB::connection('mysql2')->table('costos_presupuesto')
                                        ->join('universidad', 'universidad.id', '=', 'costos_presupuesto.id_universidad')
                                        ->select('costos_presupuesto.id','universidad.id as id_universidad','universidad.nombre','costos_presupuesto.costo_vivienda','costos_presupuesto.costo_alimentacion','costos_presupuesto.costo_seguro')
                                        ->get();
        return json_encode(['costos' => $costos]);
    }


    public function store(Request $request)
    {
        $data = (object) $request->data;
        // dd($data);
        $verificacion = DB::connection('mysql2')->table('costos_presupuesto')->where('id_universidad','=',$data->id_universidad)->first();
        if(!isset($verificacion->id)){
            return json_encode(['mensaje' => 'error']);
        }
        $costos = new Costos_presupuesto();
        $costos->id_universidad = $data->id_universidad;
        $costos->costo_vivienda = $data->costo_vivienda;
        $costos->costo_alimentacion = $data->costo_alimentacion;
        $costos->costo_seguro = $data->costo_seguro;
        $save = $costos->save();
        if ($save) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error2']);
        }

    }

      public function update(Request $request, $id)
    {
        $data = (object) $request->data;
        // dd($data);
        $costos = Costos_presupuesto::find($data->id);
        $costos->costo_vivienda = $data->costo_vivienda;
        $costos->costo_alimentacion = $data->costo_alimentacion;
        $costos->costo_seguro = $data->costo_seguro;
        $save = $costos->save();
        if ($save) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }

    public function destroy($id)
    {
        $costos = Costos_presupuesto::destroy($id);
        if ($costos) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }
}
