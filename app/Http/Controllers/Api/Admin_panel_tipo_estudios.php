<?php

namespace App\Http\Controllers\Api;

use App\TipoEstudio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class Admin_panel_tipo_estudios extends Controller
{
    public function index()
    {
        $tipoE = DB::connection('mysql2')->table('tipo_estudio')->get();

        return json_encode(['tipoE' => $tipoE]);
    }


    public function store(Request $request)
    {
        $data = (object) $request->data;
        // dd($data);
        $tipoE = new TipoEstudio();
        $tipoE->descripcion = $data->descripcion;

        $tipoE->save();
        if ($tipoE) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }


    public function update(Request $request, $id)
    {
        $data = (object) $request->data;
        // dd($data->nombre);

        $tipoE = TipoEstudio::find($data->id);
        $tipoE->descripcion = $data->descripcion;

        $tipoE->save();
        if ($tipoE) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }

    public function destroy($id)
    {
        $tipoE = TipoEstudio::destroy($id);
        if ($tipoE) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }
}
