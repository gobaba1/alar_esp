<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Events\UpdateDocNotifications;
use App\Notificaciones AS ModelNotificaciones;

class UpdateStateDocController extends Controller
{
    public function store(Request $request)
    {
        // return $request->all();

        $id_doc = $request->id;
        $state = $request->state;

        $docs_user = DB::table('validaciones_archivos')
                        ->where('id', $id_doc)
                        ->update(['state' => $state]);
        return json_encode($docs_user);
    }
}
