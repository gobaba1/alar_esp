<?php

namespace App\Http\Controllers\Api;

use App\Universidad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class Admin_panel_universidades extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $universidades = DB::connection('mysql2')
                            ->table('universidad')
                            ->select('universidad.nombre',
                            'universidad.estado',
                            'universidad.id',
                            'universidad.abrev',
                            'universidad.url',
                            'universidad.idciudad',
                            'universidad.nomImage',
                            'ciudad.nombre as ciudad')
                            ->join('ciudad', 'universidad.idciudad','=','ciudad.id')
                            ->get();
        $select_ciudad = DB::connection('mysql2')
                            ->table('ciudad')
                            ->get();
        $collect_ciudad = collect();
        $collect_ciudad->push(['value' => '', 'text' => 'Seleccione una ciudad', 'id' => '']);

        foreach ($select_ciudad as $key => $value) {
            $collect_ciudad->push(['value' => $value->nombre, 'text' => $value->nombre, 'id' => $value->id]);
        }

        return json_encode([
            'data' => $universidades,
            'ciudad' => $collect_ciudad,
            'mensaje' => 'OK'
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = (object) $request->data;
        
        $universidad = new Universidad();
        $universidad->nombre = $datos->universidad;
        $universidad->abrev = strtoupper($datos->abrev);
        $universidad->url = $datos->url;
        $universidad->idciudad = $datos->ciudad;
        $universidad->estado = $datos->estado;
        $universidad->save();
        $universidad->nomImage = 'images/universidades/'. $universidad->id.'.jpg';
        
        $save = $universidad->save();

        if($request->logo!=null){
            $data = explode(',',$datos->logo);
            $imagen= base64_decode($data[1]);
            Storage::disk('logo_universidades')->put($universidad->id.'.jpg',$imagen);
        }
        
    
        if ($save) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datos = (object) $request->data;
       
        

        $universidad = Universidad::find($id);

        $universidad->nombre = $datos->nombre;
        $universidad->abrev = strtoupper($datos->abrev);
        $universidad->url = $datos->url;
        $universidad->idciudad = $datos->idciudad;
        $universidad->estado = $datos->estado;
        
        $universidad->nomImage = 'images/universidades/'. $universidad->id.'.jpg';
        $exists = Storage::disk('logo_universidades')->exists($universidad->id.'.jpg');
        
        if($request->logo!=null){
            $data = explode(',',$request->logo);
            $imagen = base64_decode($data[1]);
            Storage::disk('logo_universidades')->put($universidad->id.'.jpg',$imagen);
        }
        $universidad->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_programa = Universidad::find($id);
        $programa = Universidad::destroy($id);

        $delete = Storage::disk('logo_universidades')->delete($data_programa->abrev.'.jpg');
        if ($programa) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }

    }
}
