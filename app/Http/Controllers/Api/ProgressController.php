<?php

namespace  App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
class ProgressController extends Controller
{
    public function store(Request $request){
        //ACTUALIZAR
        $progress           = DB::table('videos_user')->where('video_id',$request->video_id)
                                            ->where('user_id',$request->user_id)
                                            ->update(['progress' => $request->checked]); 
        //CALCULAR EL PROGRESS BAR
        $data_videos        = DB::table('videos_user')
                                        ->join('videos', 'videos.id', '=', 'videos_user.video_id')
                                        ->where('user_id', $request->user_id)
                                        ->where('state', '1')
                                        ->select('videos.path','videos.title','videos.id','videos_user.progress')
                                        ->orderBy('videos.id','asc')->get();
        $data_progress      = DB::table('videos_user')
                                        ->join('videos', 'videos.id', '=', 'videos_user.video_id')
                                        ->where('user_id',  $request->user_id)
                                        ->where('state', '1')
                                        ->where('progress', 'checked')
                                        ->get();
        $value_progress     = (count($data_progress)*100)/count($data_videos);
        return $value_progress;
    }
}
