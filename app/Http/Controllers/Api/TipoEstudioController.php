<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TipoEstudioController extends Controller
{

    public function index()
    {
        $tipoE = DB::connection('mysql2')->table('tipo_estudio')->get();

        return json_encode(['tipoE' => $tipoE]);
    }


    public function store(Request $request)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
