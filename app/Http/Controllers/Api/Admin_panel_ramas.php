<?php

namespace App\Http\Controllers\Api;

use App\Ramas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class Admin_panel_ramas extends Controller
{

    public function index()
    {
        $ramas = DB::connection('mysql2')->table('rama')->get();

        return json_encode(['ramas' => $ramas]);
    }


    public function store(Request $request)
    {
        $data = (object) $request->data;
        // dd($data);
        $rama = new Ramas();
        $rama->descripcion = $data->descripcion;
        $save = $rama->save();
        if ($save) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }

    }

      public function update(Request $request, $id)
    {
        $data = (object) $request->data;
        // dd($data);
        $rama = Ramas::find($data->id);
        $rama->descripcion = $data->descripcion;
        $save = $rama->save();
        if ($save) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }

    public function destroy($id)
    {
        $rama = Ramas::destroy($id);
        if ($rama) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }
}
