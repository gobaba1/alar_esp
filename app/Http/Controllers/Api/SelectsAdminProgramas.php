<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SelectsAdminProgramas extends Controller
{
    public function index()
    {
        // TIPO DE ESTUDIO
        $tipo_estudios = DB::connection('mysql2')
                        ->table('tipo_estudio')
                        ->get();
        $collect_tipo_estudio= collect();
        $collect_tipo_estudio->push(['value' => '', 'text' => 'Seleccione un Tipo de Estudio', 'id' => '']);
        foreach ($tipo_estudios as $key => $value) {
            // dd($value);
            $collect_tipo_estudio->push(['value' => $value->descripcion, 'text' => $value->descripcion, 'id' => $value->id]);
        }
        // RAMAS
        $ramas = DB::connection('mysql2')
                        ->table('rama')
                        ->get();
        $collect_rama= collect();
        $collect_rama->push(['value' => '', 'text' => 'Seleccione una Rama', 'id' => '']);
        foreach ($ramas as $key => $value) {
            // dd($value);
            $collect_rama->push(['value' => $value->descripcion, 'text' => $value->descripcion, 'id' => $value->id]);
        }
        // PERFIL
        $perfiles = DB::connection('mysql2')
                        ->table('perfil')
                        ->get();
        $collect_perfil= collect();
        $collect_perfil->push(['value' => '', 'text' => 'Seleccione un Perfil', 'id' => '']);
        foreach ($perfiles as $key => $value) {
            // dd($value);
            $collect_perfil->push(['value' => $value->descripcion, 'text' => $value->descripcion, 'id' => $value->id]);
        }
        // UNIVERSIDAD
        $universidades = DB::connection('mysql2')
                        ->table('universidad')
                        ->get();
        $collect_universidad= collect();
        $collect_universidad->push(['value' => '', 'text' => 'Seleccione una universidad', 'id' => '']);
        foreach ($universidades as $key => $value) {
            // dd($value);
            $collect_universidad->push(['value' => $value->nombre, 'text' => $value->nombre.' - '.$value->abrev, 'id' => $value->id]);
        }
        // IDIOMA
        $idiomas = DB::connection('mysql2')
                        ->table('idioma')
                        ->get();
        $collect_idioma= collect();
        $collect_idioma->push(['value' => '', 'text' => 'Seleccione un idioma', 'id' => '']);
        foreach ($idiomas as $key => $value) {
            // dd($value);
            $collect_idioma->push(['value' => $value->descripcion, 'text' => $value->descripcion, 'id' => $value->id]);
        }

        return json_encode([
            'tipo_estudio' => $collect_tipo_estudio,
            'rama' => $collect_rama,
            'perfil' => $collect_perfil,
            'universidad' => $collect_universidad,
            'idioma' => $collect_idioma,
        ]);
    }
}