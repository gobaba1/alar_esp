<?php

namespace App\Http\Controllers\Api;

use App\Programa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class Programas_admin extends Controller
{
    public function index()
    {

        $programas = DB::connection('mysql2')
                        ->table('programa')
                        ->select('programa.id',
                        'programa.codigo',
                        'programa.descripcion',
                        'universidad.nombre as universidad',
                        'universidad.id as id_universidad',
                        'programa.duracion',
                        'tipo_estudio.descripcion as tipo_estudio',
                        'tipo_estudio.id as id_tipo_estudio',
                        'rama.descripcion as rama',
                        'rama.id as id_rama',
                        'idioma.descripcion as idioma',
                        'idioma.id as id_idioma',
                        'programa.beca',
                        'programa.estado_inscrip',
                        'perfil.descripcion as perfil',
                        'perfil.id as id_perfil' ,
                        'costo.costo_d as costo',
                        'costo.divisa as divisa')
                        ->join('tipo_estudio', 'programa.idtipoestudio', '=', 'tipo_estudio.id')
                        ->join('rama', 'programa.idrama', '=', 'rama.id')
                        ->join('universidad', 'programa.iduniversidad', '=', 'universidad.id')
                        ->join('perfil', 'programa.idperfil', '=', 'perfil.id')
                        ->join('idioma', 'programa.ididioma', '=', 'idioma.id')
                        ->join('costo', 'programa.id', '=', 'costo.idprograma')
                        ->get();
        return json_encode([
            'data' => $programas
        ]);
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $programa = Programa::find($request->data['id']);
        // dd($programa);
        $programa->descripcion = $request->data['descripcion'];
        $programa->codigo = trim($request->data['codigo']);
        $programa->duracion = $request->data['duracion'];
        $programa->idtipoestudio = $request->data['id_tipo_estudio'];
        $programa->idrama = $request->data['id_rama'];
        $programa->iduniversidad = $request->data['id_universidad'];
        $programa->estado_inscrip = $request->data['estado_inscrip'];
        $programa->ididioma = $request->data['id_idioma'];
        $programa->beca = $request->data['beca'];
        $programa->idperfil = $request->data['id_perfil'];
        $update = $programa->save();
        DB::connection('mysql2')->table('costo')
            ->where('idprograma',$request->data['id'])
            ->update(['costo_d' => $request->data['costo'],'divisa'=>$request->data['divisa']]);
        if ($update) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }

    public function destroy
    ($id)
    {
        $programa = Programa::destroy($id);
        DB::connection('mysql2')->table('costo')->where('idprograma',$id)->delete();
        if ($programa) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }
    public function store(Request $request)
    {
        $data = (object)$request->data;

        // $programa = new Programa();
        // $programa->descripcion = $data->programa;
        // $programa->duracion = $data->duracion;
        // $programa->ididioma = $data->idioma;
        // $programa->codigo = $data->codigo;
        // $programa->idtipoestudio = $data->tipo_estudio;
        // $programa->idrama = $data->rama;
        // $programa->idperfil = $data->perfil;
        // $programa->iduniversidad = $data->universidad;
        // $programa->beca = $data->beca;
        // $programa->estado_inscrip = $data->estado;        
        // $save = $programa->save();
        // $currentId = $save->id;
        //
        $codigo=(!empty($data->codigo)) ? $data->codigo : "" ;
        $idprograma = DB::connection('mysql2')->table('programa')->insertGetId(
            [   'descripcion' => $data->programa, 
                'duracion' => $data->duracion,
                'ididioma' => $data->idioma,
                'codigo' => $codigo,
                'idtipoestudio' => $data->tipo_estudio,
                'idrama' => $data->rama,
                'idperfil' => $data->perfil,
                'iduniversidad' => $data->universidad,
                'beca' => $data->beca,
                'estado_inscrip' => $data->estado,
            ]
        );
        $save_costo=DB::connection('mysql2')->table('costo')->insert(
            ['idprograma' => $idprograma, 'idtipocosto' => 1,'costo_d'=>$data->costo,'divisa'=>$data->divisa]
        );
        if ($save_costo) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }
}
