<?php
namespace App\Http\Controllers\Api;
use view;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CodePhoneController extends Controller{
    public function store(Request $request){
        $codePhone = DB::table('pais')->where('id',$request->id)->select('codigoTelefono')->get(); 
        return $codePhone;
    }
}   

?>