<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
class ChangeImageController extends Controller
{
    public function store(Request $request){
        $data = explode(',',$request->imagen);
        $imagen= base64_decode($data[1]);
        Storage::disk('images')->put($request->id.'.jpg',$imagen);
        return "ok";
    }
    public function getImage($filename){
        $file = Storage::disk('images')->get($filename);
        return new Response($file,200);
    }
}
