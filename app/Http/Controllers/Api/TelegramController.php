<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TelegramController extends Controller
{
    public function store(Request $request){
        // TABLA USER 
        $user=DB::table('users')->where('users.email',$request->correo);
        //ACTUALIZAR
        $resultado  = $user->where('email',$request->correo)->update(['idTelegram' => $request->idTelegram]);
        if($resultado==1){
            // VALIDAR STATE PROFILE
            $data_perfil = $user->select('perfiles.*')->join('perfiles','perfiles.user_id','=','users.id')->first();
            $campos=['sexo','fecNacimiento','idPais','ciudad','nacionalidad','prefijo','celular','direccion','codPostal','telefono'];
            $validador=0;
            for ($i=0; $i < count($campos); $i++) {
                $dato=(string)$campos[$i]; 
                if($data_perfil->$dato !== null){
                    $validador++;
                }
            }
            if($validador=='10'){
                $state  = $user->where('email',$request->correo)->update(['stateProfile' => '1']);
            }
        }
        return $resultado;
    }
}