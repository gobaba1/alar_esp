<?php

namespace App\Http\Controllers\Api;

use App\Ciudad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class Admin_panel_ciudades extends Controller
{

    public function index()
    {
        $ciudades = DB::connection('mysql2')->table('ciudad')->get();

        return json_encode(['ciudades' => $ciudades]);
    }

    public function store(Request $request)
    {
        $data = (object) $request->data;
        // dd($data->nombre);
        $ciudad = new Ciudad();
        $ciudad->nombre = $data->nombre;

        $ciudad->save();
        if ($ciudad) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }

    public function update(Request $request, $id)
    {
        $data = (object) $request->data;
        // dd($data->nombre);

        $ciudad = Ciudad::find($data->id);
        $ciudad->nombre = $data->nombre;

        $save = $ciudad->save();
        if ($save) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }

    }

    public function destroy($id)
    {
        $ciudad = Ciudad::destroy($id);
        if ($ciudad) {
            return json_encode(['mensaje' => 'ok']);
        }else{
            return json_encode(['mensaje' => 'error']);
        }
    }
}
