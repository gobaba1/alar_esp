<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth;
class VerificarUsuarioController extends Controller
{
    public function Verificar(Request $request){
        $correo     =   $request["correo"];
        $clave      =  $request["contrasena"];
        // VALIDAR USUARIO
        $verificar=DB::connection('mysql3')->table('wp_users')->select('wp_users.*')
                                        ->where('wp_users.user_email',$correo)
                                        ->first();
        // VALIDAR EN LA SEGUNDA TABLA
        if($verificar!=null){
            // Verificar en la tabla users
            $usuario= User::where('email',$correo)->first();
            if(!empty($usuario)==0){
                $user   = User::create([
                    'name' => $verificar->display_name,
                    'email' =>$correo,
                    'password' =>\Hash::make($clave),
                ]);
                $user->assignRole('CURSO_RUSO');
                // Añadir curso al usuario
                $clases = DB::table('videos')->select('videos.*');
                switch ($verificar->user_status) {
                    case 2:
                        $clases->where('id_modulo','<',3);
                        break;
                    case 4:
                        $clases->where('id_modulo','<',5);
                        break;
                    case 6:
                        $clases->where('id_modulo','<',7);
                        break;
                    case 8:
                        $clases->where('id_modulo','<',9);
                        break;
                }
                $id_modulos=$clases->get();
                foreach ($id_modulos as $variable) {
                    $create = DB::table('videos_user')->insert([
                        ['user_id' => $user->id, 'video_id' => $variable->id, 'state' => 1, 'progress' => ''],
                    ]);
                }
                $user = User::find($user->id);
                Auth::Login($user, true);
                return redirect('/mi_preparacion');
            }else{
                $user = User::find($usuario->id);
                Auth::Login($user, true);
                return redirect('/mi_preparacion');
            }
        }else{
            return back()->withInput();
        }
    }
}
