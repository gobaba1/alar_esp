<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class ProgramaController extends Controller
{
    public function selectAll(){
        $tipo_estudio = DB::connection('mysql2')->table('tipo_estudio')->select('tipo_estudio.id','tipo_estudio.descripcion')->get();
        $ramas = DB::connection('mysql2')->table('rama')->select('rama.id','rama.descripcion')->get();
        $ciudades=DB::connection('mysql2')->table('ciudad')->select('ciudad.*')->get();
        // dd($tipo_estudio);
        return view('welcome',array(
            'tipo_estudio' =>$tipo_estudio,
            'ramas'=> $ramas,
            'ciudades'=> $ciudades
        ));
    }
    public function selectBuscador(){
        $tipo_estudio = DB::connection('mysql2')->table('tipo_estudio')->select('tipo_estudio.id','tipo_estudio.descripcion')->get();
        $ramas = DB::connection('mysql2')->table('rama')->select('rama.id','rama.descripcion')->where('rama.descripcion', '<>', 'Facultad Preparatoria')->get();
        $ciudades=DB::connection('mysql2')->table('ciudad')->select('ciudad.*')->get();
        return array(
            'tipo_estudio' =>$tipo_estudio,
            'ramas'=> $ramas,
            'ciudades'=> $ciudades
        );
    }
    public function selectPerfiles($id_rama){
        $perfiles = DB::connection('mysql2')->table('perfil')->select('perfil.*')->where('idrama',$id_rama)->get();
        return $perfiles;
    }
    public function monedaLocal($iso4217){
        $moneda = DB::connection('mysql2')->table('divisa')->select('divisa.*')->where('iso4217',$iso4217)->get();
        return $moneda;
    }
    public function Divisa($iso4217){
        $datos = DB::connection('mysql2')->table('divisa')
                                        ->select('ratio','iso4217')
                                        ->where('iso4217','RUB')
                                        ->orWhere('iso4217', 'USD')
                                        ->orWhere('iso4217',$iso4217)
                                        ->get();
        foreach ($datos as $value) {
            switch ($value->iso4217) {
                case 'USD':
                    $ratio['USD']=$value->ratio;
                    break;
                case 'RUB':
                    $ratio['RUB']=$value->ratio;
                    break;
                default:
                    $ratio['LOCAL']=$value->ratio;
                    break;
            }
        }
        return array("message" => $ratio);
    }
    public function Presupuesto($iduniversidad){
        $datos = DB::connection('mysql2')->table('costos_presupuesto')->select('costos_presupuesto.*')->where('id_universidad',$iduniversidad)->get();
        $datos2 = DB::connection('mysql2')->table('programa')
                                            ->join('costo','programa.id','=','costo.idprograma')
                                            ->select('costo.costo_d','costo.divisa')
                                            ->where('programa.iduniversidad',$iduniversidad)
                                            ->where('programa.descripcion','like','%'.'Facultad Preparatoria III'.'%')
                                            ->get();
        return array("message"=>$datos,"facultad"=>$datos2);
    }
    public function filtroPrograma(Request $request){
        $moneda=$request->moneda;
        $pagina=$request->cantidadHojas;
        $presupuesto = DB::connection('mysql2')->table('programa')
                                        ->join('costo','programa.id','=','costo.idprograma')
                                        ->select('programa.iduniversidad as presu_id','costo.costo_d as presu_costo_d')
                                        ->where('programa.descripcion','=','Facultad Preparatoria III');
        $rspt=DB::connection('mysql2')->table('programa')
                                        ->join('tipo_estudio', 'programa.idtipoestudio', '=', 'tipo_estudio.id')
                                        ->join('rama', 'programa.idrama', '=', 'rama.id')
                                        ->join('universidad', 'programa.iduniversidad', '=', 'universidad.id')
                                        ->join('idioma', 'programa.ididioma', '=', 'idioma.id')
                                        ->join('ciudad', 'universidad.idciudad', '=', 'ciudad.id')
                                        ->join('costo', 'costo.idprograma', '=', 'programa.id')
                                        ->join('divisa', function($join)use($moneda)
                                        {
                                            $join->where('divisa.iso4217', '=',$moneda);
                                        })
                                        ->join('perfil', 'perfil.id', '=', 'programa.idperfil')
                                        ->joinSub($presupuesto, 'presupuesto', function ($join){
                                            $join->on('programa.iduniversidad', '=', 'presupuesto.presu_id');
                                        });
        if((int)$request->tipoestudio>0){
                $rspt->where('programa.idtipoestudio', '=',(int) $request->tipoestudio);
        };
        if((int)$request->rama>0){
            $rspt->where('programa.idrama', '=',(int) $request->rama);
        };
        if ((int)$request->perfil > 0) {
            $rspt->where('perfil.id', '=',(int) $request->perfil);
        }
        if ((int)$request->idioma > 0) {
            $rspt->where('programa.ididioma', '=',(int) $request->idioma);
        }
        if ((int)$request->idCiudad > 0) {
            $rspt->where('ciudad.id', '=',(int) $request->idCiudad);
        }
        if(!empty($request->array_programas)){
           $consulta=$request->array_programas;
            $rspt->where(function ($query)use($consulta) {
                $query->where('programa.descripcion', 'like','%'.$consulta."%")
                ->orWhere('perfil.descripcion', 'like','%'.$consulta."%");
            });
        }
        $results=$rspt->select(['programa.id AS id_programa',
        'programa.descripcion AS descripcion_programa',
        'programa.codigo AS codigo_programa',
        'programa.duracion AS duracion_programa',
        'programa.estado_inscrip AS estado_inscrip',
        'programa.creditos_ects AS creditos_ects',
        'programa.beca AS beca',
        'programa.idtipoestudio as tipo_estudio',
        'tipo_estudio.descripcion AS tipo_descripcion',
        'rama.descripcion AS rama',
        'universidad.id AS id_universidad',
        'universidad.nombre AS universidad',
        'universidad.url AS url',
        'universidad.abrev AS abreviatura',
        'universidad.nomImage AS nomImage',
        'ciudad.nombre AS ciudad',
        'idioma.descripcion AS idioma',
        'costo.costo_d AS costo_d',
        'costo.divisa AS divisa',
        'divisa.iso4217 AS iso4217',
        'divisa.ratio AS ratio',
        'divisa.simbolo AS simbolo',
        'perfil.descripcion AS perfil',
        'presupuesto.presu_costo_d'
        ])->paginate($pagina);
        
        $ratioRUB = DB::connection('mysql2')->table('divisa')->select('divisa.ratio')->where('iso4217','RUB')->get();
        //INTRODUCIR ESTADISTICA
            if(!empty($request->tipoestudio)){
                DB::table('estadisticas_programas_buscador')->insert([
                    'tipo_estudio'      => $request->tipoestudio,
                    'rama_estudio'      => $request->rama,
                    'perfil_estudio'    => $request->perfil,
                    'idioma'            => $request->idioma,
                    'ciudad'            => $request->idCiudad,
                    'moneda'            => intval($moneda),
                    'pais_procedencia'  => $request->pais_procedencia,
                    'region_procedencia'=> $request->region_procedencia,
                    "created_at"        =>  \Carbon\Carbon::now(), # new \Datetime()
                    "updated_at"        => \Carbon\Carbon::now(),  # new \Datetime()
                ]);
            }
        // Todos los programas
        return json_encode(['results' => $results,'ratioRUB'=>$ratioRUB,'simbolo'=>$request->simbolo,'pagination'=>[
            'total'         =>$results->total(),
            'current_page'  =>$results->currentPage(),
            'per_page'      =>$results->perPage(),
            'last_page'     =>$results->lastPage(),
            'from'          =>$results->firstItem(),
            'to'            =>$results->lastItem(),
        ]]);
        // return array(
        //     'results'=>$results,'ratioRUB'=>$ratioRUB,'simbolo'=>$request->simbolo
        // );
    }
    public function AllProgramas(){
        $programas = DB::connection('mysql2')->table('programa')->select('programa.id','programa.descripcion')->get();
        return array("programas"=>$programas);
    }
    public function listarPresupuesto(Request $request){
        $message = DB::connection('mysql2')->table('costos_presupuesto')
                                        ->select('costos_presupuesto.*')
                                        ->where('id_universidad',$request->id_universidad)
                                        ->get();
        // Estadisticas
        DB::table('estadisticas_programas_interacccion')->insert([
            'tipo'              => "presupuesto",
            'id_programa'       => $request->id_programa,
            'pais_procedencia'  => $request->pais_procedencia,
            'region_procedencia'=> $request->region_procedencia,
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);
        return array("message" => $message);
    }
    public function inscripcionPrograma(Request $request){
        // Estadisticas
        DB::table('estadisticas_programas_interacccion')->insert([
            'tipo'              => "inscripcion",
            'id_programa'       => $request->id_programa,
            'pais_procedencia'  => $request->pais_procedencia,
            'region_procedencia'=> $request->region_procedencia,
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);
    }
}
?>
