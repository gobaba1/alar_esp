<?php

namespace App\Http\Controllers;

use Facade\FlareClient\View;
use Illuminate\Http\Request;
use App\Events\Notificaciones;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Notificaciones as NotClass;

use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function subirArchivo(Request $request)
	{
		//SUBIDA DE LA MINIATURA
		$image = $request->file('archivo');
		if ($image) {
			$image_path = time() . $image->getClientOriginalName();
			\Storage::disk('public')->put('imagenes_usuario/user' . auth()->user()->id . '/passport.' . $image->getClientOriginalExtension(), \File::get($image));
		}
		broadcast(new Notificaciones( 'Actualizó su foto de perfil' ,
									(string)auth()->user()->name, (string)auth()->user()->lastname, 'image' ));
		
		$admin_users = DB::table('model_has_roles')->where('role_id', 2)->get();
		foreach ($admin_users as $user_id ) {
			$notificacion_created = NotClass::create([
				'user1' 			=> auth()->user()->id,
				'user2' 			=> $user_id->model_id,
				'razon_constancia' 	=> 'Actualizó su foto de perfil.',
				'sustento' 			=> 'Foto de perfil.',
			]);
		}

		toast('Su foto de perfil se ha actualizado correctamente !','success')
            ->position('top')
            ->hideCloseButton()
			->autoClose(2000);
		return redirect('/inicio');
	}
}
