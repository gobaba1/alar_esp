<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Form_inscripcions;
class Form_inscripcionController extends Controller
{
    public function update(Request $request){
        $verification = Form_inscripcions::where('user_id', auth()->id())->first();
        $verification->user_id = auth()->id();
        $verification->facultad_necesita = $request->facultad_necesita ;
        if($request->facultad_necesita=='2'){
            $verification->facultad_mes = $request->facultad_mes;
            $verification->facultad_anio = $request->facultad_anio;
            $verification->facultad_universidad = $request->facultad_universidad;
            
        }else if($request->facultad_necesita=='1' | $request->facultad_necesita=='0'){
            $verification->facultad_mes = null;
            $verification->facultad_anio = null;
            $verification->facultad_universidad = null;
        }
        $verification->vacante_estudio = $request->vacante_estudio;
        $verification->vacante_universidad = $request->vacante_universidad;
        $verification->vacante_rama = $request->vacante_rama;
        $verification->vancante_gradoActual = $request->vancante_gradoActual;
        $verification->vacante_financia = $request->vacante_financia;
        $verification->madre_nombre = $request->madre_nombre;
        $verification->madre_correo = $request->madre_correo;
        $verification->padre_nombre = $request->padre_nombre;
        $verification->padre_correo = $request->padre_correo;
        $verification->padres_direccion = $request->padres_direccion;
        $verification->padres_telefono = $request->padres_telefono;
        $verification->padres_celular = $request->padres_celular;
        $verification->respon_nombre = $request->respon_nombre;
        $verification->respon_correo = $request->respon_correo;
        $verification->respon_telefono = $request->respon_telefono;
        $verification->respon_celular = $request->respon_celular;
        $verification->apoder_nombre = $request->apoder_nombre;
        $verification->apoder_dni = $request->apoder_dni;
        $verification->save();
        return "done";
    }
    public function validar_formulario(){
        $data_formulario  = DB::table('form_inscripcions')->where('user_id', auth()->id())->get();
        $campos=['facultad_necesita','facultad_mes','facultad_anio','facultad_universidad','vacante_estudio','vacante_universidad','vacante_rama','vancante_gradoActual',
        'vacante_financia','madre_nombre','madre_correo','padre_nombre','padre_correo','padres_direccion','padres_telefono','padres_celular','respon_nombre','respon_correo',
        'respon_telefono','respon_celular','apoder_nombre','apoder_dni'];
        $limite;
        if($data_formulario[0]->facultad_necesita==2){
            $limite=22;
        }else{
            $limite=19;
        }
        $validador_formulario=0;
        for ($i=0; $i < count($campos); $i++) {
            $dato=(string)$campos[$i]; 
            if($data_formulario[0]->$dato !== null || !empty($data_formulario[0]->$dato)){
                $validador_formulario++;
            }
        }
        $mostrar=false;
        if($validador_formulario==$limite){
            $mostrar=true;
        }
        return array("mostrar"=>$mostrar);
    }
}
