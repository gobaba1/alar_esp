<?php

namespace App\Http\Controllers;
use PHPMailer\PHPMailer;
use App\DocumentoToUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Events\UpdateDocNotifications;
use App\Notificaciones AS ModelNotificaciones;
class DocumentoToUserController extends Controller
{
    public function enviar_documento_to_user(Request $request)
    {
        $data_user=DB::table('users')->select('users.idTelegram','users.email','users.name')
                    ->where('id', $request->u53810)->get();
        // ENVIAR CORREO
        $subject = "Alar ha enviado un documento";
        $for = $data_user[0]->email;
        // $for = "gobaba28@gmail.com";
        // dd($data_usuario);

        $text             = 'Alar Envió un documento';
        $mail             = new PHPMailer\PHPMailer(); // create a n
        $mail->isSMTP();
        $mail->SMTPDebug  = 2; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth   = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host       = "mail.universidades-rusia.com";
        $mail->Port       = 465; // or 587
        $mail->IsHTML(true);
        $mail->Username = "alar_noreply@universidades-rusia.com";
        $mail->Password = "JJqgc5o7V(gk";
        $mail->SetFrom("alar_noreply@universidades-rusia.com", 'ALAR', 0);
        $mail->Subject = utf8_decode($subject);
        $mail->Body    = '<html>
                        <center><p style="font-size: 3rem; font-weight: bold;">Respuesta de documento</p></center>
                        <div style="border:1px; background: #2837A1; margin-right: 4%; margin-left: 4%; padding-right: 20px; padding-left: 20px; padding-top: 0.5px; padding-bottom: 0.5px; border-radius: 15px;" >
                            <p style="color: #fff;font-size: 25px;">
                                <strong>Estimado '.$data_user[0]->name.', ALAR ha enviado un documento, porfavor revisar la zona de usuario en la sección de mi inscripción.</strong>
                            </p>
                        </div>
                        </html>';
        $mail->AddAddress($for);
        $mail->Send();
        // ENVIAR TELEGRAM 
        file_get_contents("https://api.telegram.org/bot833065748:AAH4kCQQQJoC7HIHk2pFxvbfFYexFWrZXfI/sendmessage?chat_id=".$data_user[0]->idTelegram."&text=ALAR ha enviado un documento, porfavor revisar la zona de usuario en la sección de mi inscripción.");
        $documento_to_user = new DocumentoToUser();
        $documento_to_user->type = $request->tipo_documento;
        $documento_to_user->url_drive = $request->url_documento;
        $documento_to_user->id_admin = auth()->id();
        $documento_to_user->id_alumno = $request->u53810;
        $documento_to_user->state = '2'; // RECIBIDO
        $documento_to_user->save();
        // ENVIAR NOTIFICACION
        $notificacion = new ModelNotificaciones();
        $notificacion->user1 = 1;    //usuario que envia;
        $notificacion->user2 =  $request->u53810;               //usuario que recibe;
        $notificacion->razon_constancia = "Entra al siguiente enlace" ;
        $notificacion->sustento = "Enviado por alar";
        $notificacion->razon2 = $request->url_documento;
        $notificacion->tipo = "aprobado";
        $notificacion->save();
        broadcast( new UpdateDocNotifications($request->id_user, 'Ha subido su : ', (string)$notificacion->sustento , 1) );
        return back()->withInput();
    }
}
