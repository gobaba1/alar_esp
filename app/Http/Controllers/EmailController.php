<?php

namespace App\Http\Controllers;

use App\User;
use PHPMailer\PHPMailer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function olvido_password(Request $request){

        $subject = "Recuperar contraseña";
        $for = $request->email;

        $data_usuario = User::where('email', $for)->first();

        $text             = 'Recuperar Contraseña';
        $mail             = new PHPMailer\PHPMailer(); // create a n
        $mail->isSMTP();
        $mail->SMTPDebug  = 2; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth   = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host       = "mail.universidades-rusia.com";
        $mail->Port       = 465; // or 587
        $mail->IsHTML(true);
        $mail->Username = "alar_noreply@universidades-rusia.com";
        $mail->Password = "JJqgc5o7V(gk";
        $mail->SetFrom("alar_noreply@universidades-rusia.com", 'ALAR', 0);
        $mail->Subject = utf8_decode($subject);
        $mail->Body    = '<html>

                        <center><p style="font-size: 3rem; font-weight: bold;">Elige una nueva contrase&ntilde;a</p></center>
                        <div style="border:1px; background: #2837A1; margin-right: 4%; margin-left: 4%; padding-right: 20px; padding-left: 20px; padding-top: 0.5px; padding-bottom: 0.5px; border-radius: 15px;" >
                            <p style="color: #fff;font-size: 25px;">
                                <strong>Estimado '.$data_usuario->name.', hemos recibido una solicitud para restablecer tu contraseña de la Zona de Usuario de ALAR.</strong>
                            </p>
                        </div>

                        <div style="border:1px; background: #fff; margin-top: 10px; margin-right: 4%; margin-left: 4%; padding-right: 20px; padding-left: 20px; padding-top: 10px; padding-bottom: 20px; border-radius: 20px;  border: 4px outset #cccccc; background-color: #fff;" >
                        <br>
                            <a href="https://www.universidades-rusia.com/zonaUsuario/'.$data_usuario->id.'" style="text-decoration: none;
                            padding: 10px;font-weight: 600;font-size: 20px;color: #ffffff;background-color: #1883ba;border-radius: 6px; border: 2px solid #0016b0; margin-right: 4%; margin-top: 1px; align:center; vertical-align:middle; ">
                                Restablece tu contrase&ntilde;a
                            </a>
                            <br>
                            <br>
                            <hr>
                            <p style="color: #000; font-size: 1.2rem; align:center;">
                                O copia y pega este link en tu navegador.<br>
                                <a href="https://www.universidades-rusia.com/zonaUsuario/'.$data_usuario->id.'">https://www.universidades-rusia.com/zonaUsuario/'.$data_usuario->id.'</a>
                            </p>
                        </div>

                        </html>';
        $mail->AddAddress($for);
        if ($mail->Send()) {
            return 'Email enviado !';
        } else {
            return 'Error al enviar Email';
        }
    }
}
