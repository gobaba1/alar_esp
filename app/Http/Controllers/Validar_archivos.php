<?php

namespace App\Http\Controllers;
use App\User;
use Carbon\Carbon;
use App\Form_inscripcions;
use Illuminate\Http\Request;
use App\Validaciones_archivos;
use App\DocumentoToUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Validar_archivos extends Controller
{
    public function index(Request $request)
    {
        // VALIDACION DE MI PERFIL
        $stateProfile    = User::select('stateProfile')->where('id', auth()->id())->first();
        // dd($stateProfile->stateProfile);
        if($stateProfile->stateProfile==null){
            return redirect('/inicio')->with('status', 'No puede acceder a Mi inscripción hasta haber completado todo el perfil');;   
        }
        //NOTIFICACIONES
        $time_not = array();
        $notificaciones = DB::table('notificaciones')
                            ->join('users', 'users.id', '=', 'notificaciones.user1')
                            ->where('user2', auth()->id())
                            ->select('notificaciones.id','notificaciones.razon_constancia', 'notificaciones.created_at','users.name', 'users.lastname')
                            ->orderBy('notificaciones.created_at','DESC')->get();
        $notificaciones = (count($notificaciones) == 0) ? array() : $notificaciones;
        if (count($notificaciones)) {
            foreach ($notificaciones as $not) {
                $time_ago = new Carbon($not->created_at);
                array_push($time_not,$time_ago->diffForHumans());
            }
        }
        //CREAR REGISTROS EN VALIDACIONES ARCHIVOS (formulario_pdf,pasaporte,contrato)
        $result  = Validaciones_archivos::where('user_id',auth()->id())->get();
        if (count($result)==0){
            $archivos_name=['formulario_pdf','pasaporte','contrato'];
            for ($i=0; $i < count($archivos_name); $i++) {
                # code...
                Validaciones_archivos::create([
                    'user_id'       => auth()->id(),
                    'nameArchivo'   =>$archivos_name[$i],
                    'state'         =>'0',
                    'type'          =>'preferencial'
                ]);
            }
        }
        //CREAR REGISTRO DE FORMULARIO
        $verificationFormulario = DB::table('form_inscripcions')->where('user_id', auth()->id())->get();
        if (count($verificationFormulario)==0){
            Form_inscripcions::create([
                'user_id' => auth()->id(),
            ]);
        }
        $data_formulario        = DB::table('form_inscripcions')->where('user_id', auth()->id())->get();
        //ESTADOS DE LOS ARCHIVOS DE PROGRAMA DE ACCESOS PREFERENCIAL
        $data_validaciones    = Validaciones_archivos::where('user_id', auth()->id())->limit(3)->get();
        foreach ($data_validaciones as $type ){
            switch ($type["nameArchivo"]) {
                case 'pasaporte':
                    switch ($type["state"]) {
                        case '0': $passport = 'bg-danger' ;$textPassport="Subir Archivo";$propertyPassport="data-toggle=modal data-target=.modal_upload_passport " ;$levelPasaporte="0"; break;
                        case '1': $passport = 'bg-warning';$textPassport="Archivo Pendiente";$propertyPassport="disabled='disabled'"; $levelPasaporte="1";break;
                        case '2': $passport = 'bg-success';$textPassport="Aprobado";$propertyPassport="disabled='disabled'";$levelPasaporte="2"; break;
                    };
                    $datosPasaporte=[
                        "bgPasaporte"       =>$passport,
                        "textPasaporte"     =>$textPassport,
                        "propertyPasaporte" =>$propertyPassport,
                        "levelPasaporte"    =>$levelPasaporte
                    ];
                break;
                case 'contrato':
                    switch ($type['state']) {
                        case '0': $contrato = 'bg-danger' ;$textContrato="Firmar Contrato"; $propertyContrato="data-toggle=modal data-target=.modal_firmar_contrato ";$levelContrato="0";break;
                        case '1': $contrato = 'bg-warning' ; $textContrato="Archivo pendiente"; $propertyContrato="disabled='disabled'";$levelContrato="1";break;
                        case '2': $contrato = 'bg-success' ; $textContrato="Aprobado"; $propertyContrato="disabled='disabled'";$levelContrato="2";break;
                    }
                    $datosContrato=[
                        "bgContrato"       =>$contrato,
                        "textContrato"     =>$textContrato,
                        "propertyContrato" =>$propertyContrato,
                        "levelContrato"    =>$levelContrato,
                    ];
                break;
                case 'formulario_pdf':
                    switch ($type['state']) {
                        case '0': $formulario = 'bg-danger';$textFormulario="Firmar Contrato" ; $propertyFormulario="data-toggle=modal data-target=.modal_upload_form";$levelFormulario="0";break;
                        case '1': $formulario = 'bg-warning' ; $textFormulario="Archivo pendiente"; $propertyFormulario="disabled='disabled'";$levelFormulario="1";break;
                        case '2': $formulario = 'bg-success' ; $textFormulario="Aprobado"; $propertyFormulario="disabled='disabled'";$levelFormulario="2";break;
                    }
                    $datosFormulario=[
                        "bgFormulario"       =>$formulario,
                        "textFormulario"     =>$textFormulario,
                        "propertyFormulario" =>$propertyFormulario,
                        "levelFormulario"    =>$levelFormulario,
                    ];
                break;
            }
        }
        //ESTADOS DE LOS ARCHIVOS DOCUMENTOS ACADEMICOS
        $data_doc_academicos      = Validaciones_archivos::where('user_id', auth()->id())
                                                            ->where('type','academicos')
                                                            ->get();
        $state_Secundaria="enabled";
        $state_Partida="enabled";
        $state_Universitario="enabled";
        if(count($data_doc_academicos)>0){
            foreach ($data_doc_academicos as $key) {
                switch ($key['nameArchivo']) {
                    case 'Certificado de Estudios Secundaria':
                        switch ($key['state']) {
                            case '0':
                                $state_Secundaria="enabled";
                                break;
                            default:
                                $state_Secundaria="disabled";
                            break;
                        }
                    break;
                    case 'Certificado de Estudios Universidad':
                        switch ($key['state']) {
                            case '0':
                                $state_Universitario="enabled";
                                break;
                            default:
                                $state_Universitario="disabled";
                            break;
                        }
                    break;
                    case 'Partida de Nacimiento':
                        switch ($key['state']) {
                            case '0':
                                $state_Partida="enabled";
                                break;
                            default:
                                $state_Partida="disabled";
                            break;
                        }
                    break;
                }
                # code...
            }
        }
        $data_disables_academicos=[
            "state_Secundaria" => $state_Secundaria,
            "state_Universitario"=>$state_Universitario,
            "state_Partida"=>$state_Partida,
        ];
        //OTROS DOCUMENTOS
        $data_otherDocuments     = Validaciones_archivos::where('user_id', auth()->id())
                                                        ->where('type','documentos')
                                                        ->get();
        //Obtener el id del folder del usuario en Google Drive
        $folder_id=DB::table('perfiles')
                        ->where('user_id', auth()->id())
                        ->select('perfiles.folder_id')->get();
        $exists = Storage::disk('images')->exists(auth()->id().'.jpg');
        //DOCUMENTOS POR PARTE DE ALAR
        $documents_to_user= DocumentoToUser::where('id_alumno',auth()->id())->get();
        // VALIDAR CAMPOS FORMULARIO
        $campos=['facultad_necesita','facultad_mes','facultad_anio','facultad_universidad','vacante_estudio','vacante_universidad','vacante_rama','vancante_gradoActual',
        'vacante_financia','madre_nombre','madre_correo','padre_nombre','padre_correo','padres_direccion','padres_telefono','padres_celular','respon_nombre','respon_correo',
        'respon_telefono','respon_celular','apoder_nombre','apoder_dni'];
            $validador_formulario=0;
            for ($i=0; $i < count($campos); $i++) {
                $dato=(string)$campos[$i]; 
                if($data_formulario[0]->$dato !== null || !empty($data_formulario[0]->$dato)){
                    $validador_formulario++;
                }
            } 
        return view('/mi_inscripcion',[
            'user'                      => auth()->user()->id,
            'notificaciones'            => $notificaciones,
            'notificaciones_time'       => $time_not,
            'folder_id'                 =>$folder_id,
            'datosFormulario'           =>$datosFormulario,
            'datosContrato'             =>$datosContrato,
            'datosPasaporte'            =>$datosPasaporte,
            'data_doc_academicos'       =>$data_doc_academicos,
            'data_disables_academicos'  =>$data_disables_academicos,
            'data_otherDocuments'       =>$data_otherDocuments,
            'data_formulario'           =>$data_formulario,
            'foto_perfil'               =>$exists,
            'validador_formulario'      =>$validador_formulario,
            'documents_to_user'         =>$documents_to_user,
            // 'class_name_validador'      =>$class_name_validador,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
