<?php

namespace App\Http\Controllers;

use App\User;
use App\Perfiles;
use Carbon\Carbon;
use App\Perfil_idiomas;
use Illuminate\Http\Request;
use App\Events\Notificaciones;

use League\Flysystem\Filesystem;
use Illuminate\Support\Facades\DB;

use App\Notificaciones as NotClass;
use Illuminate\Support\Facades\Storage;

//CONTROLLER GOOGLE
use Illuminate\Support\ServiceProvider;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Validator;
use Hypweb\Flysystem\GoogleDrive\GoogleDriveAdapter;

class PerfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function __construct (){
        Carbon::setLocale('es');
    }
    public function index(Request $request)
    {
        $role_user = DB::table('model_has_roles')->where('model_id', auth()->id())->value('role_id');
        $time_not = array();
        $notificaciones = DB::table('notificaciones')
                            ->join('users', 'users.id', '=', 'notificaciones.user1')
                            ->where('user2', auth()->id())
                            ->select('notificaciones.id','notificaciones.razon_constancia', 'notificaciones.created_at','users.name', 'users.lastname')
                            ->orderBy('notificaciones.created_at','DESC')->get();
        $notificaciones = (count($notificaciones) == 0) ? array() : $notificaciones;
        if (count($notificaciones)) {
            foreach ($notificaciones as $not) {
                $time_ago = new Carbon($not->created_at);
                array_push($time_not,$time_ago->diffForHumans());
            }
        }
        switch ($role_user) {
            case '1': #USER_ROL
                $results = DB::table('perfiles')->where('user_id', auth()->id())->get();
                $data_user      = User::where('id',auth()->id())->get();
                if (count($results)==0){
                    
                    //  dd($folder->id);
                    Perfiles::create([
                        'user_id' => auth()->id(),
                        // 'folder_id'=>$folder->id,
                    ]);
                }
                $data_perfil    = Perfiles::where('user_id', auth()->id())->get();
                $perfil_idiomas = Perfil_idiomas::where('user_id',auth()->id())->get();
                $exists = Storage::disk('images')->exists(auth()->id().'.jpg');
                // dd(count($perfil_idiomas));
                return view('/perfil',[
                    'perfil'                        => $data_perfil,
                    'user'                          => $data_user,
                    'perfil_idiomas'                => $perfil_idiomas,
                    'notificaciones'                => $notificaciones,
                    'notificaciones_time'           => $time_not,
                    'foto_perfil'           => $exists

                ]);
                break;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $rules = [
            'name'              => 'required',
            'lastname'          => 'required',
            'genero'            => 'required',
            'idiomas_state'     => 'required',
            'birthday'          => 'required',
            'pais'              => 'required',
            'ciudad'            => 'required',
            'nacionalidad'      => 'required',
            'celular'           => 'required',
            'direccion'         => 'required',
            'codPostal'         => 'required',
            'telefono'          => 'required',
            'prefijoHide'       => 'required',
        ];
        $messages = [
            'required'            => 'Asegúrese que todos los campos con el (*) estén completos.',
        ];
        $validaData = Validator::make($request->all(), $rules, $messages);
        if($validaData->fails()){
            return  back()
                ->with('mensaje_pefil', $validaData->messages()->all()[0])
                ->withInput();
        }
        $perfil = Perfiles::find($id);
        $perfil->sexo = $request->genero;
        $perfil->fecNacimiento = $request->birthday;
        $perfil->idPais = $request->pais;
        $perfil->ciudad = $request->ciudad;
        $perfil->nacionalidad = $request->nacionalidad;
        $perfil->celular = $request->celular;
        $perfil->prefijo=$request->prefijoHide;
        $perfil->direccion = $request->direccion;
        $perfil->codPostal = $request->codPostal;
        $perfil->telefono = $request->telefono;
        $perfil->facebook = $request->facebook;
        $perfil->numPasaporte = $request->pasaporte;
        $perfil->estado_civil = $request->estado_civil;
        $perfil->save();
        $perfil_idiomas=Perfil_idiomas::where('user_id', auth()->id())->get();
        if(count($perfil_idiomas)>0){
            $deletedRows = Perfil_idiomas::where('user_id', auth()->id())->delete();
        }
        if (isset($request->idioma)) {
            for ($i=0; $i < count($request->idioma); $i++) {
                Perfil_idiomas::create([
                    'hablado'=> $request->hablado[$i],
                    'escrito'=>$request->escrito[$i],
                    'lectura'=>$request->lectura[$i],
                    'user_id'=>auth()->id(),
                    'idioma_id'=>$request->idioma[$i],
                ]);
            }
        }
        //VERIFICAR DATA
        $data   = User::select('idTelegram')->where('id', auth()->id())->first();
        if($data->idTelegram!==null && $validaData->fails()==false){
            $user = User::find(auth()->id());
            $user->stateProfile='1';
            $user->save();
        }
        broadcast( new Notificaciones( 'Actualizó su foto de perfil',
                    (string)auth()->user()->name, (string)auth()->user()->lastname, 'image') );

        // return redirect('/perfil');
        
        return redirect('/perfil')->with(['mensaje_pefil' => 'Su perfil se actualizo correctamente', 'icon' => 'success' ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
