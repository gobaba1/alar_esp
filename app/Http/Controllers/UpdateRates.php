<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UpdateRates extends Controller
{
    public function updateRates()
    {
        $url = "https://openexchangerates.org/api/latest.json?app_id=187737026b6c4172a8b0cdd3a62a5062";
        $client = new \GuzzleHttp\Client();
        $respuesta = $client->get($url);
        $data =(string) $respuesta->getBody();
        $json = json_decode($data, true);

        foreach ($json['rates'] as $iso4217 => $ratio ) {
            $update = DB::connection('mysql2')->table('divisa')
              ->where('iso4217', $iso4217)
              ->update(['ratio' => $ratio, 'base' => $json['base'], 'timestamp' => $json['timestamp']]);
        }
    }
}
