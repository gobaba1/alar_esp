<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Artesaos\SEOTools\Facades\SEOTools as SEO;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        Carbon::setLocale('es');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // SEO::title('Programas ALAR');
        // SEO::setDescription('Ejemplo de descripción de la página');
        // SEO::opengraph()->setUrl('https://www.universidades-rusia.com/programas');
        // SEO::setCanonical('https://www.universidades-rusia.com/programas');
        // SEO::opengraph()->addProperty('type', 'articles');

        $role_user = DB::table('model_has_roles')->where('model_id', auth()->id())->value('role_id');
        $date = new Carbon();
        $time_not = array();
        $notificaciones = DB::table('notificaciones')
                                    ->join('users', 'users.id', '=', 'notificaciones.user1')
                                    ->where('user2', auth()->id())
                                    ->select('notificaciones.id','notificaciones.razon_constancia', 'notificaciones.created_at','users.name', 'users.lastname')
                                    ->orderBy('notificaciones.created_at','DESC')->get();
        $notificaciones = (count($notificaciones) == 0) ? array() : $notificaciones;
        if (count($notificaciones)) {
            foreach ($notificaciones as $not) {
                $time_ago = new Carbon($not->created_at);
                array_push($time_not,$time_ago->diffForHumans());
            }
        }
        switch ($role_user) {
            case '1': #USER_ROL
                $states = DB::table('users')->where('id', auth()->id())->select('users.state','users.stateProfile')->get();
                $exists = Storage::disk('images')->exists(auth()->id().'.jpg');
                return view('/inicio',[
                    'notificaciones'        => $notificaciones,
                    'date'                  => $date,
                    'notificaciones_time'   => $time_not,
                    'states'                => $states,
                    'foto_perfil'           => $exists
                ]);
                break;

            case '2': #ADMIN_ROLE
                $videos = DB::table('videos')
                            ->select('videos.*', 'videos.id AS id_video', 'modulos.nombre AS nombreModulo','modulos.abreviatura AS abrModulo')
                            ->join('modulos','modulos.id','=','videos.id_modulo')
                            ->get();

                $users = DB::table('users')
                            ->join('model_has_roles', 'model_id', '=', 'users.id')
                            ->where('role_id', 1)
                            ->select('users.*')
                            ->get();
                $exists = Storage::disk('images')->exists(auth()->id().'.jpg');

                return view('/inicio', [
                    'users'                 => $users,
                    'notificaciones'        => $notificaciones,
                    'notificaciones_time'   => $time_not,
                    'videos'                => $videos,
                    'foto_perfil'           => $exists
                    // 'modulos'               => $modulos
                ]);
                break;
            case '3' : #ADMINISTRADOR PROGRAMAS ROLE
            $count_programas = $programas = DB::connection('mysql2')
                                            ->table('programa')
                                            ->select('programa.id',
                                            'programa.codigo',
                                            'programa.descripcion',
                                            'universidad.nombre as universidad',
                                            'universidad.id as id_universidad',
                                            'programa.duracion',
                                            'tipo_estudio.descripcion as tipo_estudio',
                                            'tipo_estudio.id as id_tipo_estudio',
                                            'rama.descripcion as rama',
                                            'rama.id as id_rama',
                                            'idioma.descripcion as idioma',
                                            'idioma.id as id_idioma',
                                            'programa.beca',
                                            'programa.estado_inscrip',
                                            'perfil.descripcion as perfil',
                                            'perfil.id as id_perfil' )
                                            ->join('tipo_estudio', 'programa.idtipoestudio', '=', 'tipo_estudio.id')
                                            ->join('rama', 'programa.idrama', '=', 'rama.id')
                                            ->join('universidad', 'programa.iduniversidad', '=', 'universidad.id')
                                            ->join('perfil', 'programa.idperfil', '=', 'perfil.id')
                                            ->join('idioma', 'programa.ididioma', '=', 'idioma.id')
                                            ->count();

            $exists = Storage::disk('images')->exists(auth()->id().'.jpg');
                return view('/inicio', [
                    'foto_perfil'           => $exists,
                    'count_programas'       => $count_programas
                ]);
            break;
            case '4':
                return view('/inicio', [
                    'foto_perfil'    =>false
                ]);
            break;
        }
    }
}
