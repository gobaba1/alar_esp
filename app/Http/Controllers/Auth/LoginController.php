<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Socialite;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    //FACEBOOK
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        // $user = Socialite::driver('facebook')->user();
        $user = Socialite::driver('facebook')
                            ->fields([
                                'first_name',
                                'last_name',
                                'email',
                                'verified'
                            ])
                            ->stateless()
                            ->user();
        // dd($user);
        $url_photo_fb = $user->avatar_original;
        $user_exists = User::where('email', $user->user['email'])->get();

        if (count($user_exists)==0) {
            //CREATE USER
            $user = User::firstOrCreate([
                'name'          => $user->user['first_name'],
                'lastname'     => $user->user['last_name'],
                'email'         => $user->user['email'],
                'provider_id'   => $user->getId(),
                'password'      => Hash::make('12345678'),
            ]);
            //SAVE PHOTO PROFILE
            $contents = file_get_contents($url_photo_fb);
            Storage::disk('images')->put($user->id.'.jpg',$contents);
            $user->assignRole('USER_ROLE');
            Auth::Login($user, true);
            return redirect('/inicio');
        } else {
            // USER EXISTS
            $user = User::find($user_exists[0]->id);
            Auth::Login($user, true);
            return redirect('/inicio');
        }
    }
}
