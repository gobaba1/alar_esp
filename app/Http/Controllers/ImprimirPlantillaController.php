<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Plantilla_descarga_masiva;
use App\Exports\Plantilla_descarga_costo;
class ImprimirPlantillaController extends Controller
{
    function imprimir_excel_programas(){
        $tipo_estudio = DB::connection('mysql2')->table('tipo_estudio')->select('tipo_estudio.*')->get();
        $rama = DB::connection('mysql2')->table('rama')->select('rama.*')->get();
        $perfil = DB::connection('mysql2')->table('perfil')->select('perfil.*')->get();
        $mayor_array=max(count($tipo_estudio),count($rama),count($perfil));
        $obj=new Plantilla_descarga_masiva();
        $obj->rama=$rama;
        $obj->tipo_estudio=$tipo_estudio;
        $obj->perfil=$perfil;
        $obj->mayor_array=$mayor_array;
        $obj->view();
        return Excel::download($obj,'Plantilla_portugues.xls');
    }
    function imprimir_excel_costos(){
        $universidad= DB::connection('mysql2')->table('universidad')
        ->leftJoin ('costos_presupuesto','costos_presupuesto.id_universidad','universidad.id')
        ->select('universidad.nombre','universidad.abrev','costos_presupuesto.costo_vivienda','costos_presupuesto.costo_alimentacion','costos_presupuesto.costo_seguro','costos_presupuesto.costo_divisa')
        ->get();
        $obj=new Plantilla_descarga_costo();
        $obj->universidad=$universidad;
        $obj->view();
        return Excel::download($obj,'Plantilla_costos.xls');
    }
}
