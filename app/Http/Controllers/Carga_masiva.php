<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\ImportPrograma;
use App\ImportCostos;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class Carga_masiva extends Controller
{
    public function  cargar_data(Request $request){
        $datos=json_decode($request->content);
        $data = [
            'id_universidad' => $datos->universidad,
            'divisa'         => $datos->divisa, 
        ]; 
        $Excel_datos=file_get_contents($request->file_excel);
        Storage::disk('public')->put('subida_masiva.xls',$Excel_datos);
        $importar=new ImportPrograma($data);
        $resultado=Excel::import($importar,'subida_masiva.xls','public');
        Storage::disk('public')->delete('subida_masiva.xls');
        return json_encode(['data' =>  $importar->getErrores()]);
    }
    public function  cargar_data_costos(Request $request){
        $datos=json_decode($request->content);
        $Excel_datos=file_get_contents($request->file_excel);
        Storage::disk('public')->put('subida_masiva_costo.xls',$Excel_datos);
        Excel::import(new ImportCostos(),'subida_masiva_costo.xls','public');
        Storage::disk('public')->delete('subida_masiva_costo.xls');
        return json_encode(['data' => 'subido']);
    }
    public function eliminar_datos(Request $request){
        $programas = DB::connection('mysql2')->table('programa')->select('programa.id')->where('iduniversidad',$request->universidad)->get();
        if(count($programas)>0){
            foreach ($programas as $value) {
                DB::connection('mysql2')->table('programa')->where('id',$value->id)->delete();
                DB::connection('mysql2')->table('costo')->where('idprograma',$value->id)->delete();
            }
            return "ok";
        }else{
            return "error";
        }
    }
}
