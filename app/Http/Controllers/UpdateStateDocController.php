<?php

namespace App\Http\Controllers;

use App\Notificaciones AS ModelNotificaciones;
use Illuminate\Http\Request;
use PHPMailer\PHPMailer;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Events\UpdateDocNotifications;

class UpdateStateDocController extends Controller
{

    public function updateState(Request $request)
    {
        // return $request->all();

        $id_doc = $request->id;
        $state = $request->state;

        $docs_user = DB::table('validaciones_archivos')
                        ->where('id', $id_doc)
                        ->update(['state' => $state]);

        $data_user=DB::table('users')->select('users.idTelegram','users.email','users.name')
                    ->where('id', $request->id_user)->get();
        ############ ENVIAR Y GUARDAR NOTIFICACION   ############
        switch ($state) {
            case '0':
                $razon_constancia = 'Se ha rechazado su documento';
                $tipo_not = 'rechazo';
            break;
            case '2':
                $razon_constancia = 'Se ha aceptado su documento';
                $tipo_not = 'aprobado';

                break;
        }
        // ENVIAR MENSAJES POR TELEGRAM
        file_get_contents("https://api.telegram.org/bot833065748:AAH4kCQQQJoC7HIHk2pFxvbfFYexFWrZXfI/sendmessage?chat_id=".$data_user[0]->idTelegram."&text=".$razon_constancia);

        // ENVIAR CORREO
        $subject = "Respuesta a su documento enviado";
        $for = $data_user[0]->email;
        // $for = "gobaba28@gmail.com";
        // dd($data_usuario);

        $text             = 'Envio de documento';
        $mail             = new PHPMailer\PHPMailer(); // create a n
        $mail->isSMTP();
        $mail->SMTPDebug  = 2; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth   = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host       = "mail.universidades-rusia.com";
        $mail->Port       = 465; // or 587
        $mail->IsHTML(true);
        $mail->Username = "alar_noreply@universidades-rusia.com";
        $mail->Password = "JJqgc5o7V(gk";
        $mail->SetFrom("alar_noreply@universidades-rusia.com", 'ALAR', 0);
        $mail->Subject = utf8_decode($subject);
        $mail->Body    = '<html>
                        <center><p style="font-size: 3rem; font-weight: bold;">Respuesta de documento</p></center>
                        <div style="border:1px; background: #2837A1; margin-right: 4%; margin-left: 4%; padding-right: 20px; padding-left: 20px; padding-top: 0.5px; padding-bottom: 0.5px; border-radius: 15px;" >
                            <p style="color: #fff;font-size: 25px;">
                                <strong>Estimado '.$data_user[0]->name.', '.strtolower($razon_constancia).', para más información ingrese a su zona de usuario.</strong>
                            </p>
                        </div>
                        </html>';
        $mail->AddAddress($for);
        $mail->Send();
        // ENVIAR NOTIFICACION
        $notificacion = new ModelNotificaciones();
        $notificacion->user1 = 1;    //usuario que envia;
        $notificacion->user2 = $request->id_user;               //usuario que recibe;
        $notificacion->razon_constancia = $razon_constancia ;
        $notificacion->sustento = $request->nameArchivo;
        $notificacion->razon2 = $request->razon;
        $notificacion->tipo = $tipo_not;
        $notificacion->save();
        broadcast( new UpdateDocNotifications($request->id_user, 'Ha subido su : ', (string)$notificacion->sustento , 1) );
        ###################################################################################################################
        return json_encode($docs_user );
    }
}
