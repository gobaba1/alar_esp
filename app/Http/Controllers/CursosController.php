<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CursosController extends Controller
{
    public function getCursosxID(Request $request)
    {
        // return $request->all();

        $cursos = DB::table('videos_user')
                        ->select('videos.*', 'videos.id AS id_video', 'modulos.nombre AS nombreModulo','modulos.abreviatura AS abrModulo')
                        ->join('videos','videos.id','=','videos_user.video_id')
                        ->join('modulos','modulos.id','=','videos.id_modulo')
                        ->where('user_id', $request->id)
                        ->where('state', '1')
                        ->get();
        return json_encode([
            'cursos' => $cursos
        ]);


    }

    public function updateCursosActivosxID(Request $request)
    {
        // return $request->all();
        $id_user = $request->id_alumno;
        foreach ($request->id_cursos as $id_curso) {
            $verificar = DB::table('videos_user')
                            ->where('user_id', $id_user)
                            ->where('video_id', $id_curso)
                            ->get();
            if (count($verificar)==0) {
                $create = DB::table('videos_user')->insert([
                                ['user_id' => $id_user, 'video_id' => $id_curso, 'state' => 1, 'progress' => ''],
                            ]);
            }else {
                $update_status = DB::table('videos_user')
                                ->where('user_id', $id_user)
                                ->where('video_id', $id_curso)
                                ->update(['state' => '1']);
            }

        }
        // return $update_status;
    }

    public function removeCursosxID(Request $request)
    {
        // return $request->all();
        $id_user = $request->id_alumno;

        foreach ($request->cursos as $curso => $value) {
            // echo $value.'====';
            $update_status = DB::table('videos_user')
                                ->where('user_id', $id_user)
                                ->where('video_id', $value)
                                ->update(['state' => '0']);
        }

    }


    public function add_clasesXmodulos(Request $request)
    {
        // return $request->id;

        $id_modules = $request->id;
        $id_alumno = $request->id_alumno;

        foreach ($id_modules as $modulo ) {
            $clases = DB::table('videos')
            ->where('id_modulo',$modulo)
            ->get();
            foreach ($clases as $clase) {
                // print_r($clase->id.'=================');
                $verificar = DB::table('videos_user')
                            ->where('user_id', $id_alumno)
                            ->where('video_id', $clase->id)
                            ->get();
                if (count($verificar)==0) {
                    $create = DB::table('videos_user')->insert([
                                    ['user_id' => $id_alumno, 'video_id' => $clase->id, 'state' => 1, 'progress' => ''],
                                ]);
                } else {
                    $update_status = DB::table('videos_user')
                                    ->where('user_id', $id_alumno)
                                    ->where('video_id', $clase->id)
                                    ->update(['state' => '1']);
                    return redirect('/inicio');
                }
            }
        }

    }

    public function getModulos()
    {
        $modulos = DB::table('modulos')->get();
        return json_encode([ 'modulos' => $modulos]);
    }
}
