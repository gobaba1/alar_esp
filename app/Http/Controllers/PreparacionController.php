<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

class PreparacionController extends Controller
{
    public function index(){
        $data_videos = DB::table('videos_user')
                            ->join('videos', 'videos.id', '=', 'videos_user.video_id')
                            ->join('modulos', 'modulos.id', '=', 'videos.id_modulo')
                            ->where('user_id', auth()->id())
                            ->where('state', '1')
                            ->select('videos.path','videos.title','videos.id','videos_user.progress','videos.id_modulo','videos_user.id as video_user_id','modulos.nombre','modulos.abreviatura')
                            ->orderBy('videos.id','asc')->get();
        $videos = collect ($data_videos)->groupBy('id_modulo');
        $exists = Storage::disk('images')->exists(auth()->id().'.jpg');
        if(count($data_videos)>0){
            $data_progress    = DB::table('videos_user')
                                ->join('videos', 'videos.id', '=', 'videos_user.video_id')
                                ->where('user_id', auth()->id())
                                ->where('state', '1')
                                ->where('progress', 'checked')
                                ->get();

            $value_progress   = (count($data_progress)*100)/count($data_videos);
            return view('/mi_preparacion',[
                'data_videos'       => $videos,
                'value_progress'    => $value_progress,
                'foto_perfil'       => $exists,
                'mostrar'           => '1'
            ]);
        }else{
            return view('/mi_preparacion',[
                'foto_perfil'           => $exists,
                'mostrar'               => '0'
            ]);
        }
    }
    public function get_video($filename){
        $confirmation= DB::table('videos_user')
                            ->join('videos', 'videos.id', '=', 'videos_user.video_id')
                            ->where('user_id', auth()->id())
                            ->where('video_id', $filename)
                            ->where('state', '1')
                            ->select('videos.path','videos.title','videos.id')
                            ->orderBy('videos.id','asc')
                            ->get();
        if(count($confirmation)>0){
            $file = Storage::disk('videos')->get($filename.".mp4");
            return new Response($file,200);
        }
    }
}