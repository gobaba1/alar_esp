<?php

namespace App\Http\Controllers;

use App\Exports\Programas;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Carbon\Carbon;

class ImprimirPdfDomPdf extends Controller
{
    public function __construct()
    {
        Carbon::setLocale('es');
    }
    public function form_presupuesto($id_universidad,$id_programa,$moneda,$pais_procedencia,$region_procedencia){
        // Estadisticas 
        DB::table('estadisticas_programas_interacccion')->insert([
            'tipo'              => "descargar",
            'id_programa'       => $id_programa,
            'pais_procedencia'  => $pais_procedencia,
            'region_procedencia'=> $region_procedencia,
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);
        // TIPO DE CAMBIO RUBLOS
        $ratioRUB = DB::connection('mysql2')->table('divisa')->select('divisa.ratio')->where('iso4217','RUB')->first();
        //TIPO DE CAMBIO MONEDA LOCAL
        $ratioLOCAL=DB::connection('mysql2')->table('divisa')->select('divisa.ratio','divisa.simbolo','divisa.nombre')->where('iso4217',$moneda)->first();
        $divisa['nombre']=$ratioLOCAL->nombre;
        $divisa['simbolo']=$ratioLOCAL->simbolo;
        // COSTOS DE VIDA EN DOLARES
        $costo_vida = DB::connection('mysql2')->table('costos_presupuesto')
                                        ->select('costos_presupuesto.*')
                                        ->where('id_universidad',$id_universidad)
                                        ->get();
        //COSTO DE VIDA EN LA MONEDA LOCAL
        $presupuesto_local  = array(
            "costo_vivienda"    =>intval($costo_vida[0]->costo_vivienda*$ratioLOCAL->ratio),
            "costo_seguro"      =>intval($costo_vida[0]->costo_seguro*$ratioLOCAL->ratio),
            "costo_alimentacion"=>intval($costo_vida[0]->costo_alimentacion*$ratioLOCAL->ratio)
        );
        //COSTO DEL LA FACULTA PREPARATORIA
        $facultad = DB::connection('mysql2')->table('programa')
                                        ->join('costo','programa.id','=','costo.idprograma')
                                        ->select('programa.iduniversidad as presu_id','costo.costo_d as costo_facultad','costo.divisa')
                                        ->where('programa.descripcion','=','Facultad Preparatoria III')
                                        ->where('programa.iduniversidad',$id_universidad)->first();
        switch ($facultad->divisa) {
            case 'USD':
                // $presupuesto_rublos["costo_facultad"]=intval($facultad->costo_facultad*$ratioRUB->ratio);
                // $presupuesto_dolares["costo_facultad"]=intval($facultad->costo_facultad);
                $presupuesto_local["costo_facultad"]=intval($facultad->costo_facultad*$ratioLOCAL->ratio);
                break;
            case 'RUB':
                // $presupuesto_rublos["costo_facultad"]=intval($facultad->costo_facultad);
                // $presupuesto_dolares["costo_facultad"]=intval($facultad->costo_facultad/$ratioRUB->ratio);
                $presupuesto_local["costo_facultad"]=intval(($facultad->costo_facultad/$ratioRUB->ratio)*$ratioLOCAL->ratio);
        }
        //COSTO DEL PROGRAMA
        $programa = DB::connection('mysql2')->table('programa')
                                        ->join('costo','programa.id','=','costo.idprograma')
                                        ->select('programa.iduniversidad as presu_id','programa.idtipoestudio','programa.ididioma','costo.costo_d as costo_facultad','costo.divisa','programa.descripcion')
                                        ->where('programa.id',$id_programa)->first();
        
        $mostrar["columna"] = ($programa->idtipoestudio==5) ? false : true;//MUESTRA LA SEGUNDA COLUMNA DEL PRESUPUESTO
        if($programa->idtipoestudio==5){
            switch ($programa->descripcion) {
                case 'Facultad Preparatoria I':
                    $mostrar["caso_tabla"]="facultad1";
                    break;
                case 'Facultad Preparatoria II':
                    $mostrar["caso_tabla"]="facultad2";
                    break;
                case 'Facultad Preparatoria III':
                    $mostrar["caso_tabla"]="facultad3";
                    break;
            }
        }else{
            if($programa->ididioma==2){
                $mostrar["caso_tabla"]="carrera_ingles";
            }else{
                $mostrar["caso_tabla"]="carrera_ruso/español";
            }
        }
        switch ($programa->divisa) {
            case 'USD':
                // $presupuesto_rublos["costo_programa"]=intval($programa->costo_facultad*$ratioRUB->ratio);
                // $presupuesto_dolares["costo_programa"]=intval($programa->costo_facultad);
                $presupuesto_local["costo_programa"]=intval($programa->costo_facultad*$ratioLOCAL->ratio);
                break;
            case 'RUB':
                // $presupuesto_rublos["costo_programa"]=intval($programa->costo_facultad);
                // $presupuesto_dolares["costo_programa"]=intval($programa->costo_facultad/$ratioRUB->ratio);
                $presupuesto_local["costo_programa"]=intval(($programa->costo_facultad/$ratioRUB->ratio)*$ratioLOCAL->ratio);
        }
        // COSTO TOTAL EN RUBLOS
        // $costo_total_programas_rublos=$presupuesto_rublos["costo_programa"]+$presupuesto_rublos["costo_seguro"]+$presupuesto_rublos["costo_alimentacion"]+$presupuesto_rublos["costo_vivienda"];
        // $costo_total_facultad_rublos=$presupuesto_rublos["costo_facultad"]+$presupuesto_rublos["costo_seguro"]+$presupuesto_rublos["costo_alimentacion"]+$presupuesto_rublos["costo_vivienda"];
        
        // $presupuesto_rublos["total_programas"]=$costo_total_programas_rublos;
        // $presupuesto_rublos["total_facultad"]=$costo_total_facultad_rublos;
        
        // COSTO TOTAL EN DOLARES
        // $costo_total_programas_dolares=$presupuesto_dolares["costo_programa"]+$presupuesto_dolares["costo_seguro"]+$presupuesto_dolares["costo_alimentacion"]+$presupuesto_dolares["costo_vivienda"];
        // $costo_total_facultad_dolares=$presupuesto_dolares["costo_facultad"]+$presupuesto_dolares["costo_seguro"]+$presupuesto_dolares["costo_alimentacion"]+$presupuesto_dolares["costo_vivienda"];

        // $presupuesto_dolares["total_programas"]=$costo_total_programas_dolares;
        // $presupuesto_dolares["total_facultad"]=$costo_total_facultad_dolares;
        // COSTO TOTAL EN MONEDA LOCAL
        $costo_total_programas_local=$presupuesto_local["costo_programa"]+$presupuesto_local["costo_seguro"]+$presupuesto_local["costo_alimentacion"]+$presupuesto_local["costo_vivienda"];
        $costo_total_facultad_local=$presupuesto_local["costo_facultad"]+$presupuesto_local["costo_seguro"]+$presupuesto_local["costo_alimentacion"]+$presupuesto_local["costo_vivienda"];
        $presupuesto_local["total_programas"]=$costo_total_programas_local;
        $presupuesto_local["total_facultad"]=$costo_total_facultad_local;
        
        // COSTOS PARA CALENDARIO
        $costos_calendario=array();
        $costos_calendario["cal_costo_facultad"]=$presupuesto_local["costo_facultad"];
        $costos_calendario["cal_costo_programa"]=$presupuesto_local["costo_programa"]/2;
        $costos_calendario["cal_costo_seguro"]=$presupuesto_local["costo_seguro"];
        $costos_calendario["cal_costo_alimentacion"]=intval($presupuesto_local["costo_alimentacion"]/12);
        $costos_calendario["cal_costo_vivienda"]=intval($presupuesto_local["costo_vivienda"]/12);
        $costos_calendario["suma1"]=$costos_calendario["cal_costo_facultad"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_alimentacion"];
        $costos_calendario["suma2"]=$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_alimentacion"];
        $costos_calendario["suma3"]=$costos_calendario["cal_costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_alimentacion"];
        $costos_calendario["suma4"]=$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_alimentacion"];
        $costos_calendario["suma5"]=$costos_calendario["cal_costo_facultad"]+$costos_calendario["cal_costo_vivienda"];
        $costos_calendario["año"]=date("Y");
        //INFO GENERAL DEL PROGRAMA
        $info_general=DB::connection('mysql2')->table('programa')
        ->join('tipo_estudio', 'programa.idtipoestudio', '=', 'tipo_estudio.id')
        ->join('rama', 'programa.idrama', '=', 'rama.id')
        ->join('universidad', 'programa.iduniversidad', '=', 'universidad.id')
        ->join('idioma', 'programa.ididioma', '=', 'idioma.id')
        ->join('ciudad', 'universidad.idciudad', '=', 'ciudad.id')
        ->join('perfil', 'perfil.id', '=', 'programa.idperfil')
        ->select(['programa.id AS id_programa',
        'programa.descripcion AS descripcion_programa',
        'programa.codigo AS codigo_programa',
        'programa.duracion AS duracion_programa',
        'programa.beca AS beca',
        'tipo_estudio.descripcion AS tipo_descripcion',
        'rama.descripcion AS rama',
        'universidad.id AS id_universidad',
        'universidad.nombre AS universidad',
        'universidad.url AS url',
        'universidad.abrev AS abreviatura',
        'universidad.nomImage AS nomImage',
        'ciudad.nombre AS ciudad',
        'idioma.descripcion AS idioma',
        'perfil.descripcion AS perfil',
        ])->where('programa.id',$id_programa)->first();
        $fecha = Carbon::parse(Carbon::now(), 'UTC');
        $fecha_proforma = $fecha->isoFormat('D [de] MMMM [del] YYYY, h:mm:ss a');
        $pdf = \PDF::loadView('dompdf_pdf/pdf_form_presupuesto', compact('presupuesto_local','info_general', 'fecha_proforma','costos_calendario','mostrar','divisa'));
        $pdf->setPaper('A4', 'landscape');
        // dd($pdf);
        return $pdf->stream('ALAR_formulario_form_presupuesto.pdf');
    }
    public function imprimir_excel_presupuesto($id_universidad,$id_programa,$moneda,$pais_procedencia,$region_procedencia){
        // Estadisticas 
        DB::table('estadisticas_programas_interacccion')->insert([
            'tipo'              => "descargar",
            'id_programa'       => $id_programa,
            'pais_procedencia'  => $pais_procedencia,
            'region_procedencia'=> $region_procedencia,
            "created_at" =>  \Carbon\Carbon::now(), # new \Datetime()
            "updated_at" => \Carbon\Carbon::now(),  # new \Datetime()
        ]);
        // TIPO DE CAMBIO RUBLOS
        $ratioRUB = DB::connection('mysql2')->table('divisa')->select('divisa.ratio')->where('iso4217','RUB')->first();
        //TIPO DE CAMBIO MONEDA LOCAL
        $ratioLOCAL=DB::connection('mysql2')->table('divisa')->select('divisa.ratio','divisa.simbolo','divisa.nombre')->where('iso4217',$moneda)->first();
        $divisa['nombre']=$ratioLOCAL->nombre;
        $divisa['simbolo']=$ratioLOCAL->simbolo;
        // COSTOS DE VIDA EN DOLARES
        $costo_vida = DB::connection('mysql2')->table('costos_presupuesto')
                                        ->select('costos_presupuesto.*')
                                        ->where('id_universidad',$id_universidad)
                                        ->get();
        //COSTO DE VIDA EN LA MONEDA LOCAL
        $presupuesto_local  = array(
            "costo_vivienda"    =>intval($costo_vida[0]->costo_vivienda*$ratioLOCAL->ratio),
            "costo_seguro"      =>intval($costo_vida[0]->costo_seguro*$ratioLOCAL->ratio),
            "costo_alimentacion"=>intval($costo_vida[0]->costo_alimentacion*$ratioLOCAL->ratio)
        );
        //COSTO DEL LA FACULTA PREPARATORIA
        $facultad = DB::connection('mysql2')->table('programa')
                                        ->join('costo','programa.id','=','costo.idprograma')
                                        ->select('programa.iduniversidad as presu_id','costo.costo_d as costo_facultad','costo.divisa')
                                        ->where('programa.descripcion','=','Facultad Preparatoria III')
                                        ->where('programa.iduniversidad',$id_universidad)->first();
        switch ($facultad->divisa) {
            case 'USD':
                // $presupuesto_rublos["costo_facultad"]=intval($facultad->costo_facultad*$ratioRUB->ratio);
                // $presupuesto_dolares["costo_facultad"]=intval($facultad->costo_facultad);
                $presupuesto_local["costo_facultad"]=intval($facultad->costo_facultad*$ratioLOCAL->ratio);
                break;
            case 'RUB':
                // $presupuesto_rublos["costo_facultad"]=intval($facultad->costo_facultad);
                // $presupuesto_dolares["costo_facultad"]=intval($facultad->costo_facultad/$ratioRUB->ratio);
                $presupuesto_local["costo_facultad"]=intval(($facultad->costo_facultad/$ratioRUB->ratio)*$ratioLOCAL->ratio);
        }
        //COSTO DEL PROGRAMA
        $programa = DB::connection('mysql2')->table('programa')
                                        ->join('costo','programa.id','=','costo.idprograma')
                                        ->select('programa.iduniversidad as presu_id','programa.idtipoestudio','programa.ididioma','costo.costo_d as costo_facultad','costo.divisa','programa.descripcion')
                                        ->where('programa.id',$id_programa)->first();
        
        $mostrar["columna"] = ($programa->idtipoestudio==5) ? false : true;//MUESTRA LA SEGUNDA COLUMNA DEL PRESUPUESTO
        if($programa->idtipoestudio==5){
            switch ($programa->descripcion) {
                case 'Facultad Preparatoria I':
                    $mostrar["caso_tabla"]="facultad1";
                    break;
                case 'Facultad Preparatoria II':
                    $mostrar["caso_tabla"]="facultad2";
                    break;
                case 'Facultad Preparatoria III':
                    $mostrar["caso_tabla"]="facultad3";
                    break;
            }
        }else{
            if($programa->ididioma==2){
                $mostrar["caso_tabla"]="carrera_ingles";
            }else{
                $mostrar["caso_tabla"]="carrera_ruso/español";
            }
        }
        switch ($programa->divisa) {
            case 'USD':
                // $presupuesto_rublos["costo_programa"]=intval($programa->costo_facultad*$ratioRUB->ratio);
                // $presupuesto_dolares["costo_programa"]=intval($programa->costo_facultad);
                $presupuesto_local["costo_programa"]=intval($programa->costo_facultad*$ratioLOCAL->ratio);
                break;
            case 'RUB':
                // $presupuesto_rublos["costo_programa"]=intval($programa->costo_facultad);
                // $presupuesto_dolares["costo_programa"]=intval($programa->costo_facultad/$ratioRUB->ratio);
                $presupuesto_local["costo_programa"]=intval(($programa->costo_facultad/$ratioRUB->ratio)*$ratioLOCAL->ratio);
        }
        // COSTO TOTAL EN RUBLOS
        // $costo_total_programas_rublos=$presupuesto_rublos["costo_programa"]+$presupuesto_rublos["costo_seguro"]+$presupuesto_rublos["costo_alimentacion"]+$presupuesto_rublos["costo_vivienda"];
        // $costo_total_facultad_rublos=$presupuesto_rublos["costo_facultad"]+$presupuesto_rublos["costo_seguro"]+$presupuesto_rublos["costo_alimentacion"]+$presupuesto_rublos["costo_vivienda"];
        
        // $presupuesto_rublos["total_programas"]=$costo_total_programas_rublos;
        // $presupuesto_rublos["total_facultad"]=$costo_total_facultad_rublos;
        
        // COSTO TOTAL EN DOLARES
        // $costo_total_programas_dolares=$presupuesto_dolares["costo_programa"]+$presupuesto_dolares["costo_seguro"]+$presupuesto_dolares["costo_alimentacion"]+$presupuesto_dolares["costo_vivienda"];
        // $costo_total_facultad_dolares=$presupuesto_dolares["costo_facultad"]+$presupuesto_dolares["costo_seguro"]+$presupuesto_dolares["costo_alimentacion"]+$presupuesto_dolares["costo_vivienda"];

        // $presupuesto_dolares["total_programas"]=$costo_total_programas_dolares;
        // $presupuesto_dolares["total_facultad"]=$costo_total_facultad_dolares;
        // COSTO TOTAL EN MONEDA LOCAL
        $costo_total_programas_local=$presupuesto_local["costo_programa"]+$presupuesto_local["costo_seguro"]+$presupuesto_local["costo_alimentacion"]+$presupuesto_local["costo_vivienda"];
        $costo_total_facultad_local=$presupuesto_local["costo_facultad"]+$presupuesto_local["costo_seguro"]+$presupuesto_local["costo_alimentacion"]+$presupuesto_local["costo_vivienda"];
        $presupuesto_local["total_programas"]=$costo_total_programas_local;
        $presupuesto_local["total_facultad"]=$costo_total_facultad_local;
        
        // COSTOS PARA CALENDARIO
        $costos_calendario=array();
        $costos_calendario["cal_costo_facultad"]=$presupuesto_local["costo_facultad"];
        $costos_calendario["cal_costo_programa"]=$presupuesto_local["costo_programa"]/2;
        $costos_calendario["cal_costo_seguro"]=$presupuesto_local["costo_seguro"];
        $costos_calendario["cal_costo_alimentacion"]=intval($presupuesto_local["costo_alimentacion"]/12);
        $costos_calendario["cal_costo_vivienda"]=intval($presupuesto_local["costo_vivienda"]/12);
        $costos_calendario["suma1"]=$costos_calendario["cal_costo_facultad"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_alimentacion"];
        $costos_calendario["suma2"]=$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_alimentacion"];
        $costos_calendario["suma3"]=$costos_calendario["cal_costo_programa"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_alimentacion"];
        $costos_calendario["suma4"]=$costos_calendario["cal_costo_seguro"]+$costos_calendario["cal_costo_vivienda"]+$costos_calendario["cal_costo_alimentacion"];
        $costos_calendario["suma5"]=$costos_calendario["cal_costo_facultad"]+$costos_calendario["cal_costo_vivienda"];
        $costos_calendario["año"]=date("Y");
        //INFO GENERAL DEL PROGRAMA
        $info_general=DB::connection('mysql2')->table('programa')
        ->join('tipo_estudio', 'programa.idtipoestudio', '=', 'tipo_estudio.id')
        ->join('rama', 'programa.idrama', '=', 'rama.id')
        ->join('universidad', 'programa.iduniversidad', '=', 'universidad.id')
        ->join('idioma', 'programa.ididioma', '=', 'idioma.id')
        ->join('ciudad', 'universidad.idciudad', '=', 'ciudad.id')
        ->join('perfil', 'perfil.id', '=', 'programa.idperfil')
        ->select(['programa.id AS id_programa',
        'programa.descripcion AS descripcion_programa',
        'programa.codigo AS codigo_programa',
        'programa.duracion AS duracion_programa',
        'programa.beca AS beca',
        'tipo_estudio.descripcion AS tipo_descripcion',
        'rama.descripcion AS rama',
        'universidad.id AS id_universidad',
        'universidad.nombre AS universidad',
        'universidad.url AS url',
        'universidad.abrev AS abreviatura',
        'universidad.nomImage AS nomImage',
        'ciudad.nombre AS ciudad',
        'idioma.descripcion AS idioma',
        'perfil.descripcion AS perfil',
        ])->where('programa.id',$id_programa)->first();
        $fecha = Carbon::parse(Carbon::now(), 'UTC');
        $fecha_proforma = $fecha->isoFormat('D [de] MMMM [del] YYYY, h:mm:ss a');
        //SETEAR VALORES;
        $obj=new Programas($info_general);
        $obj->info_general=$info_general;
        // $obj->presupuesto_rublos=$presupuesto_rublos;
        // $obj->presupuesto_dolares=$presupuesto_dolares;
        $obj->presupuesto_local=$presupuesto_local;
        $obj->fecha_proforma=$fecha_proforma;
        $obj->costos_calendario=$costos_calendario;
        $obj->mostrar=$mostrar;
        $obj->divisa=$divisa;
        $obj->view();
        return Excel::download($obj,'Detail.xlsx');
    }
}
