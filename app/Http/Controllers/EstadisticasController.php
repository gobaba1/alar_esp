<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class EstadisticasController extends Controller
{
    public function index()
    {
        $users = DB::table('users')
        ->join('model_has_roles', 'model_id', '=', 'users.id')
        ->where('role_id', 1)
        ->select('users.*')
        ->get();
        $exists = Storage::disk('images')->exists(auth()->id().'.jpg');

        return view('estadisticas', [
            'users'                 => $users,
            'foto_perfil'           => $exists

        ]);
    }
    public function tab1()
    {
        $arrayPais = [];
        $arrayPaisNull = [];
        $data = DB::table('perfiles')
                    ->select(DB::raw('count(*) as contador_pais, idPais'), 'perfiles.idPais')
                    ->where('idPais','<>',null)
                    ->groupBy('idPais')
                    ->get();

        foreach ($data as $idPais ) {
            // echo $idPais->idPais;
            $pais = DB::table('pais')
            ->select('nombre')
            ->where('id',$idPais->idPais)
            ->first();
            array_push($arrayPais, $pais->nombre);
        }
        $data_null = DB::table('perfiles')
                    ->select(DB::raw('count(*) as contador_pais, idPais'), 'perfiles.idPais')
                    ->where('idPais','=',null)
                    ->groupBy('idPais')
                    ->get();
        foreach ($data_null as $idPais_null) {
            $null = 'null';
            array_push($arrayPaisNull, $null);
        }
        return json_encode(['data' => $data, 'paises' => $arrayPais, 'data_null' => $data_null, 'sin_pais_registrado' => $arrayPaisNull]);
    }
    public function tab2()
    {
        $contador0 = 0;
        $contador1 = 0;
        $contador2 = 0;
        $contador3 = 0;

        $data = DB::table('form_inscripcions')
                    ->select(DB::raw('count(*) as contador, facultad_necesita'), 'form_inscripcions.facultad_necesita')
                    ->groupBy('facultad_necesita')
                    ->get();
        // dd($data);
        foreach ($data as $facultad_necesita => $value) {
            // print_r($value->facultad_necesita.'-----');
            switch ($value->facultad_necesita) {
                case '0':                    $contador0++;                    break;
                case '1':                    $contador1++;                    break;
                case '2':                    $contador2++;                    break;
                case '':                     $contador3++;                    break;
            }
        }
        return json_encode(['data' => $data, 'contador0' => $contador0, 'contador1' => $contador1, 'contador2' => $contador2, 'contador3' => $contador3,]);
    }
    public function tab3(Request $request)
    {
        // dd($request->idioma);
        $table = DB::table('form_inscripcions');

        if ($request->idioma !== null) {
            $table->where('facultad_necesita', $request->idioma);
        }
        $data = $table->select(DB::raw('count(*) as contador, vacante_estudio'), 'form_inscripcions.vacante_estudio')
                    ->groupBy('vacante_estudio')
                    ->get();

        return json_encode(['data' => $data]);
    }
    public function tab4()
    {
        $anio_actual = new \DateTime();
        //ESPECIALIDAD MEDICA
        $EM_collectionEdadesAll = collect([]);
        $data_esp_medicas = DB::table('perfiles')
                            ->select('perfiles.fecNacimiento', 'form_inscripcions.vacante_estudio')
                            ->join('form_inscripcions', 'form_inscripcions.user_id', '=', 'perfiles.user_id')
                            ->where('perfiles.fecNacimiento','<>',null)
                            ->where('form_inscripcions.vacante_estudio','<>',null)
                            ->where('form_inscripcions.vacante_estudio','=','Especialidad Médica')
                            ->orderBy('fecNacimiento', 'asc')
                            ->get();
            foreach ($data_esp_medicas as $anios) {
                $fechaCumpleanios = new \DateTime($anios->fecNacimiento);
                $diff = $anio_actual->diff($fechaCumpleanios);
                $edad = $diff->y;
                $EM_collectionEdadesAll->push(['edades' => $edad, 'especialidad' => $anios->vacante_estudio]);
            }
            // 18 -25
            $EM_collectionEdades_18_25 = $EM_collectionEdadesAll->whereBetween('edades', [18, 25]);
            // 26-35
            $EM_collectionEdades_26_35 = $EM_collectionEdadesAll->whereBetween('edades', [26, 35]);
            // 36+
            $EM_collectionEdades_35_mas = $EM_collectionEdadesAll->where('edades','>',35);

        //CARRERAS
        $C_collectionEdadesAll = collect([]);
        $data_esp_carreras = DB::table('perfiles')
                            ->select('perfiles.fecNacimiento', 'form_inscripcions.vacante_estudio')
                            ->join('form_inscripcions', 'form_inscripcions.user_id', '=', 'perfiles.user_id')
                            ->where('perfiles.fecNacimiento','<>',null)
                            ->where('form_inscripcions.vacante_estudio','<>',null)
                            ->where('form_inscripcions.vacante_estudio','=','Maestría')
                            ->orderBy('fecNacimiento', 'asc')
                            ->get();
            foreach ($data_esp_carreras as $anios) {
                $fechaCumpleanios = new \DateTime($anios->fecNacimiento);
                $diff = $anio_actual->diff($fechaCumpleanios);
                $edad = $diff->y;
                $C_collectionEdadesAll->push(['edades' => $edad, 'especialidad' => $anios->vacante_estudio]);
            }
            // 18 -25
            $C_collectionEdades_18_25 = $C_collectionEdadesAll->whereBetween('edades', [18, 25]);
            // 26-35
            $C_collectionEdades_26_35 = $C_collectionEdadesAll->whereBetween('edades', [26, 35]);
            // 36+
            $C_collectionEdades_35_mas = $C_collectionEdadesAll->where('edades','>',35);

        return json_encode([
            'data' => [
                //ESPECIALIDAD MEDICA
                'em_18_25' => $EM_collectionEdades_18_25->count(),
                'em_26-35' => $EM_collectionEdades_26_35->count(),
                'em_35_mas' => $EM_collectionEdades_35_mas->count(),

                //CARRERAS
                'c_18_25' => $C_collectionEdades_18_25->count(),
                'c_26-35' => $C_collectionEdades_26_35->count(),
                'c_35_mas' => $C_collectionEdades_35_mas->count(),
            ]
        ]);
    }
    public function tab5(Request $request)
    {
        // dd($request->tipo_estudio);
        $tipo_estudio_select = DB::connection('mysql2')->table('tipo_estudio')->select('tipo_estudio.id','tipo_estudio.descripcion')->get();
        $ramas = DB::connection('mysql2')->table('rama')->select('rama.id','rama.descripcion')->get();
        $ciudades=DB::connection('mysql2')->table('ciudad')->select('ciudad.*')->get();
        $paises= DB::table('estadisticas_programas_buscador')
                    ->select(DB::raw('count(*) as contador, pais_procedencia'), 'estadisticas_programas_buscador.pais_procedencia')
                    ->groupBy('pais_procedencia')
                    ->get();
        $coleccion = collect([]);
        $tabla = DB::table('estadisticas_programas_buscador');
        if ($request->pais == null){
            if ($request->tipo_estudio !== null) {
                $tabla->where('tipo_estudio', $request->tipo_estudio);
                if($request->rama == null){
                    $data = $tabla->select(DB::raw('count(*) as contador, rama_estudio'), 'rama_estudio')
                                    ->where ('rama_estudio', '<>' , 0) // AQUI
                                    ->groupBy('rama_estudio')
                                    ->get();
                    foreach ($data as $rama_estudio) {
                        $nombre_rama_estudio = DB::connection('mysql2')->table('rama')
                                                    ->where('id', $rama_estudio->rama_estudio)
                                                    ->first();
                        $coleccion->push(['label' => $nombre_rama_estudio->descripcion, 'contador' => $rama_estudio->contador]);
                    }
                }else if($request->rama !== null){
                        $tabla->where ('rama_estudio', '=' , $request->rama); // AQUI
                    $data = $tabla->select(DB::raw('count(*) as contador, perfil_estudio'), 'perfil_estudio')
                                    ->where ('perfil_estudio', '<>' , 0) // AQUI
                                    ->groupBy('perfil_estudio')
                                    ->get();
                    foreach ($data as $perfil_estudio) {
                        $nombre_perfil_estudio = DB::connection('mysql2')->table('perfil')
                                                    ->where('id', $perfil_estudio->perfil_estudio)
                                                    ->first();
                        $coleccion->push(['label' => $nombre_perfil_estudio->descripcion, 'contador' => $perfil_estudio->contador]);
                    }
                }
            }else if($request->tipo_estudio == null){
                $data = $tabla->select(DB::raw('count(*) as contador, tipo_estudio'), 'estadisticas_programas_buscador.tipo_estudio')
                                ->groupBy('tipo_estudio')
                                ->get();
                foreach ($data as $tipo_estudio) {
                    $nombre_tipo_estudio = DB::connection('mysql2')->table('tipo_estudio')
                                                ->where('id', $tipo_estudio->tipo_estudio)
                                                ->first();
                    $coleccion->push(['label' => $nombre_tipo_estudio->descripcion, 'contador' => $tipo_estudio->contador]);
                }
            }
        }else if ($request->pais !== null) {
            $tabla->where('pais_procedencia', $request->pais);
            if ($request->tipo_estudio !== null) {
                $tabla->where('tipo_estudio', $request->tipo_estudio);
                if($request->rama == null){
                    $data = $tabla->select(DB::raw('count(*) as contador, rama_estudio'), 'rama_estudio')
                                    ->where ('rama_estudio', '<>' , 0) // AQUI
                                    ->groupBy('rama_estudio')
                                    ->get();
                    foreach ($data as $rama_estudio) {
                        $nombre_rama_estudio = DB::connection('mysql2')->table('rama')
                                                    ->where('id', $rama_estudio->rama_estudio)
                                                    ->first();
                        $coleccion->push(['label' => $nombre_rama_estudio->descripcion, 'contador' => $rama_estudio->contador]);
                    }
                }else if($request->rama !== null){
                        $tabla->where ('rama_estudio', '=' , $request->rama); // AQUI
                    $data = $tabla->select(DB::raw('count(*) as contador, perfil_estudio'), 'perfil_estudio')
                                    ->where ('perfil_estudio', '<>' , 0) // AQUI
                                    ->groupBy('perfil_estudio')
                                    ->get();
                    foreach ($data as $perfil_estudio) {
                        $nombre_perfil_estudio = DB::connection('mysql2')->table('perfil')
                                                    ->where('id', $perfil_estudio->perfil_estudio)
                                                    ->first();
                        $coleccion->push(['label' => $nombre_perfil_estudio->descripcion, 'contador' => $perfil_estudio->contador]);
                    }
                }
            } else if($request->tipo_estudio == null){
                $data = $tabla->select(DB::raw('count(*) as contador, tipo_estudio'), 'estadisticas_programas_buscador.tipo_estudio')
                                ->groupBy('tipo_estudio')
                                ->get();
                foreach ($data as $tipo_estudio) {
                    $nombre_tipo_estudio = DB::connection('mysql2')->table('tipo_estudio')
                                                ->where('id', $tipo_estudio->tipo_estudio)
                                                ->first();
                    $coleccion->push(['label' => $nombre_tipo_estudio->descripcion, 'contador' => $tipo_estudio->contador]);
                }
            }
        }

        return json_encode([
            'data' => $coleccion,
            'tipo_estudio'=>$tipo_estudio_select,
            'ramas'=>$ramas,
            'ciudades'=>$ciudades,
            'paises'=>$paises
        ]);
    }
    public function tab6(Request $request)
    {
        $coleccion = collect([]); // DATA GRAFICO
        $coleccion2 = collect([]); // SELECT FILTRO
        $paises= DB::table('estadisticas_programas_interacccion')
                ->select(DB::raw('count(*) as contador, pais_procedencia'), 'estadisticas_programas_interacccion.pais_procedencia')
                ->groupBy('pais_procedencia')
                ->get();
        if ($request->pais == null) {
            $data = DB::table('estadisticas_programas_interacccion')
                        ->select(DB::raw('count(*) as contador, id_programa'), 'id_programa')
                        ->groupBy('id_programa')
                        ->orderBy('contador')
                        ->limit(10)
                        ->get();
            foreach ($data as $programa) {
                // print_r($programa);
                $nombre_programa = DB::connection('mysql2')->table('programa')
                                        ->where('id', $programa->id_programa)
                                        ->first();
                $coleccion2->push(['label' => $nombre_programa->descripcion, 'contador' => $programa->contador, 'id_programa'=> $programa->id_programa]);
            }
            if ($request->programa == null) {
                $data = DB::table('estadisticas_programas_interacccion')
                            ->select(DB::raw('count(*) as contador, id_programa'), 'id_programa')
                            ->groupBy('id_programa')
                            ->orderBy('contador')
                            ->limit(10)
                            ->get();
                foreach ($data as $programa) {
                    // print_r($programa);
                    $nombre_programa = DB::connection('mysql2')->table('programa')
                                            ->where('id', $programa->id_programa)
                                            ->first();
                    $coleccion->push(['label' => $nombre_programa->descripcion, 'contador' => $programa->contador, 'id_programa'=> $programa->id_programa]);
                }
            } else if ($request->programa !== null){
                $data = DB::table('estadisticas_programas_interacccion')
                            ->select(DB::raw('count(*) as contador, tipo'), 'tipo')
                            ->where('id_programa', $request->programa)
                            ->groupBy('tipo')
                            ->get();
                foreach ($data as $programa) {
                    $coleccion->push(['label' => $programa->tipo, 'contador' => $programa->contador]);
                }
            }
        } else if ($request->pais !== null) {
            $data = DB::table('estadisticas_programas_interacccion')
                        ->select(DB::raw('count(*) as contador, id_programa'), 'id_programa')
                        ->where('pais_procedencia', $request->pais)
                        ->groupBy('id_programa')
                        ->orderBy('contador')
                        ->limit(10)
                        ->get();
            foreach ($data as $programa) {
                // print_r($programa);
                $nombre_programa = DB::connection('mysql2')->table('programa')
                                        ->where('id', $programa->id_programa)
                                        ->first();
                $coleccion2->push(['label' => $nombre_programa->descripcion, 'contador' => $programa->contador, 'id_programa'=> $programa->id_programa]);
            }
            if ($request->programa == null) {
                $data = DB::table('estadisticas_programas_interacccion')
                            ->select(DB::raw('count(*) as contador, id_programa'), 'id_programa')
                            ->where('pais_procedencia', $request->pais)
                            ->groupBy('id_programa')
                            ->orderBy('contador')
                            ->limit(10)
                            ->get();
                foreach ($data as $programa) {
                    // print_r($programa);
                    $nombre_programa = DB::connection('mysql2')->table('programa')
                                            ->where('id', $programa->id_programa)
                                            ->first();
                    $coleccion->push(['label' => $nombre_programa->descripcion, 'contador' => $programa->contador, 'id_programa'=> $programa->id_programa]);
                }
            } else if ($request->programa !== null){
                $data = DB::table('estadisticas_programas_interacccion')
                            ->select(DB::raw('count(*) as contador, tipo'), 'tipo')
                            ->where('id_programa', $request->programa)
                            ->where('pais_procedencia', $request->pais)
                            ->groupBy('tipo')
                            ->get();
                foreach ($data as $programa) {
                    $coleccion->push(['label' => $programa->tipo, 'contador' => $programa->contador]);
                }
            }
        }




        return json_encode([
            'data'    =>   $coleccion,
            'select'  =>   $coleccion2,
            'paises'  =>   $paises,
        ]);
    }
}
