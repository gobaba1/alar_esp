<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class programa extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'programa';


    protected $fillable = [
        'id', 'descripcion', 'codigo', 'duracion', 'idtipoestudio', 'idrama', 'iduniversidad', 'fecha_ini', 'fecha_fin', 'fecha_inscrip', 'estado_inscrip', 'ididioma', 'creditos_ects', 'beca', 'ideperfil'
    ];
}
