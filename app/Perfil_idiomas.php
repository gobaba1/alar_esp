<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil_idiomas extends Model
{
    protected $fillable = ['hablado','escrito','lectura','user_id','idioma_id'];
}
