<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Validaciones_archivos extends Model
{
    protected $fillable = ['nameArchivo','type','state','user_id'];
}
