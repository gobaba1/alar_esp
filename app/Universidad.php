<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Universidad extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'universidad';

    protected $fillable = [
        'nombre', 'estado', 'abrev', 'url', 'idciudad', 'nomImage'
    ];
}
