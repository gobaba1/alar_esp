<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
class ImportPrograma  implements ToCollection
{
    private $data;
    private $errores =array();
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function collection(Collection $rows)
    {
        // $rows [$i][0] ->Tipo de estudio
        // $rows [$i][1] ->Rama
        // $rows [$i][2] ->Perfil
        // $rows [$i][3] ->Codigo
        // $rows [$i][4] ->Programa
        // $rows [$i][5] ->Duracion
        // $rows [$i][6] ->Costo
        // $rows [$i][7] ->Idioma
        // $rows [$i][8] ->Beca
        for ($i=1; $i < count($rows); $i++) {
            if(!empty($rows[$i][0])){
                //ID TIPO ESTUDIO
                $id_tipo_estudio = DB::connection('mysql2')
                                    ->table('tipo_estudio')
                                    ->select('tipo_estudio.id')
                                    ->where('descripcion',$rows [$i][0])->first();
                if($id_tipo_estudio==null){
                    $error="Error en la celda A".($i+1);
                    array_push($this->errores, $error);
                }
                //ID RAMA
                $id_rama         = DB::connection('mysql2')
                                    ->table('rama')
                                    ->select('rama.id')
                                    ->where('descripcion',$rows [$i][1])->first();
                if($id_rama==null){
                    $error="Error en la celda B".($i+1);
                    array_push($this->errores, $error);
                }
                //ID IDIOMA
                $id_idioma      = DB::connection('mysql2')
                                    ->table('idioma')
                                    ->select('idioma.id')
                                    ->where('descripcion',$rows [$i][7])->first();
                if($id_idioma==null){
                    $error="Error en la celda H".($i+1);
                    array_push($this->errores, $error);
                }
                if($id_tipo_estudio==null || $id_rama==null || $id_idioma==null){
                    continue;
                }
                // ID PERFIL
                $id_perfil;
                $verificar_id   = DB::connection('mysql2')
                                    ->table('perfil')
                                    ->select('perfil.id')
                                    ->where('descripcion',$rows [$i][2])
                                    ->where('idrama',intval($id_rama->id))
                                    ->first();
                
                if($verificar_id==null){
                    $id_perfil=DB::connection('mysql2')
                        ->table('perfil')->insertGetId([
                                'descripcion'   => $rows [$i][2],
                                'idrama'        => $id_rama->id
                        ]);
                }else{
                    $id_perfil=$verificar_id->id;
                }
                //CODIGO
                $codigo = ($rows [$i][3]==null) ? "" : $rows [$i][3];
                // INTRODUCIR NUEVO PROGRAMA
                $id_programa=DB::connection('mysql2')
                                ->table('programa')->insertGetId([
                                    'descripcion'       => $rows [$i][4] ,
                                    'codigo'            => $codigo,
                                    'duracion'          => $rows [$i][5],
                                    'idtipoestudio'     => $id_tipo_estudio->id,
                                    "idrama"            => $id_rama->id ,
                                    "iduniversidad"     => $this->data['id_universidad'],
                                    "ididioma"          =>$id_idioma->id,
                                    "beca"              =>$rows [$i][8],
                                    "idperfil"          =>$id_perfil,
                            ]);
                // INTRODUCIR EN LA TABLA DE COSTOS
                DB::connection('mysql2')
                    ->table('costo')->insert([
                    'idprograma'  => $id_programa,
                    'idtipocosto' => "1",
                    'costo_d'     => $rows [$i][6],
                    'divisa'      => $this->data['divisa']
                ]);
            }
        }
    }
    public function getErrores()
    {
        return $this->errores;
    }
}
