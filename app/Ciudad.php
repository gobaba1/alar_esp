<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'ciudad';

    protected $fillable = [
        'nombre'
    ];
}
