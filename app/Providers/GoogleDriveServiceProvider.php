<?php

namespace App\Providers;
use App\User;
use App\Notificaciones AS ModelNotificaciones;
use Illuminate\Http\Request;
use App\Events\Notificaciones;

use App\Validaciones_archivos;
use League\Flysystem\Filesystem;
use Illuminate\Support\Facades\DB;
use App\Events\UpdateDocNotifications;
use Illuminate\Support\ServiceProvider;
use Hypweb\Flysystem\GoogleDrive\GoogleDriveAdapter;

class GoogleDriveServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(){
        \Storage::extend("google",function($app ,$config){
            // CREAR LA CARPETA PAIS Y LA DEL ALUMNO EN GOOGLE DRIVE
                
                $data_perfil=DB::table('perfiles')->join('pais','perfiles.idPais','=','pais.id')
                ->select('perfiles.folder_id','perfiles.idPais','pais.nombre')
                ->where('user_id',auth()->id())->first();
                $data_form_inscipciones=DB::table('form_inscripcions')->select('form_inscripcions.facultad_mes','form_inscripcions.facultad_anio')
                ->where('user_id',auth()->id())->first();
                $data_carpeta_pais=DB::table('carpetaspaisesdrive')->select('carpetaspaisesdrive.*')
                ->join('carpetastiempodrive','carpetastiempodrive.id','=','carpetaspaisesdrive.id_carpeta_tiempo')
                ->where('carpetaspaisesdrive.id_pais',$data_perfil->idPais)
                ->where('carpetastiempodrive.tiempo',$data_form_inscipciones->facultad_mes."_".$data_form_inscipciones->facultad_anio)
                ->first();
                $id_carpeta=DB::table('carpetastiempodrive')->select('carpetastiempodrive.id_carpeta','carpetastiempodrive.id')
                    ->where('tiempo',$data_form_inscipciones->facultad_mes."_".$data_form_inscipciones->facultad_anio)->first();
                //CREAR LA CARPETA DEL AÑO SI ES QUE AUN NO HA SIDO CREADA
                if($id_carpeta==null){
                    $client = new \Google_Client;
                    $client->setClientId("220264139639-lnpvtufq0vc9b80n7u9t63g891iqnb1e.apps.googleusercontent.com");
                    $client->setClientSecret("QW1LFlwBfPv5hknAFABfVyj0");
                    $client->refreshToken("1//03Zfh-n4x_gIdCgYIARAAGAMSNwF-L9Irj9PFzCazU_cRrZ7Tl4nDwahYnBm-ncBM0J1_XpvFtNsxhSjoDe2Vd3602ptAMw8IT70");
                    
                    $service = new \Google_Service_Drive($client);
                    $fileMetadata = new \Google_Service_Drive_DriveFile([
                        'name'     =>$data_form_inscipciones->facultad_mes."_".$data_form_inscipciones->facultad_anio,
                        'mimeType' => 'application/vnd.google-apps.folder',
                        'parents' => array("1j8832aRXR0b7XmS_NoqKhQxL8VGnRHDH"),
                    ]);
                    $folder = $service->files->create($fileMetadata, ['fields' => 'id']);
                    DB::table('carpetastiempodrive')->insert([
                        ['id_carpeta' => $folder->id, 'tiempo' => $data_form_inscipciones->facultad_mes."_".$data_form_inscipciones->facultad_anio]
                    ]);
                }
                //CREAR LA CARPETA DEL PAIS SI ES QUE AUN NO HA SIDO CREADA
                if($data_carpeta_pais == null){
                    $id_carpeta=DB::table('carpetastiempodrive')->select('carpetastiempodrive.id_carpeta','carpetastiempodrive.id')
                    ->where('tiempo',$data_form_inscipciones->facultad_mes."_".$data_form_inscipciones->facultad_anio)
                    ->first();
                    $client = new \Google_Client;
                    $client->setClientId("220264139639-lnpvtufq0vc9b80n7u9t63g891iqnb1e.apps.googleusercontent.com");
                    $client->setClientSecret("QW1LFlwBfPv5hknAFABfVyj0");
                    $client->refreshToken("1//03Zfh-n4x_gIdCgYIARAAGAMSNwF-L9Irj9PFzCazU_cRrZ7Tl4nDwahYnBm-ncBM0J1_XpvFtNsxhSjoDe2Vd3602ptAMw8IT70");
                    
                    $service = new \Google_Service_Drive($client);
                    $fileMetadata = new \Google_Service_Drive_DriveFile([
                        'name'     =>$data_perfil->nombre,
                        'mimeType' => 'application/vnd.google-apps.folder',
                        'parents' => array($id_carpeta->id_carpeta),
                    ]);
                    $folder = $service->files->create($fileMetadata, ['fields' => 'id']);
                    DB::table('carpetaspaisesdrive')->insert([
                        ['nombre' => $data_perfil->nombre, 'id_carpeta' => $folder->id,'id_pais' => $data_perfil->idPais,'id_carpeta_tiempo' => $id_carpeta->id]
                    ]);
                }
                //FIN CREAR LA CARPETA DEL PAIS SI ES QUE AUN NO HA SIDO CREADA
                // TRAER ID_CARPETA EN LA QUE SE ALMACENARA EL USUARIO
                $id_carpeta_pais_tiempo=DB::table('carpetaspaisesdrive')
                                        ->join('carpetastiempodrive','carpetastiempodrive.id','=','carpetaspaisesdrive.id_carpeta_tiempo')
                                        ->select('carpetaspaisesdrive.id_carpeta')
                                        ->where('carpetastiempodrive.tiempo',$data_form_inscipciones->facultad_mes."_".$data_form_inscipciones->facultad_anio)
                                        ->where('id_pais',$data_perfil->idPais)
                                        ->first();
                // CREAR LA CARPETA DEL USUARIO SI ES AUN NO HA SIDO CREADA
                if($data_perfil->folder_id==null){
                    $data_personal=DB::table('users')
                                ->select('users.name','users.lastname')
                                ->where('id',auth()->id())->first();
                    $client = new \Google_Client;
                    $client->setClientId("220264139639-lnpvtufq0vc9b80n7u9t63g891iqnb1e.apps.googleusercontent.com");
                    $client->setClientSecret("QW1LFlwBfPv5hknAFABfVyj0");
                    $client->refreshToken("1//03Zfh-n4x_gIdCgYIARAAGAMSNwF-L9Irj9PFzCazU_cRrZ7Tl4nDwahYnBm-ncBM0J1_XpvFtNsxhSjoDe2Vd3602ptAMw8IT70");
                    
                    $service = new \Google_Service_Drive($client);
                    $fileMetadata = new \Google_Service_Drive_DriveFile([
                        'name'     =>$data_personal->name." ".$data_personal->lastname,
                        'mimeType' => 'application/vnd.google-apps.folder',
                        'parents' => array($id_carpeta_pais_tiempo->id_carpeta),
                    ]);
                    $folder = $service->files->create($fileMetadata, ['fields' => 'id']);
                    //GUARDAR ID EN CARPETA EN PERFIL
                    DB::table('perfiles')
                            ->where('user_id',auth()->id())
                            ->update(['folder_id' => $folder->id]);
                    // GUARDAR EN LA CARPETA DEL DRIVE 
                    $request = $this->app->request;
                    $client = new \Google_Client;
                    $client->setClientId($config['clientId']);
                    $client->setClientSecret($config['clientSecret']);
                    $client->refreshToken($config['refreshToken']);
                    $service = new \Google_Service_Drive($client);
                    $adapter = new GoogleDriveAdapter($service,$folder->id);
                }else{
                    // GUARDAR EN LA CARPETA DEL DRIVE 
                    $request = $this->app->request;
                    $client = new \Google_Client;
                    $client->setClientId($config['clientId']);
                    $client->setClientSecret($config['clientSecret']);
                    $client->refreshToken($config['refreshToken']);
                    $service = new \Google_Service_Drive($client);
                    $adapter = new GoogleDriveAdapter($service,$data_perfil->folder_id);
                }
            
            $data=DB::table('validaciones_archivos')
            ->where('nameArchivo', $request->file_name)->get();
            if(count($data)>0){
                DB::table('validaciones_archivos')
                ->where('nameArchivo', $request->file_name)
                ->where('user_id', auth()->id())
                ->update(['state' => 1]);
            }else{
                Validaciones_archivos::create([
                    'user_id'       => auth()->id(),
                    'nameArchivo'   =>$request->file_name,
                    'state'         =>'1',
                    'type'          =>$request->file_type,
                ]);
            }

            $notificacion = new ModelNotificaciones();
            $notificacion->user1 = auth()->id();    //usuario que envia;
            $notificacion->user2 = 1;               //usuario que recibe;
            $notificacion->razon_constancia = 'Ha subido su '.$request->file_name ;
            $notificacion->sustento = $request->file_name;
            $notificacion->save();
            broadcast( new UpdateDocNotifications('1', 'Ha subido su : ', (string)$request->file_name , auth()->id()) );

            return new Filesystem($adapter);
        });
    }
}
