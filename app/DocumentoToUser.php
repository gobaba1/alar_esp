<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoToUser extends Model
{
    protected $table = 'documentos_to_user';
    protected $fillable = [
        'id_admin', 'id_alumno', 'url_drive', 'type','state','mensaje'
    ];
}
