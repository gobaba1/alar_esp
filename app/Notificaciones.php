<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notificaciones extends Model
{
    protected $fillable = [
        'user1', 'user2', 'razon_constancia','sustento'
    ];
}
