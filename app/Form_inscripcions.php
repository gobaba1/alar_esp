<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form_inscripcions extends Model
{
    protected $fillable = ['user_id',
                            'facultad_necesita',
                            'facultad_mes',
                            'facultad_anio',
                            'facultad_universidad',
                            'vacante_estudio',
                            'vacante_universidad',
                            'vacante_rama',
                            'vancante_gradoActual',
                            'vacante_financia',
                            'madre_nombre',
                            'madre_correo',
                            'padre_nombre',
                            'padre_correo',
                            'padres_direccion',
                            'padres_telefono',
                            'padres_celular',
                            'respon_nombre',
                            'respon_correo',
                            'respon_telefono',
                            'respon_celular',
                            'apoder_nombre',
                            'apoder_dni'];
}
