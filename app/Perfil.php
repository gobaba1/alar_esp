<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'perfil';

    protected $fillable = [
        'descripcion', 'id', 'idrama'
    ];
}
