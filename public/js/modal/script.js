$( "#showForm" ).click(function() {
  $(".titleLogin").html("Registrate con tus redes sociales");
  $(".subTitleLogin").html("Ó llena el formulario");
  $(".backStart").css({"display":"block"});
  $("#formLogin").css({"display":"none"});
  $("#register").css({"display":"block"}); 
  $(".btnRegistro").css({"display":"block"}); 
  $(".login").css({"display":"none"});                             
});

$("#backStart").click(function() {
  $(".titleLogin").css({"display":"block"});        
  $(".titleLogin").html("Ingresa con tus redes sociales");
  $(".subTitleLogin").html(" Ó ingresa con tu usuario");
  $(".subTitleLogin").css({"display":"block"});        
  $(".backStart").css({"display":"none"});
  $("#formLogin").css({"display":"block"});     
  $("#register").css({"display":"none"});  
  $(".btn-redes").css({"display":"block"});    
  $(".btnRegistro").css({"display":"none"});    
});

$("#showFormRegister").click(function() {
  $(".login").css({"display":"block"});  
  $(".btn-redes").css({"display":"none"});  
  $(".titleLogin").css({"display":"none"});        
  $(".subTitleLogin").css({"display":"none"});        
});