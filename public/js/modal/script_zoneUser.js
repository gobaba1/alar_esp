$("#showForm").click(function() {
    $(".titleLogin").html("Registrate con tus redes sociales");
    $(".subTitleLogin").html("Ó llena el formulario");
    $(".backStart").css({ "display": "block" });
    $("#formLogin").css({ "display": "none" });
    $("#register").css({ "display": "block" });
    $(".btnRegistro").css({ "display": "block" });
    $(".login").css({ "display": "none" });
});

$("#backStart").click(function() {
    $(".titleLogin").css({ "display": "block" });
    $(".titleLogin").html("Ingresa con tus redes sociales");
    $(".subTitleLogin").html(" Ó ingresa con tu usuario");
    $(".subTitleLogin").css({ "display": "block" });
    $(".backStart").css({ "display": "none" });
    $("#formLogin").css({ "display": "block" });
    $("#register").css({ "display": "none" });
    $(".btn-redes").css({ "display": "block" });
    $(".btnRegistro").css({ "display": "none" });
});
$("#showFormRegister").click(function() {
    $(".login").css({ "display": "block" });
    $(".btn-redes").css({ "display": "none" });
    $(".titleLogin").css({ "display": "none" });
    $(".subTitleLogin").css({ "display": "none" });
});
// LOGUEO CON FB

window.fbAsyncInit = function() {
    FB.init({
        appId: '2347857862211034',
        autoLogAppEvents: true,
        xfbml: true,
        version: 'v5.0'
    });
};

function fb_login() {
    FB.login(function(response) {

        if (response.authResponse) {
            //console.log('Welcome!  Fetching your information.... ');
            //console.log(response); // dump complete info
            let access_token = response.authResponse.accessToken; //get access token
            let user_id = response.authResponse.userID; //get FB UID
            let user_email = response.email;
            //console.log(access_token+"/"+user_id+"/");
            FB.api('/me', function(response) {
                //console.log('Good to see you, ' + response.name + '.');
            });
            FB.api('/me', { fields: 'last_name,first_name,email,middle_name,picture' }, function(response) {
                let param = {
                    'first_name': response.first_name,
                    'middle_name': response.middle_name,
                    'lastname': response.last_name,
                    'email': response.email,
                    'picture': response.picture.data.url,
                    'social_id': user_id,
                    'oper': 'fb'

                };
                console.log(param);
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(param),
                    url: "https://www.universidades-rusia.com/programas/zoneUser/api/reg-vefUser.php",
                    success: function(response) {
                        let data = {
                            'email': response.message[0].email
                        }
                        $.ajax({
                            type: "POST",
                            contentType: "application/json",
                            data: JSON.stringify(data),
                            url: "zoneUser/api/autLogFb.php",
                            success: function(response) {
                                switch (response.message) {
                                    case 'email failed.':
                                        swal("El correo no se encuentra registrado", "", "error");
                                        break;
                                    case 'pass failed.':
                                        swal("La contraseña es incorrecta", "", "error");
                                        break;
                                    case 'unauthenticated email':
                                        swal("El email no esta autentificado", "Porfavor revisar su email y darle click al link que se le ha enviado", "error");
                                        break;
                                    case 'Successful login.':
                                        var d = new Date();
                                        d.setTime(d.getTime() + (1 * 24 * 60 * 60 * 1000));
                                        var expires = "expires=" + d.toUTCString();
                                        document.cookie = 'jwt' + "=" + response.jwt + ";" + expires + ";path=/";
                                        console.log(document.cookie);
                                        // document.cookie = 'id'+ "=" + response.id + ";" + expires + ";path=/";
                                        window.location.replace("../zonaUsuario/index.php");
                                        break;
                                    case 'Login exists':
                                        swal("El usuario ya se encuentra logueado", "Cambiar contraseña", "error");
                                        break;
                                }
                            }
                        });
                    }
                });

            });

        } else {
            //user hit cancel button
            console.log('User cancelled login or did not fully authorize.');

        }
    }, {
        scope: 'publish_stream,email'
    });
}
(function() {
    var e = document.createElement('script');
    e.src = document.location.protocol + '//connect.facebook.net/en_US/sdk.js';
    e.async = true;
    // document.getElementById('fb-root').appendChild(e);
}());
// FORMULARIO DE REGISTRO
$("#registrar").click(function(e) {
    e.preventDefault();
    let firstname = $("#name").val();
    let lastname = $("#lastname").val();
    let email = $("#mailUser").val();
    let password = $("#passUser").val();
    let data = {
        'firstname': firstname,
        'lastname': lastname,
        'email': email,
        'password': password,
        'oper': 'newUser'
    }
    $.ajax({
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(data),
        url: "https://www.universidades-rusia.com/programas/zoneUser/api/reg-vefUser.php",
        success: function(response) {
            console.log(response.message);
            if (response.message == "200") {
                Object.assign(data, { "idUser": response.idUser });
                $.ajax({
                    data: data,
                    type: "POST",
                    datatype: "json",
                    url: "zoneUser/sendMail.php",
                    success: function(response) {
                        console.log(response);
                        swal("Usuario Registrado!", "Se le ha enviado un correo para la activacion de su cuenta", "success");
                    }
                });
            } else {
                console.log("El correo ya se encuentra registrado");
                swal("El correo ya se encuentra registrado", "Escoja otro", "error");
            }
        }
    })
});

// //AUTENTIFICACION DE LOGIN
$("#btn_logueo").click(function(e) {
    e.preventDefault();
    let emailLog = $('#emailLog').val();
    let passLog = $('#passLog').val();
    let dataForm = {
        'email': emailLog,
        'pass': passLog,
    };
    // console.log(dataForm);

    $.ajax({
        type: "POST",
        url: "zoneUser/funciones/fn_login.php",
        data: dataForm,
        success: function(response) {
            let respuesta = JSON.parse(response);
            console.log(respuesta);

            if (respuesta.message == '1') {
                switch (respuesta.role) {
                    case 'USER_ROLE':
                        swal("Usuario verificado", "Espere unos segundos...", "success");
                        setTimeout(function() {
                            window.location.replace("../zonaUsuario/index.php");
                        }, 2000);
                        break;
                    case 'ADMIN_ROLE':
                        swal("Usuario verificado", "Espere unos segundos...", "success");
                        setTimeout(function() {
                            window.location.replace("../zonaUsuario/user_list.php");
                        }, 2000);
                        break;

                    default:
                        break;
                }
            }

        }
    });

});

// //AUTENTIFICACION DE LOGIN
// $("#auth").click(function(e){
//   e.preventDefault();
//   emailLog
//   passLog
//   let email=$("#emailLog").val();
//   let password=$("#passLog").val();
//   let data={
//       'email':email,
//       'password':password
//   }
//   $.ajax({
//     type: "POST",
//     contentType: "application/json",
//     data: JSON.stringify(data),
//     url: "zoneUser/api/authlogin.php",
//       success: function (response) {           
//         switch (response.message) {
//           case 'email failed.':
//           swal("El correo no se encuentra registrado","", "error");           
//             break;  
//           case 'pass failed.': 
//           swal("La contraseña es incorrecta","", "error");           
//           break;
//           case 'unauthenticated email':
//           swal("El email no esta autentificado","Porfavor revisar su email y darle click al link que se le ha enviado", "error");           
//           break;
//           case 'Successful login.':
//             var d = new Date();
//             d.setTime(d.getTime() + (1*24*60*60*1000));
//             var expires = "expires="+ d.toUTCString();
//             document.cookie = "jwt"+ "=" + response.jwt + ";" + expires + ";path=/";
//             console.log(document.cookie);-
//             // alert('hola');
//             window.location.replace("zonaUsuario/index");
//           break;
//           case 'Login exists':
//             swal("El usuario ya se encuentra logueado","Cambiar contraseña", "error");
//           break;

//         }
//       }   
//     });
// });