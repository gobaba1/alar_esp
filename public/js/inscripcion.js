//RETROCEDES Y AVANZAR MODAL INSCRIPCION
$("#next1").click(function(e) {
    e.preventDefault();
    switch ($("#back1").attr("numberB")) {
        case "0":
            $("#primeraParte").css({ display: "none" });
            $("#segundaParte").css({ display: "block" });
            $("#back1").attr("numberB", "1");
            $("#numero_paso").html("2/5")
            break;
        case "1":
            $("#segundaParte").css({ display: "none" });
            $("#terceraParte").css({ display: "block" });
            $("#back1").attr("numberB", "2");
            $("#numero_paso").html("3/5")
            //  $("#dowload").removeAttr("disabled");
        break;
        case "2":
            $("#numero_paso").html("4/5")
            var validador_formulario=$('#validador_formulario').val();
            var checkedValue = $("[name='facultad_necesita']:checked").val();
            var limite;
            if(checkedValue==2){
                limite=22;
            }else{
                limite=19;
            }
            console.log(limite);
            console.log(validador_formulario);
            $.ajax({
                type:"GET", // la variable type guarda el tipo de la peticion GET,POST,..
                url:"/validar_formulario", //url guarda la ruta hacia donde se hace la peticion
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(datos){ 
                    console.log(datos.mostrar);
                    if(datos.mostrar){
                        $("#terceraParte").css({ display: "none" });
                        $("#cuartaParte").css({ display: "block" });
                        $("#back1").attr("numberB", "3");
                    }else{
                        Swal.fire({
                            title: "Tienes que llenar y guardar todos los campos para poder continuar",
                            icon: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#3085d6",
                            cancelButtonColor: "#d33",
                            // confirmButtonText: "Si",
                            // cancelButtonText: "No",
                            allowOutsideClick: () => Swal.isLoading(),
                            allowEscapeKey: () => Swal.isLoading()
                        });
                    }
                },
            });
        break;
        case "3": 
            $("#numero_paso").html("5/5")
            $("#cuartaParte").css({ display: "none" });
            $("#quintaParte").css({ display: "block" });
            $("#back1").attr("numberB", "4");
        break;
    }
});
$("#back1").click(function(e) {
    e.preventDefault();
    switch ($("#back1").attr("numberB")) {
        case "1":
            $("#numero_paso").html("1/5")
            $("#segundaParte").css({ display: "none" });
            $("#primeraParte").css({ display: "block" });
            $("#back1").attr("numberB", "0");
        break;
        case "2":
            $("#numero_paso").html("2/5")
            $("#segundaParte").css({ display: "block" });
            $("#terceraParte").css({ display: "none" });
            $("#back1").attr("numberB", "1");
            // $("#dowload").attr("disabled","disabled");
        break;
        case "3":
            $("#numero_paso").html("3/5")
            $("#terceraParte").css({ display: "block" });
            $("#cuartaParte").css({ display: "none" });
            $("#back1").attr("numberB", "2");
        break
        case "4":
            $("#numero_paso").html("4/5")
            $("#cuartaParte").css({ display: "block" });
            $("#quintaParte").css({ display: "none" });
            $("#back1").attr("numberB", "3");
        break;
    }
});
// OCULTAR LA OPCION DE FACULTAD    
$("#noFacultad").on("click", function(event) {
    if ($(this).is(":checked")) {
        $("#exoFacultad").prop("checked", false);
        $("#siCurso").prop("checked", false);
        $("#datosCurso").css({ display: "none" });
    } else {
        console.log("no chekeado");
    }
});
$("#exoFacultad").on("click", function(event) {
    if ($(this).is(":checked")) {
        $("#noFacultad").prop("checked", false);
        $("#siCurso").prop("checked", false);
        $("#datosCurso").css({ display: "none" });
    } else {
        console.log("no chekeado");
    }
});
$("#siCurso").on("click", function(event) {
    if ($(this).is(":checked")) {
        $("#noFacultad").prop("checked", false);
        $("#exoFacultad").prop("checked", false);
        $("#datosCurso").css({ display: "block" });
    } else {
        $("#datosCurso").css({ display: "none" });
    }
});
$('#formulario_upload_passport').on("click", function(event) {
    event.preventDefault();
});
// Firma digital 
$('#firma_escaneada').on("change", function(event) {
    event.preventDefault();
    var firma_escaneada=$('#firma_escaneada').val();
    if(firma_escaneada=="si"){
        $("#si_firma_escaneada").css({ display: "block" });
        $("#no_firma_escaneada").css({ display: "none" });
    }else if(firma_escaneada=="no"){
        $("#no_firma_escaneada").css({ display: "block" });
        $("#si_firma_escaneada").css({ display: "none" });
    }
});
// Start upload preview image
// Start upload preview image
var $uploadCrop,
    tempFilename,
    rawImg,
    imageId;
function readFile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.upload-demo').addClass('ready');
            $('#cropImagePop').modal('show');
            rawImg = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
    }
    else {
        swal("Sorry - you're browser doesn't support the FileReader API");
    }
}

$uploadCrop = $('#upload-demo').croppie({
    viewport: {
        width: 150,
        height: 100,
        type: 'square'
    },
    enforceBoundary: false,
    enableExif: true
});
$('#cropImagePop').on('shown.bs.modal', function () {
    // alert('Shown pop');
    $uploadCrop.croppie('bind', {
        url: rawImg
    }).then(function () {
        console.log('jQuery bind complete');
    });
});

$('.item-img').on('change', function () {
    imageId = $(this).data('id'); tempFilename = $(this).val();
    $('#cancelCropBtn').data('id', imageId); readFile(this);
});
$('#cropImageBtn').on('click', function (ev) {
    $uploadCrop.croppie('result', {
        type: 'base64',
        format: 'jpeg',
        size: { width: 125, height: 125 }
    }).then(function (resp) {
        $("#EnviarPDF").css({ cursor:"default",opacity:"1" });
        $('#item-img-output').attr('src', resp);
        $('#cropImagePop').modal('hide');
    });
});
$('.falta_completar').on('click',function(){
    Swal.fire({
        title: "Tienes que enviar el formulario de inscripción para poder continuar",
        icon: "warning",
        // showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        // confirmButtonText: "Si",
        // cancelButtonText: "No",
        allowOutsideClick: () => Swal.isLoading(),
        allowEscapeKey: () => Swal.isLoading()
    });
});
// FUNCION PARA GUARDAR DATOS DE INSCRIPCION
$('.saveInscripcion').click(function(e){
    e.preventDefault();
    var facultad_necesita = [];
    $.each($("input[name='facultad_necesita']:checked"), function(){
        facultad_necesita=$(this).val();
    });
    var data={
        "respon_nombre" : $('#respon_nombre').val(), 
        "vacante_estudio":$('#vacante_estudio').val(),
        "vacante_universidad":$('#vacante_universidad').val(),
        "vacante_rama":$('#vacante_rama').val(),
        "vancante_gradoActual":$('#vancante_gradoActual').val(),
        "vacante_financia":$('#vacante_financia').val(),
        "madre_nombre":$('#madre_nombre').val(),
        "madre_correo":$('#madre_correo').val(),
        "padre_nombre":$('#padre_nombre').val(),
        "padre_correo":$('#padre_correo').val(),
        "padres_direccion":$('#padres_direccion').val(),
        "padres_telefono":$('#padres_telefono').val(),
        "padres_celular":$('#padres_celular').val(),
        "respon_nombre":$('#respon_nombre').val(),
        "respon_correo":$('#respon_correo').val(),
        "respon_telefono":$('#respon_telefono').val(),
        "respon_celular":$('#respon_celular').val(),
        "apoder_nombre":$('#apoder_nombre').val(),
        "apoder_dni":$('#apoder_dni').val(),
        "facultad_necesita":facultad_necesita,
    };
    
    if (facultad_necesita==2) {
        data['facultad_necesita']=facultad_necesita;
        data['facultad_mes']=$('#facultad_mes').val();
        data['facultad_anio']=$('#facultad_anio').val();
        data['facultad_universidad']=$('#facultad_universidad').val();
    }
    $.ajax({
        type:"POST", // la variable type guarda el tipo de la peticion GET,POST,..
        url:"/update_formulario_inscripcion", //url guarda la ruta hacia donde se hace la peticion
        data:data, // data recive un objeto con la informacion que se enviara al servidor
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(datos){ //success es una funcion que se utiliza si el servidor retorna informacion
            Swal.fire({
                title: "Su información se ha guardado correctamente",
                icon: "success",
                showCancelButton: false,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                // confirmButtonText: "Si",
                // cancelButtonText: "No",
                allowOutsideClick: () => Swal.isLoading(),
                allowEscapeKey: () => Swal.isLoading()
            });
        },
    })
})
$('#modal_firmar_contrato').click(function(){
    validar_formulario('modal_firmar_contrato');
});
$('#modal_upload_passport').click(function(){
    validar_formulario('modal_upload_passport');
});
$('#modal_upload_documents').click(function(){
    validar_formulario('modal_upload_documents');
});
$('#modal_upload_otherDocuments').click(function(){
    validar_formulario('modal_upload_otherDocuments');
});

// FUNCION PARA VALIDAR FORMULARIO
function validar_formulario(modal){
    $.ajax({
        type:"GET", // la variable type guarda el tipo de la peticion GET,POST,..
        url:"/validar_formulario", //url guarda la ruta hacia donde se hace la peticion
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function(datos){ 
            if(datos.mostrar){
                $('.'+modal).modal('show');
            }else{
                Swal.fire({
                    title: "Tienes que llenar y guardar todos los campos para poder continuar",
                    icon: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    // confirmButtonText: "Si",
                    // cancelButtonText: "No",
                    allowOutsideClick: () => Swal.isLoading(),
                    allowEscapeKey: () => Swal.isLoading()
                });
            }
        },
    });
}