//NO CLICK INDEX ZONA DE USUARIO
function noclick(e, tipo) {
    e.preventDefault();
    var texto;
    if (tipo == 1) {
        texto =
            "Para poder ingresar necesita ir a Perfil, completar su información personal y autentificar su Telegram.";
    } else if (tipo == 2) {
        texto = "Para poder ingresar es necesario comprar el Curso.";
    }
    toastrNotifyUploadDocument("No se puede acceder ", texto);
}
function toastrNotifyUploadDocument(user, message) {
    toastr.options = {
        escapeHtml: false,
        closeButton: false,
        debug: false,
        newestOnTop: true,
        progressBar: true,
        positionClass: "toast-top-right",
        preventDuplicates: false,
        onclick: null,
        showDuration: "5000",
        hideDuration: "5000",
        timeOut: "5000",
        extendedTimeOut: "5000",
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
    };
    var a_href =
        "<a href='https://www.google.com.pe' target='_blank' class='btn btn-info btn-sm'><i class='fas fa-binoculars'></i> Revisar</a>";
    toastr["info"](message, user);
}

function verDoc(id) {
    let data = {
        id: id
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(data),
        url: "api/user_doc",
        success: function(data) {
            var resultado = JSON.parse(data);
            var filas = "";
            if (resultado["docs_user"].length == 0) {
                var table = $("#table_docs").DataTable();
                table.clear();
                table.destroy();
                $("#table_docs").DataTable({
                    destroy: true,
                    language: {
                        info:
                            "Mostrando _START_ de _END_ de _TOTAL_ resultados",
                        emptyTable: "No hay documentos disponibles",
                        zeroRecords: "No hay documentos disponibles",
                        lengthMenu: "Mostrar _MENU_ resultados",
                        search: "Buscar:",
                        paginate: {
                            first: "Primero",
                            last: "Último",
                            next: ">>",
                            previous: "<<"
                        }
                    }
                });
            } else if (resultado["docs_user"].length > 0) {
                var table = $("#table_docs").DataTable();
                table.clear();
                table.destroy();
                $.each(resultado["docs_user"], function(index, value) {
                    if (value.state != 0) {
                        switch (value.state) {
                            case "1":
                                class_estado = "bg-warning";
                                txt_estado = "POR REVISAR";
                                opc1 =
                                    `<a class="dropdown-item" href="#" onclick="update_doc('` +
                                    value.id +
                                    `','0', '` +
                                    value.nameArchivo +
                                    `','` +
                                    id +
                                    `')">Rechazar</a>`;
                                opc2 =
                                    `<a class="dropdown-item" href="#" onclick="update_doc('` +
                                    value.id +
                                    `','2','` +
                                    value.nameArchivo +
                                    `','` +
                                    id +
                                    `')">Aceptar</a>`;
                                break;
                            case "2":
                                class_estado = "bg-success";
                                txt_estado = "ACEPTADO";
                                opc1 =
                                    `<a class="dropdown-item" href="#" onclick="update_doc('` +
                                    value.id +
                                    `','0', '` +
                                    value.nameArchivo +
                                    `','` +
                                    id +
                                    `')">Rechazar</a>`;
                                opc2 = `<a class="dropdown-item isDisabled" href="#" >Aceptar</a>`;
                                break;
                        }
                        var documento =
                            "<td>" + value.nameArchivo.toUpperCase() + "</td>";
                        var estado =
                            "<td align='center'><span class='btn btn-sm " +
                            class_estado +
                            "'>" +
                            txt_estado +
                            "</span></td>";
                        var acciones =
                            `<td align='center'>
                        <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Acciones
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        ` +
                            opc1 +
                            `
                        ` +
                            opc2 +
                            `
                        </div>
                        </div>
                        </td>
                        `;
                        var ver =
                            `<td align="center">
                                <a href="https://drive.google.com/drive/u/0/folders/` +
                            resultado["folder_url"][0].folder_id +
                            `" target="_blank">
                            <span class='btn btn-sm bg-light'><i class="fab fa-google-drive"></i> <strong>VER EN DRIVE</strong></span>
                                </a>
                            </td>`;

                        filas += "<tr>";
                        filas += documento;
                        filas += estado;
                        filas += ver;
                        filas += acciones;
                        filas += "</tr>";
                    }
                });
                $("#table_docs tbody").html(filas);
                $("#table_docs").DataTable({
                    destroy: true,
                    language: {
                        info:
                            "Mostrando _START_ de _END_ de _TOTAL_ resultados",
                        emptyTable: "No hay documentos disponibles",
                        zeroRecords: "No hay documentos disponibles",
                        lengthMenu: "Mostrar _MENU_ resultados",
                        search: "Buscar:",
                        paginate: {
                            first: "Primero",
                            last: "Último",
                            next: ">>",
                            previous: "<<"
                        }
                    }
                });
            }
        }
    });
}

function update_doc(id, state, nameArchivo, id_user) {
    let textarea = `<div class="form-group">
        <label for="exampleFormControlTextarea1">Escriba un comentario:</label>
        <textarea class="form-control" id="razon" rows="3" cols="20"></textarea>
      </div>`;
    if (state == 0) {
        title = "rechazar";
    } else if (state == 2) {
        title = "aprobar";
    }
    Swal.fire({
        title: "¿Está seguro de " + title + " este documento?",
        icon: "warning",
        html: textarea,
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        allowOutsideClick: () => Swal.isLoading(),
        allowEscapeKey: () => Swal.isLoading()
    }).then(result => {
        if (result.value) {
            let data = {
                id: id,
                state: state,
                nameArchivo: nameArchivo,
                id_user: id_user,
                razon: $("#razon").val()
            };
            $.ajax({
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(data),
                url: "updated_user_doc",
                success: function(response) {
                    if (response == 1) {
                        Swal.fire("Estado actualizado", "", "success");
                        setTimeout(() => {
                            $(".close").click();
                        }, 1500);
                    }
                }
            });
        }
    });
}

$(".see_courses").on("click", function() {
    let data = {
        id: this.dataset.id
    };
    $("#id_alumno").val(this.dataset.id);
    $.ajax({
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(data),
        url: "getCursosxID",
        success: function(data) {
            var resultado = JSON.parse(data);
            var filas = "";
            $.each(resultado["cursos"], function(index, value) {
                var ul_clases_activas =
                    `
                            <ul class="list-group lista_cursos_activos">
                                <li class="list-group-item d-flex justify-content-between align-items-center">` +
                    value.nombreModulo +
                    ` - ` +
                    value.Title +
                    `<input type="checkbox" class="cursos_activos" data-id="` +
                    value.id_video +
                    `"  val="` +
                    value.id_video +
                    `" />
                                </li>
                            </ul>`;

                filas += ul_clases_activas;
                $(".clases-activas").html(filas);
            });
        }
    });
});
$(".see_modules").on("click", function() {
    $.ajax({
        type: "GET",
        url: "/getModulos",
        success: function(data) {
            var resultado = JSON.parse(data);
            var filas = "";
            $.each(resultado["modulos"], function(index, value) {
                var li =
                    `<ul class="list-group lista_cursos_activos">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            ` +
                    value.abreviatura +
                    ` - ` +
                    value.nombre +
                    `
                            <input type="checkbox" class="modulos modulos_new" data-id="` +
                    value.id +
                    `" />
                        </li>
                      </ul>`;

                filas += li;
                $(".s_mod").html(filas);
            });
        }
    });
});

$(".add_clases").on("click", function() {
    let data = {
        id_cursos: $(".select2").val(),
        id_alumno: $("#id_alumno").val()
    };

    $.ajax({
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(data),
        url: "updateCursosActivosxID",
        success: function(data) {
            // LIMPIAR TAB CURSOS ACTIVOS
            $(".lista_cursos_activos").remove();
            // LIMPIAR SELECT DE CLASES
            $(".select2")
                .val(null)
                .trigger("change");
            let data2 = {
                id: $("#id_alumno").val()
            };
            $.ajax({
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(data2),
                url: "getCursosxID",
                success: function(data) {
                    var resultado = JSON.parse(data);
                    var filas = "";
                    $.each(resultado["cursos"], function(index, value) {
                        var ul_clases_activas =
                            `
                                    <ul class="list-group lista_cursos_activos">
                                        <li class="list-group-item d-flex justify-content-between align-items-center">` +
                            value.nombreModulo +
                            ` - ` +
                            value.Title +
                            `<input type="checkbox" class="cursos_activos" data-id="` +
                            value.id_video +
                            `" val="` +
                            value.id_video +
                            `" />
                                        </li>
                                    </ul>`;

                        filas += ul_clases_activas;
                        $(".clases-activas").html(filas);
                    });
                    $("#ver_clases-tab").click();
                }
            });
        }
    });
});

$(".save_clases").on("click", function() {
    var cursos_activos = [];
    $(".cursos_activos:checkbox:checked").each(function() {
        cursos_activos.push(this.dataset.id);
    });
    let data = {
        cursos: cursos_activos,
        id_alumno: $("#id_alumno").val()
    };
    $.ajax({
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(data),
        url: "removeCursosxID",
        success: function(data) {
            // LIMPIAR TAB CURSOS ACTIVOS
            $(".clases-activas").fadeOut();
            setTimeout(() => {
                $(".lista_cursos_activos").remove();
            }, 500);
            let data2 = {
                id: $("#id_alumno").val()
            };
            setTimeout(() => {
                $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(data2),
                    url: "getCursosxID",
                    success: function(data) {
                        var resultado = JSON.parse(data);
                        var filas = "";
                        $.each(resultado["cursos"], function(index, value) {
                            var ul_clases_activas =
                                `
                                        <ul class="list-group lista_cursos_activos">
                                            <li class="list-group-item d-flex justify-content-between align-items-center">` +
                                value.nombreModulo +
                                ` - ` +
                                value.Title +
                                `<input type="checkbox" class="cursos_activos" data-id="` +
                                value.id_video +
                                `"  val="` +
                                value.id_video +
                                `" />
                                            </li>
                                        </ul>`;

                            filas += ul_clases_activas;
                            // $(".clases-activas").hide();
                            $(".clases-activas").fadeIn("slow");
                            $(".clases-activas").html(filas);
                        });
                    }
                });
            }, 1100);
        }
    });
});

$(".add_modulos").on("click", function() {
    var modulos_new = [];
    $(".modulos_new:checkbox:checked").each(function() {
        modulos_new.push(this.dataset.id);
    });
    console.log(modulos_new);
    let data = {
        id: modulos_new,
        id_alumno: $("#id_alumno").val()
    };
    $.ajax({
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(data),
        url: "/add_clasesXmodulos",
        success: function(data) {
            let data2 = {
                id: $("#id_alumno").val()
            };
            $.ajax({
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(data2),
                url: "getCursosxID",
                success: function(data) {
                    var resultado = JSON.parse(data);
                    var filas = "";
                    $.each(resultado["cursos"], function(index, value) {
                        var ul_clases_activas =
                            `
                                    <ul class="list-group lista_cursos_activos">
                                        <li class="list-group-item d-flex justify-content-between align-items-center">` +
                            value.nombreModulo +
                            ` - ` +
                            value.Title +
                            `<input type="checkbox" class="cursos_activos" data-id="` +
                            value.id_video +
                            `"  val="` +
                            value.id_video +
                            `" />
                                        </li>
                                    </ul>`;

                        filas += ul_clases_activas;
                        // $(".clases-activas").hide();
                        $(".clases-activas").fadeIn("slow");
                        $(".clases-activas").html(filas);
                    });
                }
            });
            setTimeout(() => {
                $("#ver_clases-tab").click();
            }, 500);
        }
    });
});

$("#modalActivarCursos").on("hide.bs.modal", function(e) {
    // LIMPIAR TAB CURSOS ACTIVOS
    $(".lista_cursos_activos").remove();
    // LIMPIAR SELECT DE CLASES
    $(".select2")
        .val(null)
        .trigger("change");
    $("#id_alumno").val("");
    $("#ver_clases-tab").click();
});
//PASAR ID DEL ALUMNO AL MODAL ENVIAR DOCUMENTOS ADMIN
$(".enviar_doc_to_user").on("click", function() {
    let id_user = $(this).data("id");
    $(".doc_to_user .u538").val(id_user);
});
//
