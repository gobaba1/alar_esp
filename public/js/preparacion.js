function change_video(id_video,title){
    let urls="http://127.0.0.1:8000/videos/"+id_video;
    document.getElementById("video_ruso").src = urls;
    document.getElementById("video").load(); 
    document.getElementById("video").play(); 
    document.getElementById("title_video").innerHTML=title; 
    document.getElementById("video_id").value=id_video; 
    console.log(urls);
}
// FUNCION QUE ACTUALIZA EL PROGRESS DE USER ID CADA QUE ACABA EL VIDEO
document.getElementById('video').addEventListener('ended',myHandler,false);
function myHandler(e) {
    let video_id=document.getElementById("video_id").value;
    document.getElementById('checkbox'+video_id).checked = true;
    let data = {
        user_id: $('#user_id').val(),
        video_id: video_id,
        checked : "checked"
    };
    ajax_update_progress(data);
}   
/*Actualizar videos_user con el input checked*/
function input_checked(id_video){
    if( $('#checkbox'+id_video).is(':checked') ) {
        let data = {
            user_id: $('#user_id').val(),
            video_id: id_video,
            checked : "checked"
        };
        ajax_update_progress(data);
    }else{
        let data = {
            user_id : $('#user_id').val(),
            video_id: id_video,
            checked : ""
        };
        ajax_update_progress(data);
    }
}
/* FUNCTION PARA ACTUALIZAR PROGRESS EN LA TABLA VIDEOS_USER */
function ajax_update_progress(data){
    $.ajax({
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(data),
        url: "api/update_progress",
        success: function(response) {
            console.log(response);
            document.getElementById('progress_curso').style.width = response+'%';
            document.getElementById('progress_curso').innerHTML = response+'%';
        }
    });
}
//EVITAR DESCARGA DE VIDEO CON CLICK DERECHO
$("#video").bind("contextmenu",function(e){
    return false;
});