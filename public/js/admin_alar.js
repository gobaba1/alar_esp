$(document).ready(function() {
    // COMPRIMIR EL SIDEBAR DESPUES DE 1s
    // $(document).ready(function () {
    //     var body = document.body;
    //     setTimeout(function () {
    //         body.classList.add("sidebar-collapse");
    //     }, 1000);
    // });
    //Initialize Select2 Elements
    $(".select2").select2();

    //DT LISTADO DE USUARIOS
    $("#table_users").DataTable({
        language: {
            info: "Mostrando _START_ de _END_ de _TOTAL_ resultados",
            emptyTable: "No hay usuarios disponibles",
            zeroRecords: "No hay usuarios disponibles",
            lengthMenu: "Mostrar _MENU_ resultados",
            search: "Buscar:",
            paginate: {
                first: "Primero",
                last: "Último",
                next: ">>",
                previous: "<<"
            }
        }
    });
    // $("#table_docs").DataTable({
    //     destroy: true,
    //     language: {
    //         info: "Mostrando _START_ de _END_ de _TOTAL_ resultados",
    //         emptyTable: "No hay usuarios disponibles",
    //         zeroRecords: "No hay usuarios disponibles",
    //         lengthMenu: "Mostrar _MENU_ resultados",
    //         search: "Buscar:",
    //         paginate: {
    //             first: "Primero",
    //             last: "Último",
    //             next: ">>",
    //             previous: "<<"
    //         }
    //     }
    // });
});
function toastrNotifyUploadDocument(user, message) {
    toastr.options = {
        escapeHtml: false,
        closeButton: false,
        debug: false,
        newestOnTop: true,
        progressBar: true,
        positionClass: "toast-top-right",
        preventDuplicates: false,
        onclick: null,
        showDuration: "5000",
        hideDuration: "5000",
        timeOut: "5000",
        extendedTimeOut: "5000",
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
    };
    var a_href =
        "<a href='https://www.google.com.pe' target='_blank' class='btn btn-info btn-sm'><i class='fas fa-binoculars'></i> Revisar</a>";
    toastr["info"](message, user);
}
//DUPLICAR ELEMENTO
