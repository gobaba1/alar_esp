function agregarIdioma() {
    var _this = $(this);
    var random = Math.random();
    var n = random.toString();
    var m = n.split(".");
    random = "m" + m[1];
    checked = $("#datatable-responsive tbody tr").length + 1;
    if (checked > 0) {
        for (var i = 0; i < checked; i++) {
            var valor = i + 1;
            $("#datatable-responsive tbody tr")
                .eq(i)
                .find(".predeterminado")
                .attr("value", valor);
        }
    }
    var onclic = "'" + random + "'";
    var table = '<tr id="fila-' + random + '">';
    table += `<td>
    <label class="label-input-format">Idiomas</label>
        <select class="form-control input-format marginb-24 lenguage" name="idioma[]" >
            <option value="" selected="selected">Elegir</option>
            <option value="1">Inglés</option>
            <option value="2">Alemán</option>
            <option value="3">Chino</option>
            <option value="4">Francés</option>
            <option value="5">Italiano</option>
            <option value="6">Portugués</option>
            <option value="7">Ruso</option>
        </select>
    </td>`;
    table += `<td>
    <label class="label-input-format">Hablado</label>
        <select class="form-control input-format marginb-24" name="escrito[]">
            <option value="1" selected="selected">Básico</option>
            <option value="2">Intermedio</option>
            <option value="3">Avanzado</option>
        </select>
    </td>`;
    table += `<td>
    <label class="label-input-format">Escrito</label>
        <select class="form-control input-format marginb-24" name="lectura[]">
            <option value="1" selected="selected">Básico</option>
            <option value="2">Intermedio</option>
            <option value="3">Avanzado</option>
        </select>
    </td>`;
    table += `<td>
    <label class="label-input-format">Lectura</label>
        <select class="form-control input-format marginb-24" name="hablado[]">
            <option value="1" selected="selected">Básico</option>
            <option value="2" >Intermedio</option>
            <option value="3">Avanzado</option>
        </select>
    </td>`;

    table +=
        '<td class="text-center"><a href="javascript:;" class="btn btn-sm btn-danger" style="margin-top: 2rem;" data-fila="fila-' +
        random +
        '" onclick="eliminarFila(' +
        onclic +
        ')"><i class="fas fa-trash"></i></a></td>';
    table += "</tr>";

    $("form #datatable-responsive tbody").append(table);

    return false;
}
function eliminarFila(fila) {
    $("table tr#fila-" + fila)
        .hide()
        .remove();

    return false;
}
$("#idiomas_state").change(function() {
    switch ($("#idiomas_state").val()) {
        case "1":
            agregarIdioma();
            $("#addLenguage").removeAttr("disabled");

            break;
        case "2":
            $("table tr").remove();
            $("#addLenguage").attr("disabled", "disabled");
            break;
        default:
            $("table tr").remove();
            $("#addLenguage").attr("disabled", "disabled");
            break;
    }
});
$("#addLenguage").click(function(e) {
    e.preventDefault();
    // var c = document.getElementById("msj");
    // var d = document.getElementById("user_idiomas");
    // var clon = c.cloneNode("msj");
    // var newDiv = document.createElement("div");
    // clon.innerHTML = clon.innerHTML;
    // newDiv.appendChild(clon); //a�0�9ade texto al div creado.
    // d.appendChild(clon);
});
//ELIMINAR FILA DE LENGUAJE
$(document).on("click", ".deleteLenguage", function(e) {
    e.preventDefault();
    let cant = $(".deleteLenguage *").length;
    if (cant > 0) {
        e.preventDefault();
        let element = $(this)[0].parentElement.parentElement;
        element.remove();
    }
});
// INTRODUCIR PRE FIJO EN CELULAR
$("#pais").change(function() {
    let data = {
        id: $("#pais").val()
    };
    $.ajax({
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(data),
        url: "api/codePhone",
        success: function(response) {
            // console.log();
            // $("#prefijo").val("+" + response[0].codigoTelefono);
            $("#prefijoHide").val("+" + response[0].codigoTelefono);
        }
    });
});
//COPIAR EMAIL
$("#basic-addon2").click(function() {
    /* Get the text field */
    var copyText = document.getElementById("user_email");
    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/
    /* Copy the text inside the text field */
    document.execCommand("copy");
    /* Alert the copied text */
    // Toast.fire({
    //     type: 'Copiado',
    //     title: 'El correo ha sido copiado correctamente'
    // });
    alert("El correo " + copyText.value + " ha sido copiado exitosamente");
});
// Start upload preview image
var $uploadCrop,
    tempFilename,
    rawImg,
    imageId;
function readFile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.upload-demo').addClass('ready');
            $('#cropImagePop').modal('show');
            rawImg = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
    }
    else {
        swal("Sorry - you're browser doesn't support the FileReader API");
    }
}

$uploadCrop = $('#upload-demo').croppie({
    viewport: {
        width: 125,
        height: 125,
        type: 'circle'
    },
    enforceBoundary: false,
    enableExif: true
});
$('#cropImagePop').on('shown.bs.modal', function () {
    // alert('Shown pop');
    $uploadCrop.croppie('bind', {
        url: rawImg
    }).then(function () {
        console.log('jQuery bind complete');
    });
});

$('.item-img').on('change', function () {
    imageId = $(this).data('id'); tempFilename = $(this).val();
    $('#cancelCropBtn').data('id', imageId); readFile(this);
});
$('#cropImageBtn').on('click', function (ev) {
    $uploadCrop.croppie('result', {
        type: 'base64',
        format: 'jpeg',
        size: { width: 125, height: 125 }
    }).then(function (resp) {
        let data = {
            "imagen": resp,
            "id"    : $("#user_id").val()
        };
        $.ajax({
            type: "POST",
            data: data,
            url: "api/changeImage",
            success: function(response) {
                console.log(response);
            }
        });
        $('#item-img-output').attr('src', resp);
        $('#img_profile').attr('src', resp);
        $('#cropImagePop').modal('hide');
    });
});
// End upload preview image
